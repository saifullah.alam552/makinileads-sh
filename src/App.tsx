import { AnimatePresence } from "framer-motion";
import React, { Suspense } from "react";
import { Route, Switch, useLocation } from "react-router-dom";
import { ScaleLoader } from "react-spinners";
import Layout from "./components/Layout";
import PrivateRoute from "./components/PrivateRoute";

const HomePage = React.lazy(() => import("./containers/HomePage"));
const ForgetPasswordPage = React.lazy(
  () => import("./containers/ForgetPasswordPage")
);
const SignupCreateJobPage = React.lazy(
  () => import("./containers/SignupCreateJobPage")
);
const SRSuccessPage = React.lazy(
  () => import("./containers/SuccessPages/SRSuccessPage")
);
const JobSuccessPage = React.lazy(
  () => import("./containers/SuccessPages/JobSuccessPage")
);
const JobSuccessPageLogin = React.lazy(
  () => import("./containers/SuccessPages/JobSuccessPageLogin")
);
const SPSuccessPage = React.lazy(
  () => import("./containers/SuccessPages/SPSuccessPage")
);
const EmailConfirmed = React.lazy(
  () => import("./containers/SuccessPages/EmailConfirmed")
);
const ForgetPasswordSuccess = React.lazy(
  () => import("./containers/SuccessPages/ForgetPasswordSuccess")
);
const Page404 = React.lazy(() => import("./containers/Page404"));

const Dashboard = React.lazy(() => import("./containers/Dashboard/Dashboard"));
const AccountSettings = React.lazy(
  () => import("./containers/Dashboard/AccountSettings")
);
const ManageUsers = React.lazy(
  () => import("./containers/Dashboard/ManageUsers")
);
const ManageSkills = React.lazy(
  () => import("./containers/Dashboard/ManageSkills")
);
const ManageJobs = React.lazy(
  () => import("./containers/Dashboard/ManageJobs")
);
const UserDetails = React.lazy(
  () => import("./containers/Dashboard/UserDetails")
);
const SkillForm = React.lazy(() => import("./containers/Dashboard/SkillForm"));
const JobForm = React.lazy(() => import("./containers/Dashboard/JobForm"));
const JobView = React.lazy(() => import("./containers/Dashboard/JobView"));
const CategoryForm = React.lazy(
  () => import("./containers/Dashboard/CategoryForm")
);

function App() {
  const location = useLocation();

  const routes = (
    <AnimatePresence exitBeforeEnter initial={false}>
      <Switch location={location} key={location.pathname}>
        <Route path="/forget-password" exact component={ForgetPasswordPage} />
        <Route
          path="/service-provider-signup"
          exact
          component={() => <SignupCreateJobPage formType={"SP"} />}
        />
        <Route
          path="/service-receiver-signup"
          exact
          component={() => <SignupCreateJobPage formType={"SR"} />}
        />
        <Route
          path="/create-job"
          exact
          component={() => <SignupCreateJobPage formType={"JOB"} />}
        />
        <Route path="/sp-success" exact component={SPSuccessPage} />
        <Route path="/sr-success" exact component={SRSuccessPage} />
        <Route path="/job-success" exact component={JobSuccessPage} />
        <Route
          path="/job-success-login"
          exact
          component={JobSuccessPageLogin}
        />
        <Route path="/email-confirmed" exact component={EmailConfirmed} />
        <Route
          path="/forget-password-success"
          exact
          component={ForgetPasswordSuccess}
        />
        <Route path="/" exact component={HomePage} />

        <PrivateRoute path="/dashboard" exact component={Dashboard} />
        <PrivateRoute
          path="/dashboard/account-settings"
          exact
          component={AccountSettings}
        />
        <PrivateRoute
          path="/dashboard/manage-jobs"
          exact
          component={ManageJobs}
        />
        <PrivateRoute
          path="/dashboard/manage-users"
          exact
          component={ManageUsers}
        />
        <PrivateRoute
          path="/dashboard/manage-skills"
          exact
          component={ManageSkills}
        />
        <PrivateRoute
          path="/dashboard/user-details/:userId"
          exact
          component={UserDetails}
        />
        <PrivateRoute
          path="/dashboard/manage-skills/category"
          exact
          component={CategoryForm}
        />
        <PrivateRoute
          path="/dashboard/manage-skills/category/:catId"
          exact
          component={CategoryForm}
        />
        <PrivateRoute
          path="/dashboard/manage-skills/skill"
          exact
          component={SkillForm}
        />
        <PrivateRoute
          path="/dashboard/manage-skills/skill/:skillId"
          exact
          component={SkillForm}
        />
        <PrivateRoute
          path="/dashboard/job/view/:jobId"
          exact
          component={JobView}
        />
        <PrivateRoute
          path="/dashboard/job/:action/:jobId"
          exact
          component={JobForm}
        />

        <Route path="*" exact component={Page404} />
      </Switch>
    </AnimatePresence>
  );

  return (
    <Suspense
      fallback={
        <div
          style={{
            height: "100vh",
            width: "100%",
            margin: "0 auto",
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
          }}
        >
          <ScaleLoader
            loading={true}
            color={"#02A7FF"}
            height={25}
            margin={3}
          />
        </div>
      }
    >
      <Layout>{routes}</Layout>
    </Suspense>
  );
}

export default App;
