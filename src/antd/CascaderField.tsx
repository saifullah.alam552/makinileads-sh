import { Cascader } from "antd";
import React from "react";

interface Option {
  value: string | number;
  label?: React.ReactNode;
  disabled?: boolean;
  children?: Option[];
}

interface Props {
  id: string;
  name: string;
  option: Option[];
  label?: string;
  value?: string[] | number[];
  defaultValue?: string[] | number[];
  onChange?: (value: any, selectedOptions: any) => void;
  error?: string;
  required?: boolean;
  disabled?: boolean;
  displayError?: boolean;
  placeHolder?: string;
  size?: "small" | "middle" | "large" | undefined;
  refEl?: any;
}

export default function CascaderField(props: Props) {
  return (
    <div
      style={
        props.displayError
          ? props.error
            ? { marginBottom: "4px" }
            : { marginBottom: "20px" }
          : { marginBottom: 0 }
      }
    >
      {props.label ? (
        <div>
          {props.label}:{" "}
          {props.required ? <span style={{ color: "red" }}>*</span> : null}
        </div>
      ) : null}

      <div>
        <Cascader
          options={props.option}
          onChange={props.onChange}
          placeholder={props.placeHolder}
          style={{ width: "100%" }}
          size={props.size}
          ref={props.refEl}
          value={props.value}
          disabled={props.disabled}
        />
      </div>

      <div style={{ color: "red", fontSize: ".7rem" }}>
        {props.error ? props.error : " "}
      </div>
    </div>
  );
}
