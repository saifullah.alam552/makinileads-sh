import { Input, InputNumber } from "antd";
import React from "react";

interface Props {
  id: string;
  name: string;
  type: "simple" | "password" | "number" | "textarea" | "currency";
  label?: string;
  value?: string | string[] | number;
  onBlur?: (event: any) => void;
  onChange?: (event: any) => void;
  placeHolder?: string;
  error?: string;
  required?: boolean;
  disabled?: boolean;
  addonAfter?: React.ReactNode;
  addonBefore?: React.ReactNode;
  prefix?: string;
  suffix?: string;
  displayError?: boolean;
  onPressEnter?: (event: any) => void;
  refEl?: any;
  size?: "small" | "middle" | "large" | undefined;
  onFocus?: (event: any) => void;
  numSteps?: number;
  textAreaRows?: number;
  textAreaMaxLength?: number;
  max?: number;
}

export default function InputField(props: Props) {
  const simpleInput = (
    <Input
      id={props.id}
      name={props.name}
      value={props.value}
      onChange={props.onChange}
      disabled={props.disabled}
      onBlur={props.onBlur}
      addonAfter={props.addonAfter}
      addonBefore={props.addonBefore}
      prefix={props.prefix}
      suffix={props.suffix}
      placeholder={props.placeHolder}
      style={{ borderColor: props.error ? "red" : "#d9d9d9" }}
      ref={props.refEl}
      onPressEnter={props.onPressEnter}
      size={props.size}
      onFocus={props.onFocus}
      allowClear={true}
    />
  );
  const textAreaInput = (
    <Input.TextArea
      id={props.id}
      name={props.name}
      value={props.value}
      onChange={props.onChange}
      disabled={props.disabled}
      onBlur={props.onBlur}
      placeholder={props.placeHolder}
      style={{ borderColor: props.error ? "red" : "#d9d9d9" }}
      ref={props.refEl}
      onPressEnter={props.onPressEnter}
      size={props.size}
      onFocus={props.onFocus}
      rows={props.textAreaRows}
      autoSize={{ minRows: 4, maxRows: 8 }}
      maxLength={props.textAreaMaxLength}
      showCount={true}
    />
  );
  const passwordInput = (
    <Input.Password
      id={props.id}
      name={props.name}
      value={props.value}
      onChange={props.onChange}
      onBlur={props.onBlur}
      disabled={props.disabled}
      placeholder={props.placeHolder}
      style={{ borderColor: props.error ? "red" : "#d9d9d9" }}
      ref={props.refEl}
      onPressEnter={props.onPressEnter}
      onFocus={props.onFocus}
      allowClear={true}
    />
  );

  const numberInput = (
    <InputNumber
      id={props.id}
      name={props.name}
      min={0}
      defaultValue={0}
      value={props.value !== null ? parseInt(props.value!.toString(), 10) : 0}
      step={props.numSteps ? props.numSteps : 1}
      onChange={props.onChange}
      style={{ width: "100%" }}
      ref={props.refEl}
      onPressEnter={props.onPressEnter}
      disabled={props.disabled}
      onFocus={props.onFocus}
    />
  );

  
  const currencyInput = (
    <InputNumber
      id={props.id}
      name={props.name}
      min={0}
      max={props.max}
      defaultValue={0}
      value={props.value !== null ? parseInt(props.value!.toString(), 10) : 0}
      step={props.numSteps ? props.numSteps : 100}
      formatter={(value) => `$ ${parseInt(value!.toString(), 10)}`.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")}
      parser={(value) => (value === "" || value === null ? 0 : value!.replace(/\$\s?|(,*)/g, ""))}
      onChange={props.onChange}
      style={{ width: "100%" }}
      ref={props.refEl}
      onPressEnter={props.onPressEnter}
      disabled={props.disabled}
      onFocus={props.onFocus}
    />
  )

  return (
    <div
      style={
        props.displayError
          ? props.error
            ? { marginBottom: "4px" }
            : { marginBottom: "20px" }
          : { marginBottom: 0 }
      }
    >
      {props.label ? (
        <div>
          {props.label}:{" "}
          {props.required ? <span style={{ color: "red" }}>*</span> : null}
        </div>
      ) : null}

      <div>{props.type === "simple" ? simpleInput : null}</div>
      <div>{props.type === "textarea" ? textAreaInput : null}</div>
      <div>{props.type === "password" ? passwordInput : null}</div>
      <div>{props.type === "number" ? numberInput : null}</div>
      <div>{props.type === "currency" ? currencyInput : null}</div>

      <div style={{ color: "red", fontSize: ".7rem" }}>
        {props.error ? props.error : " "}
      </div>
    </div>
  );
}
