import { Select } from "antd";
import React from "react";

const { Option } = Select;

interface Props {
  label?: string;
  id: string;
  value?: string | string[] | number;
  onChange?: (event: any) => void;
  onBlue?: (event: any) => void;
  placeholder?: string;
  error?: string;
  required: boolean;
  disabled?: boolean;
  data: any[];
  loading?: boolean;
  displayError?: boolean;
  onSearch?: (value: string) => void;
  onDropdownVisibleChange?: (open: boolean) => void;
}

function MultiSelectField(props: Props) {
  return (
    <div
      style={
        props.displayError
          ? props.error
            ? { marginBottom: "4px" }
            : { marginBottom: "20px" }
          : { marginBottom: 0 }
      }
    >
      {props.label && (
        <div>
          {props.label}:{" "}
          {props.required ? <span style={{ color: "red" }}>*</span> : null}
        </div>
      )}
      <div style={{ height: "32px" }}>
        <Select
          mode={"multiple" as const}
          id={props.id}
          value={props.value}
          onChange={props.onChange}
          onBlur={props.onBlue}
          placeholder={props.placeholder}
          dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
          disabled={props.disabled}
          style={{ width: "100%" }}
          loading={props.loading}
          onDropdownVisibleChange={props.onDropdownVisibleChange}
          maxTagCount={"responsive" as const}
        >
          {props.data
            .filter((s) => s.disabled !== false)
            .map((d) => (
              <Option key={d.key} value={`${d.key}`}>
                {d.label}
              </Option>
            ))}
        </Select>
      </div>
      <div style={{ color: "red", fontSize: ".7rem" }}>
        {props.error ? props.error : " "}
      </div>
    </div>
  );
}

export default MultiSelectField;
