import React from "react";
import { Select } from "antd";

const { Option } = Select;

interface Props {
  label?: string;
  id: string;
  value?: string | string[] | number;
  onChange?: (event: any) => void;
  onBlue?: (event: any) => void;
  placeholder?: string;
  error?: string;
  required: boolean;
  disabled?: boolean;
  data: any[];
  loading?: boolean;
  showSearch?: boolean;
  displayError?: boolean;
  onSearch?: (value: string) => void;
  onDropdownVisibleChange?: (open: boolean) => void;
  allowClear?: boolean;
}

function SelectField(props: Props) {
  return (
    <div
      style={
        props.displayError
          ? props.error
            ? { marginBottom: "4px" }
            : { marginBottom: "20px" }
          : { marginBottom: 0 }
      }
    >
      {props.label && (
        <div>
          {props.label}:{" "}
          {props.required ? <span style={{ color: "red" }}>*</span> : null}
        </div>
      )}
      <div style={{ height: "32px" }}>
        <Select
          showSearch={props.showSearch}
          filterOption={!!props.showSearch}
          allowClear={props.allowClear}
          id={props.id}
          value={props.value}
          onChange={props.onChange}
          onBlur={props.onBlue}
          placeholder={props.placeholder}
          dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
          disabled={props.disabled}
          style={{ width: "100%" }}
          loading={props.loading}
          onSearch={props.onSearch}
          onDropdownVisibleChange={props.onDropdownVisibleChange}
        >
          {props.showSearch
            ? props.data.map((d) => (
                <Option key={d.key} value={`${d.key} ${d.label}`}>
                  {d.label}
                </Option>
              ))
            : props.data.map((d) => (
                <Option key={d.key} value={`${d.key}`} disabled={!d.disabled}>
                  {d.label}
                </Option>
              ))}
        </Select>
      </div>
      <div style={{ color: "red", fontSize: ".7rem" }}>
        {props.error ? props.error : " "}
      </div>
    </div>
  );
}

export default SelectField;
