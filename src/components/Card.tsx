import {
  Avatar,
  Button,
  Descriptions,
  Divider,
  Empty,
  Tag,
  Typography,
} from "antd";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import {
  IJobSummaryData,
  IUserData,
  JobPurchaseType,
  UserTypeEnum,
} from "../types";
import ImageThumbs from "./ImageThumbs";
import SelectedSkills from "./SelectedSkills";

dayjs.extend(relativeTime);

export function Card(props: { children: React.ReactNode }) {
  return (
    <div className="p-4 mx-auto bg-white rounded-xl shadow-md">
      {props.children}
    </div>
  );
}

export const JobSummaryCard = (props: {
  job: IJobSummaryData;
  extra?: JSX.Element;
}) => (
  <div
    className="bg-white px-4 py-4 flex flex-col md:justify-between md:flex-row rounded shadow-md h-full"
    style={{ width: "350px" }}
  >
    <div className="flex gap-4 flex-row flex-wrap md:flex-nowrap overflow-hidden">
      <div className="flex flex-col " style={{ maxWidth: "245px" }}>
        <div className="font-semibold text-lg">
          <Typography.Paragraph
            ellipsis={{ expandable: false, rows: 1 }}
            style={{ margin: 0 }}
          >
            {props.job.jobTitle}
          </Typography.Paragraph>
          <div className="font-normal text-xs mb-2">
            {dayjs(props.job.createdAt).fromNow()}
          </div>
        </div>

        <div className="flex flex-col gap-2 items-start">
          <Typography.Paragraph ellipsis={{ expandable: false, rows: 2 }}>
            {props.job.jobDescription}
          </Typography.Paragraph>

          <div
            className={
              props.job.jobPurchaseType === JobPurchaseType.GENERAL
                ? "bg-green-800 px-2 py-1 rounded text-white text-xs"
                : props.job.jobPurchaseType === JobPurchaseType.EXCLUSIVE
                ? "bg-indigo-800 px-2 py-1 rounded text-white text-xs"
                : "bg-gray-800 px-2 py-1 rounded text-white text-xs"
            }
          >
            {props.job.jobPurchaseType === JobPurchaseType.NONE
              ? "Not Purchased"
              : props.job.jobPurchaseType}
          </div>

        </div>
      </div>
    </div>

    {props.extra && (
      <div
        className="md:text-center w-full mt-4 mb-2 md:w-auto md:ml-2 md:my-0"
        style={{ width: "64px" }}
      >
        {props.extra}
      </div>
    )}
  </div>
);

export const UserCard = (props: { user: IUserData; pathname: string }) => (
  <div className="bg-white px-4 py-2 flex flex-row flex-wrap items-center w-md md:justify-between md:flex-nowrap rounded shadow-md">
    <div className="flex flex-row flex-wrap gap-4 items-center  md:flex-nowrap">
      <div className="w-16">
        <Avatar
          size={64}
          className="border-2 border-gray-200"
          src={
            props.user.profilePicUrl === "" || props.user.profilePicUrl === null
              ? "/images/profile_pic.jpg"
              : props.user.profilePicUrl
          }
        />
      </div>

      <div className="flex flex-col ">
        <div className="font-semibold">
          {props.user.userType === UserTypeEnum.SP_COM ||
          props.user.userType === UserTypeEnum.SR_COM
            ? `${props.user.companyName} `
            : `${props.user.firstName} ${props.user.lastName}`}
        </div>

        <div className="flex flex-row flex-wrap gap-2">
          <div>
            {props.user.userType === UserTypeEnum.SP_COM
              ? "Serivce Provider Company"
              : props.user.userType === UserTypeEnum.SP_IND
              ? "Service Provider Individual"
              : props.user.userType === UserTypeEnum.SR_IND
              ? "Service Receiver Individual"
              : props.user.userType === UserTypeEnum.SR_COM
              ? "Service Receiver Company"
              : "Administrator"}
          </div>
          <div>
            <Tag color={props.user.isActive ? "green" : "red"}>
              {props.user.isActive ? "Active" : "Blocked"}
            </Tag>
          </div>
        </div>
      </div>
    </div>

    <div className="text-center w-full md:w-auto">
      <Link to={`${props.pathname}/${props.user._id}`}>
        <Button type="default">View Details</Button>
      </Link>
    </div>
  </div>
);

export const SPIndUserDetails = (props: {
  user: IUserData;
  showModal: () => void;
  isSr: boolean;
}) => {
  const [files, setFiles] = useState<any[]>([]);

  useEffect(() => {
    setFiles([
      ...props.user.documentsUrl.map((file) =>
        Object.assign({
          preview: file,
          name: file.split("/")[file.split("/").length - 1],
        })
      ),
    ]);
  }, [props.user.documentsUrl]);

  return (
    <>
      <div className="flex flex-row flex-wrap justify-center items-center gap-4">
        <div className="">
          <Avatar
            size={150}
            className="border-2 border-gray-400"
            src={
              props.user.profilePicUrl === "" ||
              props.user.profilePicUrl === null
                ? "/images/profile_pic.jpg"
                : props.user.profilePicUrl
            }
          />
        </div>
        <div>
          <Typography.Title level={5}>Basic Info:</Typography.Title>
          <Descriptions
            size="small"
            column={{ xs: 1, sm: 1, md: 3 }}
            className="w-full bg-gray-100 p-2 rounded"
          >
            <Descriptions.Item label="First Name">
              <div className="font-semibold">{props.user.firstName}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Last Name">
              <div className="font-semibold">{props.user.lastName}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Email">
              <div className="font-semibold">{props.user.email}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Phone Number">
              <div className="font-semibold">{props.user.phoneNumber}</div>
            </Descriptions.Item>
            <Descriptions.Item label="NIDA">
              <div className="font-semibold">{props.user.NIDA}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Address">
              <div className="font-semibold">{props.user.streetAddress}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Ward">
              <div className="font-semibold">{props.user.ward}</div>
            </Descriptions.Item>
            <Descriptions.Item label="District">
              <div className="font-semibold">{props.user.district}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Region">
              <div className="font-semibold"> {props.user.region}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Reference Name">
              <div className="font-semibold">{props.user.refPersonName}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Reference Phone No.">
              <div className="font-semibold">{props.user.refPhoneNumber}</div>
            </Descriptions.Item>
            {!props.isSr && (
              <Descriptions.Item label="No. of Jobs Can Buy">
                <div className="font-semibold">
                  {props.user.jobsCanBuy}
                  <Button
                    size="small"
                    className="ml-2"
                    onClick={props.showModal}
                    type="primary"
                  >
                    Edit
                  </Button>
                </div>
              </Descriptions.Item>
            )}
            <Descriptions.Item label="Vocational Training">
              <div className="font-semibold">{props.user.vocationTraining}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Experience">
              <div className="font-semibold">
                {props.user.experience > 0
                  ? `${props.user.experience} Year(s)`
                  : "Nil"}
              </div>
            </Descriptions.Item>

            <Descriptions.Item label="Email Confirmed">
              <div className="font-semibold">
                {props.user.isEmailConfirm ? "Yes" : "No"}
              </div>
            </Descriptions.Item>
          </Descriptions>
          <div className="mt-4">
            <div className="font-semibold">Experience Details:</div>
            <div className="font-normal mx-6 text-justify">
              {props.user.expDetails}
            </div>
          </div>
          <Divider />
          <SelectedSkills jobSkills={props.user.skills} />
          <Divider />
          {!props.isSr && (
            <Typography.Title level={5}>Uploaded Documents:</Typography.Title>
          )}
          {!props.isSr ? (
            files.length > 0 ? (
              <ImageThumbs
                files={files}
                setFiles={setFiles}
                showDelete={false}
              />
            ) : (
              <Empty />
            )
          ) : null}
        </div>
      </div>
    </>
  );
};

export const SPComUserDetails = (props: {
  user: IUserData;
  showModal: () => void;
  isSr: boolean;
}) => {
  const [files, setFiles] = useState<any[]>([]);

  useEffect(() => {
    setFiles([
      ...props.user.documentsUrl.map((file) =>
        Object.assign({
          preview: file,
          name: file.split("/")[file.split("/").length - 1],
        })
      ),
    ]);
  }, [props.user.documentsUrl]);

  return (
    <>
      <div className="flex flex-row flex-wrap justify-center items-center gap-4">
        <div className="">
          <Avatar
            size={150}
            className="border-2 border-gray-400"
            src={
              props.user.profilePicUrl === "" ||
              props.user.profilePicUrl === null
                ? "/images/profile_pic.jpg"
                : props.user.profilePicUrl
            }
          />
        </div>
        <div>
          <Typography.Title level={5}>Basic Info:</Typography.Title>
          <Descriptions
            size="small"
            column={{ xs: 1, sm: 1, md: 3 }}
            className="w-full bg-gray-100 p-2 rounded"
          >
            <Descriptions.Item label="Company Name">
              <div className="font-semibold">{props.user.companyName}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Contact Person">
              <div className="font-semibold">{props.user.contactPerson}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Email">
              <div className="font-semibold">{props.user.email}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Phone Number">
              <div className="font-semibold">{props.user.phoneNumber}</div>
            </Descriptions.Item>
            <Descriptions.Item label="BRELA">
              <div className="font-semibold">{props.user.BRELA}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Address">
              <div className="font-semibold">{props.user.streetAddress}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Ward">
              <div className="font-semibold">{props.user.ward}</div>
            </Descriptions.Item>
            <Descriptions.Item label="District">
              <div className="font-semibold">{props.user.district}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Region">
              <div className="font-semibold"> {props.user.region}</div>
            </Descriptions.Item>
            {!props.isSr && (
              <Descriptions.Item label="No. of Jobs Can Buy">
                <div className="font-semibold">
                  {props.user.jobsCanBuy}
                  <Button
                    size="small"
                    className="ml-2"
                    onClick={props.showModal}
                    type="primary"
                  >
                    Edit
                  </Button>
                </div>
              </Descriptions.Item>
            )}
            <Descriptions.Item label="No of Employees">
              <div className="font-semibold">{props.user.noOfEmpoyees}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Experience">
              <div className="font-semibold">
                {props.user.experience > 0
                  ? `${props.user.experience} Year(s)`
                  : "Nil"}
              </div>
            </Descriptions.Item>

            <Descriptions.Item label="Email Confirmed">
              <div className="font-semibold">
                {props.user.isEmailConfirm ? "Yes" : "No"}
              </div>
            </Descriptions.Item>
          </Descriptions>
          <div className="mt-4">
            <div className="font-semibold">Experience Details:</div>
            <div className="font-normal mx-6 text-justify">
              {props.user.expDetails}
            </div>
          </div>
          <Divider />
          <SelectedSkills jobSkills={props.user.skills} />
          <Divider />
          {!props.isSr && (
            <Typography.Title level={5}>Uploaded Documents:</Typography.Title>
          )}
          {!props.isSr ? (
            files.length > 0 ? (
              <ImageThumbs
                files={files}
                setFiles={setFiles}
                showDelete={false}
              />
            ) : (
              <Empty />
            )
          ) : null}
        </div>
      </div>
    </>
  );
};

export const SRIndUserDetails = (props: {
  user: IUserData;
  small?: boolean;
}) => {
  return (
    <>
      <div className="flex flex-row flex-wrap justify-center items-center gap-4">
        <div className="">
          <Avatar
            size={props.small ? 100 : 150}
            className="border-2 border-gray-400"
            src={
              props.user.profilePicUrl === "" ||
              props.user.profilePicUrl === null
                ? "/images/profile_pic.jpg"
                : props.user.profilePicUrl
            }
          />
        </div>
        <div>
          {props.small ? null : (
            <Typography.Title level={5}>Basic Info:</Typography.Title>
          )}
          <Descriptions
            size="small"
            column={{ xs: 1, sm: 1, md: props.small ? 1 : 3 }}
            className="w-full bg-gray-100 p-2 rounded"
          >
            <Descriptions.Item label="First Name">
              <div className="font-semibold">{props.user.firstName}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Last Name">
              <div className="font-semibold">{props.user.lastName}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Email">
              <div className="font-semibold">{props.user.email}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Phone Number">
              <div className="font-semibold">{props.user.phoneNumber}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Address">
              <div className="font-semibold">{props.user.streetAddress}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Ward">
              <div className="font-semibold">{props.user.ward}</div>
            </Descriptions.Item>
            <Descriptions.Item label="District">
              <div className="font-semibold">{props.user.district}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Region">
              <div className="font-semibold"> {props.user.region}</div>
            </Descriptions.Item>

            {props.small ? null : (
              <Descriptions.Item label="Email Confirmed">
                <div className="font-semibold">
                  {props.user.isEmailConfirm ? "Yes" : "No"}
                </div>
              </Descriptions.Item>
            )}
          </Descriptions>
        </div>
      </div>
    </>
  );
};

export const SRComUserDetails = (props: {
  user: IUserData;
  small?: boolean;
}) => {
  return (
    <>
      <div className="flex flex-row flex-wrap justify-center items-center gap-4">
        <div className="">
          <Avatar
            size={props.small ? 100 : 150}
            className="border-2 border-gray-400"
            src={
              props.user.profilePicUrl === "" ||
              props.user.profilePicUrl === null
                ? "/images/profile_pic.jpg"
                : props.user.profilePicUrl
            }
          />
        </div>
        <div>
          <Typography.Title level={5}>Basic Info:</Typography.Title>
          <Descriptions
            size="small"
            column={{ xs: 1, sm: 1, md: props.small ? 1 : 3 }}
            className="w-full bg-gray-100 p-2 rounded"
          >
            <Descriptions.Item label="Company Name">
              <div className="font-semibold">{props.user.companyName}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Contact Person">
              <div className="font-semibold">{props.user.contactPerson}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Email">
              <div className="font-semibold">{props.user.email}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Phone Number">
              <div className="font-semibold">{props.user.phoneNumber}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Address">
              <div className="font-semibold">{props.user.streetAddress}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Ward">
              <div className="font-semibold">{props.user.ward}</div>
            </Descriptions.Item>
            <Descriptions.Item label="District">
              <div className="font-semibold">{props.user.district}</div>
            </Descriptions.Item>
            <Descriptions.Item label="Region">
              <div className="font-semibold"> {props.user.region}</div>
            </Descriptions.Item>

            {props.small ? null : (
              <Descriptions.Item label="Email Confirmed">
                <div className="font-semibold">
                  {props.user.isEmailConfirm ? "Yes" : "No"}
                </div>
              </Descriptions.Item>
            )}
          </Descriptions>
        </div>
      </div>
    </>
  );
};
