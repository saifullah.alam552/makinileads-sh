import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";
import {
  Alert,
  Avatar,
  Drawer,
  Dropdown,
  Grid,
  Layout as AntLayout,
  Menu
} from "antd";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useGetUserInfo } from "../hooks";
import { userLogoutAction } from "../store/Actions";
import { getUsername } from "../utils/helpers";
import Navigation from "./Navigation";

const { Header, Sider, Content } = AntLayout;
const { useBreakpoint } = Grid;

const Layout: React.FC = (props) => {
  const [collapsed, setCollapsed] = useState<boolean>(false);
  const [drawerVisible, setDrawerVisible] = useState<boolean>(false);
  const screens = useBreakpoint();
  const user = useGetUserInfo();
  const dispatch = useDispatch();

  const triggerClickHandle = () => {
    setCollapsed(!collapsed);
    setDrawerVisible(!drawerVisible);
  };

  const logoutClickHandle = () => {
    dispatch(userLogoutAction());
  };

  return (
    <>
      {user.forgetPassword && (
        <Alert
          message={
            <div className="text-center">
              You are using temparay password, please change it.
            </div>
          }
          type="error"
          closable
        />
      )}
      <AntLayout hasSider={true} className={"min-h-screen"}>
        {/* SLIDER */}
        {screens.lg && (
          <Sider
            width="260"
            collapsible={true}
            collapsed={collapsed}
            onCollapse={() => setCollapsed(!collapsed)}
            trigger={null}
            breakpoint="xl"
            collapsedWidth={!screens.lg ? 0 : 80}
            className={"bg-white shadow-md"}
          >
            <Navigation collapsed={collapsed} user={user} />
          </Sider>
        )}

        <AntLayout>
          {/* HEADER */}
          <Header
            className={
              "flex items-center justify-between bg-white shadow-md pt-0 pb-0 pr-4 pl-4"
            }
          >
            {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: triggerClickHandle,
              }
            )}

            <Dropdown overlay={menu(logoutClickHandle)} trigger={["click"]}>
              <div
                className={
                  "flex items-center hover:bg-gray-100 px-2 py-4 transition-colors"
                }
              >
                <div
                  className={
                    "font-bold text-sm mr-2 overflow-hidden whitespace-nowrap overflow-ellipsis max-w-xs"
                  }
                >
                  {getUsername(user)}
                </div>
                <Avatar
                  size="default"
                  src={
                    user.profilePicUrl !== null && user.profilePicUrl !== ""
                      ? user.profilePicUrl
                      : "/images/profile_pic.jpg"
                  }
                  className="border-2"
                />
              </div>
            </Dropdown>
          </Header>

          {/* CONTENT BODY */}
          <Content className={"m-0 pb-10"}>{props.children}</Content>
        </AntLayout>

        {/* DRAWER */}
        {!screens.lg && (
          <Drawer
            placement="left"
            closable={true}
            onClose={() => setDrawerVisible(false)}
            visible={drawerVisible}
            className={"p-0"}
          >
            <Navigation
              collapsed={false}
              drawerClose={setDrawerVisible}
              user={user}
            />
          </Drawer>
        )}
      </AntLayout>
    </>
  );
};

export default Layout;

const menu = (logoutClickHandle: () => void) => (
  <Menu>
    <Menu.Item key="0" onClick={logoutClickHandle}>
      Logout
    </Menu.Item>
  </Menu>
);
