import { faChevronCircleDown } from "@fortawesome/free-solid-svg-icons/faChevronCircleDown";
import { faChevronCircleUp } from "@fortawesome/free-solid-svg-icons/faChevronCircleUp";
import { faTimes } from "@fortawesome/free-solid-svg-icons/faTimes";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Divider, Radio } from "antd";
import dayjs from "dayjs";
import React, { SetStateAction, useEffect, useState } from "react";
import DatePicker from "../antd/DatePicker";
import SelectField from "../antd/SelectField";
import { useGetSelectSubCategories, useGetSelectUsers } from "../hooks";
import { JobPurchaseType, JobStatusEnum, UserTypeEnum } from "../types";
import { JobQuery } from "../utils/apiUrls";
import ModalReact from "./ModalReact";

interface FilterProps {
  showFilter: boolean;
  setShowFilter: (value: SetStateAction<boolean>) => void;
  rq: JobQuery;
  setRq: (value: SetStateAction<JobQuery>) => void;
  userId: string;
  userType?: UserTypeEnum;
  total: number;
  fresh: () => void;
}

export function JobFilterOptions({
  rq,
  setRq,
  setShowFilter,
  showFilter,
  userId,
  userType,
  total,
  fresh,
}: FilterProps) {
  const [uType, setUType] = useState<"SP" | "SR" | "ADMIN">("SP");

  useEffect(() => {
    if (userType === UserTypeEnum.ADMIN) {
      setUType("ADMIN");
      setRq({ summary: true, limit: 12, skip: 0 });
    }

    if (userType === UserTypeEnum.SP_COM || userType === UserTypeEnum.SP_IND) {
      setUType("SP");
      setRq({ summary: true, limit: 12, skip: 0 });
    }

    if (userType === UserTypeEnum.SR_COM || userType === UserTypeEnum.SR_IND) {
      setUType("SR");
      setRq({ summary: true, limit: 12, skip: 0, postedById: userId });
    }
  }, [setRq, userId, userType]);

  return uType === "ADMIN" ? (
    <AdminJobFilter
      showFilter={showFilter}
      setShowFilter={setShowFilter}
      rq={rq}
      setRq={setRq}
      userId={userId}
      total={total}
      fresh={fresh}
    />
  ) : uType === "SP" ? (
    <SPJobFilter
      showFilter={showFilter}
      setShowFilter={setShowFilter}
      rq={rq}
      setRq={setRq}
      userId={userId}
      total={total}
      fresh={fresh}
    />
  ) : uType === "SR" ? (
    <SRJobFilter
      showFilter={showFilter}
      setShowFilter={setShowFilter}
      rq={rq}
      setRq={setRq}
      userId={userId}
      total={total}
      fresh={fresh}
    />
  ) : null;
}

const SPJobFilter = ({
  setRq,
  setShowFilter,
  showFilter,
  userId,
  total,
  fresh,
}: FilterProps) => {
  const [jobType, setJobType] = useState<
    | "ALL"
    | "NONE"
    | "USER_ALL"
    | "USER_EXC"
    | "USER_GEN"
    | "GEN"
    | "EXC"
    | "COMP"
    | "CON"
    | "PEND"
    | "USER_COMP"
    | "USER_CON"
    | "USER_PEND"
  >("ALL");

  const [postedDate, setPostedDate] = useState<{ start: string; end: string }>({
    start: "",
    end: "",
  });

  const [sortBy, setSortBy] = useState<{ field: string; order: string }>({
    field: "createdAt",
    order: "DESC",
  });

  const [jobStatus, setJobStatus] = useState<"true" | "false" | "all">("all");
  const [skills, setSkills] = useState<string>();

  const skillsSelect = useGetSelectSubCategories();

  useEffect(() => {
    setJobType("ALL");
    setSortBy({
      field: "createdAt",
      order: "DESC",
    });
    setPostedDate({
      start: "",
      end: "",
    });
  }, []);

  useEffect(() => {
    if (showFilter)
      setPostedDate({
        start: "",
        end: "",
      });
  }, [showFilter]);

  const sortData = [
    {
      key: "createdAt",
      label: "Posted Date",
      disabled: true,
    },
    {
      key: "jobPurchaseType",
      label: "Purchased Types",
      disabled: true,
    },
  ];

  const sendFilter = () => {
    let query: JobQuery = { summary: true, limit: 12, skip: 0 };

    switch (jobType) {
      case "ALL":
        query = { ...query };
        break;
      case "NONE":
        query = { ...query, purchaseType: JobPurchaseType.NONE };
        break;
      case "GEN":
        query = { ...query, purchaseType: JobPurchaseType.GENERAL };
        break;
      case "EXC":
        query = { ...query, purchaseType: JobPurchaseType.EXCLUSIVE };
        break;
      case "COMP":
        query = { ...query, status: JobStatusEnum.COMPLETED };
        break;
      case "CON":
        query = { ...query, status: JobStatusEnum.CONTACTED };
        break;
      case "PEND":
        query = { ...query, status: JobStatusEnum.PURCHASED };
        break;
      case "USER_ALL":
        query = { ...query, purchasedById: userId };
        break;
      case "USER_EXC":
        query = {
          ...query,
          purchasedById: userId,
          purchaseType: JobPurchaseType.EXCLUSIVE,
        };
        break;
      case "USER_GEN":
        query = {
          ...query,
          purchasedById: userId,
          purchaseType: JobPurchaseType.GENERAL,
        };
        break;
      case "USER_COMP":
        query = {
          ...query,
          purchasedById: userId,
          status: JobStatusEnum.COMPLETED,
        };
        break;
      case "USER_CON":
        query = {
          ...query,
          purchasedById: userId,
          status: JobStatusEnum.CONTACTED,
        };
        break;
      case "USER_PEND":
        query = {
          ...query,
          purchasedById: userId,
          status: JobStatusEnum.PURCHASED,
        };
        break;
      default:
        query = { ...query };
        break;
    }

    switch (jobStatus) {
      case "all":
        query = { ...query, isActive: undefined };
        break;
      case "true":
        query = { ...query, isActive: "true" };
        break;
      case "false":
        query = { ...query, isActive: "false" };
        break;
      default:
        query = { ...query };
        break;
    }

    query = {
      ...query,
      sortBy: `${sortBy.order === "DESC" ? `-${sortBy.field}` : sortBy.field}`,
      postedDateStart: postedDate.start,
      postedDateEnd: postedDate.end,
      skillId: skills,
    };

    setRq(query);
    setShowFilter(false);
  };

  const clear = () => {
    setJobType("ALL");
    setSortBy({
      field: "createdAt",
      order: "DESC",
    });
    setPostedDate({
      start: "",
      end: "",
    });
    setJobStatus("all");
    setSkills(undefined);
    fresh();
  };

  return (
    <>
      <div className="filter-for-all flex flex-row gap-2 justify-between text-md mb-4 py-2 px-4 bg-gray-700 text-white rounded shadow-md">
        <div className="flex flex-row flex-wrap">
          <div>
            Jobs:{" "}
            <b>
              {jobType === "ALL" ? "All" : null}
              {jobType === "NONE" ? "Not Purchased" : null}
              {jobType === "EXC" ? "Exclusive Purchased" : null}
              {jobType === "GEN" ? "General Purchased" : null}
              {jobType === "COMP" ? "Completed" : null}
              {jobType === "CON" ? "Contacted" : null}
              {jobType === "PEND" ? "Pending" : null}
              {jobType === "USER_ALL" ? "My All Jobs Purchased" : null}
              {jobType === "USER_EXC" ? "My Exclusive Purchased" : null}
              {jobType === "USER_GEN" ? "My General Purchased" : null}
              {jobType === "USER_COMP" ? "My Completed Jobs" : null}
              {jobType === "USER_CON" ? "My Contacted Jobs" : null}
              {jobType === "USER_PEND" ? "My Pending Jobs" : null}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
          <div>
            Sorted By:{" "}
            <b>
              {sortData.filter((d) => d.key === sortBy.field)[0]["label"]}{" "}
              {sortBy.order === "ASC" ? (
                <FontAwesomeIcon icon={faChevronCircleUp} />
              ) : (
                <FontAwesomeIcon icon={faChevronCircleDown} />
              )}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
          <div>
            Posted Date:{" "}
            <b>
              {postedDate.start !== ""
                ? `${dayjs(postedDate.start).format("DD-MMM-YYYY")}
                to
                ${dayjs(postedDate.end).format("DD-MMM-YYYY")}`
                : "All"}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
          <div>
            Active/Blocked:{" "}
            <b>
              {jobStatus === "all" ? "Both" : null}
              {jobStatus === "true" ? "Active Jobs" : null}
              {jobStatus === "false" ? "Blocked Jobs" : null}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
          <div>
            Skill:{" "}
            <b>
              {skills
                ? skillsSelect.filter((d) => d.key === skills)[0].label
                : "All"}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
        </div>
        <div className="flex flex-row gap-2">
          <div className="border-1 w-10 px-4">
            <Button onClick={() => clear()} size="small" danger type="primary">
              <FontAwesomeIcon icon={faTimes} />
            </Button>
          </div>
          <div className="border-l-2 pl-2">
            Total: <b>{total} Jobs</b>
          </div>
        </div>
      </div>
      <ModalReact
        id="sp-job-filter-options"
        isOpen={showFilter}
        onClose={() => setShowFilter(false)}
        body={
          <div
            className="flex flex-col space-y-8"
            style={{ maxWidth: "625px" }}
          >
            <div className="flex flex-col">
              <div>
                <Divider
                  className="font-bold"
                  style={{ marginTop: "0px", paddingTop: "0" }}
                >
                  Jobs
                </Divider>
                <div>
                  <Radio.Group
                    className="flex flex-row flex-wrap gap-1 items-center justify-center"
                    value={jobType}
                    onChange={(e) => setJobType(e.target.value)}
                    buttonStyle="solid"
                  >
                    <Radio.Button value={"ALL"}>All Jobs</Radio.Button>
                    <Radio.Button value={"NONE"}>Not Purchased</Radio.Button>
                    <Radio.Button value={"EXC"}>All Exclusive</Radio.Button>
                    <Radio.Button value={"GEN"}>All General</Radio.Button>
                    <Radio.Button value={"COMP"}>Completed Jobs</Radio.Button>
                    <Radio.Button value={"CON"}>Contacted Jobs</Radio.Button>
                    <Radio.Button value={"PEND"}>Pending Jobs</Radio.Button>
                    <Divider
                      style={{
                        fontSize: "12px",
                        marginBottom: "2px",
                        marginTop: "2px",
                        width: "50px",
                      }}
                    >
                      By Me
                    </Divider>
                    <Radio.Button value={"USER_ALL"}>Purchased</Radio.Button>
                    <Radio.Button value={"USER_EXC"}>Exclusive</Radio.Button>
                    <Radio.Button value={"USER_GEN"}>General</Radio.Button>
                    <Radio.Button value={"USER_COMP"}>Completed</Radio.Button>
                    <Radio.Button value={"USER_CON"}>Contacted</Radio.Button>
                    <Radio.Button value={"USER_PEND"}>Pending</Radio.Button>
                  </Radio.Group>
                </div>
              </div>

              <div className="grid grid-cols-1 md:grid-cols-3 gap-3">
                <div className="col-span-2">
                  <Divider className="font-bold" style={{ marginTop: "10px" }}>
                    Sort By
                  </Divider>
                  <div className="grid grid-cols-3 gap-x-1">
                    <div className="col-span-2">
                      <SelectField
                        showSearch={false}
                        id={"sortByField"}
                        value={sortBy.field}
                        onChange={(value) =>
                          setSortBy({ ...sortBy, field: value })
                        }
                        required={false}
                        data={sortData}
                        displayError={false}
                      />
                    </div>
                    <div>
                      <SelectField
                        showSearch={false}
                        id={"sortByOrder"}
                        value={sortBy.order}
                        onChange={(value) =>
                          setSortBy({ ...sortBy, order: value })
                        }
                        required={false}
                        data={[
                          {
                            key: "ASC",
                            label: "Ascending",
                            disabled: true,
                          },
                          {
                            key: "DESC",
                            label: "Descending",
                            disabled: true,
                          },
                        ]}
                        displayError={false}
                      />
                    </div>
                  </div>
                </div>

                <div>
                  <Divider className="font-bold" style={{ marginTop: "10px" }}>
                    Status
                  </Divider>
                  <div>
                    <Radio.Group
                      className="flex flex-row items-center justify-center"
                      value={jobStatus}
                      onChange={(e) => setJobStatus(e.target.value)}
                      buttonStyle="solid"
                    >
                      <Radio.Button value={"all"}>All</Radio.Button>
                      <Radio.Button value={"true"}>Active</Radio.Button>
                      <Radio.Button value={"false"}>Blocked</Radio.Button>
                    </Radio.Group>
                  </div>
                </div>
              </div>

              <div className="grid grid-cols-1 md:grid-cols-2 gap-2">
                <div className="">
                  <Divider className="font-bold" style={{ marginTop: "10px" }}>
                    Posted Date
                  </Divider>
                  <div>
                    <DatePicker.RangePicker
                      allowClear={true}
                      className="w-full"
                      format={"DD-MMM-YYYY"}
                      disabledDate={(date) =>
                        date && date > dayjs().endOf("day")
                      }
                      onChange={(values, formatString) => {
                        setPostedDate({
                          start: dayjs(formatString[0]).format("YYYY-MM-DD"),
                          end: dayjs(formatString[1]).format("YYYY-MM-DD"),
                        });
                      }}
                    />
                  </div>
                </div>

                <div>
                  <Divider className="font-bold" style={{ marginTop: "10px" }}>
                    Skill
                  </Divider>
                  <SelectField
                    id={"skills"}
                    allowClear={true}
                    value={skills}
                    placeholder="All Skills"
                    onChange={(value) => setSkills(value)}
                    required={false}
                    data={skillsSelect}
                    displayError={false}
                  />
                </div>
              </div>
            </div>

            <div className="text-right">
              <Button type="primary" onClick={sendFilter}>
                Filter
              </Button>
            </div>
          </div>
        }
      />
    </>
  );
};

const SRJobFilter = ({
  setRq,
  setShowFilter,
  showFilter,
  userId,
  total,
  fresh,
}: FilterProps) => {
  const [jobType, setJobType] = useState<
    | "USER_GEN"
    | "USER_NONE"
    | "USER_ALL"
    | "USER_EXC"
    | "USER_COMP"
    | "USER_CON"
    | "USER_PEND"
  >("USER_ALL");

  const [postedDate, setPostedDate] = useState<{ start: string; end: string }>({
    start: "",
    end: "",
  });

  const [sortBy, setSortBy] = useState<{ field: string; order: string }>({
    field: "createdAt",
    order: "DESC",
  });

  const [jobStatus, setJobStatus] = useState<"true" | "false" | "all">("all");
  const [skills, setSkills] = useState<string>();

  const skillsSelect = useGetSelectSubCategories();

  useEffect(() => {
    setJobType("USER_ALL");
    setSortBy({
      field: "createdAt",
      order: "DESC",
    });
    setPostedDate({
      start: "",
      end: "",
    });
  }, []);

  useEffect(() => {
    if (showFilter)
      setPostedDate({
        start: "",
        end: "",
      });
  }, [showFilter]);

  const sortData = [
    {
      key: "createdAt",
      label: "Posted Date",
      disabled: true,
    },
    {
      key: "jobPurchaseType",
      label: "Purchased Types",
      disabled: true,
    },
  ];

  const sendFilter = () => {
    let query: JobQuery = {
      summary: true,
      limit: 12,
      skip: 0,
      postedById: userId,
    };

    switch (jobType) {
      case "USER_ALL":
        query = { ...query };
        break;
      case "USER_NONE":
        query = { ...query, purchaseType: JobPurchaseType.NONE };
        break;
      case "USER_EXC":
        query = {
          ...query,
          purchaseType: JobPurchaseType.EXCLUSIVE,
        };
        break;
      case "USER_GEN":
        query = {
          ...query,
          purchaseType: JobPurchaseType.GENERAL,
        };
        break;
      case "USER_COMP":
        query = {
          ...query,
          status: JobStatusEnum.COMPLETED,
        };
        break;
      case "USER_CON":
        query = {
          ...query,
          status: JobStatusEnum.CONTACTED,
        };
        break;
      case "USER_PEND":
        query = {
          ...query,
          status: JobStatusEnum.PURCHASED,
        };
        break;
      default:
        query = { ...query };
        break;
    }

    switch (jobStatus) {
      case "all":
        query = { ...query, isActive: undefined };
        break;
      case "true":
        query = { ...query, isActive: "true" };
        break;
      case "false":
        query = { ...query, isActive: "false" };
        break;
      default:
        query = { ...query };
        break;
    }

    query = {
      ...query,
      sortBy: `${sortBy.order === "DESC" ? `-${sortBy.field}` : sortBy.field}`,
      postedDateStart: postedDate.start,
      postedDateEnd: postedDate.end,
      skillId: skills,
    };

    setRq(query);
    setShowFilter(false);
  };

  const clear = () => {
    setJobType("USER_ALL");
    setSortBy({
      field: "createdAt",
      order: "DESC",
    });
    setPostedDate({
      start: "",
      end: "",
    });
    setJobStatus("all");
    setSkills(undefined);
    fresh();
  };

  return (
    <>
      <div className="filter-for-all flex flex-row gap-2 justify-between text-md mb-4 py-2 px-4 bg-gray-700 text-white rounded shadow-md">
        <div className="flex flex-row flex-wrap">
          <div>
            Jobs:{" "}
            <b>
              {jobType === "USER_ALL" ? "All" : null}
              {jobType === "USER_EXC" ? "Exclusive Purchased" : null}
              {jobType === "USER_GEN" ? "General Purchased" : null}
              {jobType === "USER_NONE" ? "Not Purchased" : null}
              {jobType === "USER_COMP" ? "Completed Jobs" : null}
              {jobType === "USER_CON" ? "Contacted Jobs" : null}
              {jobType === "USER_PEND" ? "Pending Jobs" : null}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
          <div>
            Sorted By:{" "}
            <b>
              {sortData.filter((d) => d.key === sortBy.field)[0]["label"]}{" "}
              {sortBy.order === "ASC" ? (
                <FontAwesomeIcon icon={faChevronCircleUp} />
              ) : (
                <FontAwesomeIcon icon={faChevronCircleDown} />
              )}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
          <div>
            Posted Date:{" "}
            <b>
              {postedDate.start !== ""
                ? `${dayjs(postedDate.start).format("DD-MMM-YYYY")}
             to
            ${dayjs(postedDate.end).format("DD-MMM-YYYY")}`
                : "All"}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
          <div>
            Active/Blocked:{" "}
            <b>
              {jobStatus === "all" ? "Both" : null}
              {jobStatus === "true" ? "Active Jobs" : null}
              {jobStatus === "false" ? "Blocked Jobs" : null}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
          <div>
            Skill:{" "}
            <b>
              {skills
                ? skillsSelect.filter((d) => d.key === skills)[0].label
                : "All"}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
        </div>
        <div className="flex flex-row gap-2">
          <div className="border-1 w-10 px-4">
            <Button onClick={() => clear()} size="small" danger type="primary">
              <FontAwesomeIcon icon={faTimes} />
            </Button>
          </div>
          <div className="border-l-2 pl-2">
            Total: <b>{total} Jobs</b>
          </div>
        </div>
      </div>
      <ModalReact
        id="sp-job-filter-options"
        isOpen={showFilter}
        onClose={() => setShowFilter(false)}
        body={
          <div
            className="flex flex-col space-y-8"
            style={{ maxWidth: "625px" }}
          >
            <div className="flex flex-col">
              <div>
                <Divider
                  className="font-bold"
                  style={{ marginTop: "0px", paddingTop: "0" }}
                >
                  Jobs
                </Divider>
                <div>
                  <Radio.Group
                    className="flex flex-row flex-wrap gap-1 items-center justify-center"
                    value={jobType}
                    onChange={(e) => setJobType(e.target.value)}
                    buttonStyle="solid"
                  >
                    <Radio.Button value={"USER_ALL"}>All Posted</Radio.Button>
                    <Radio.Button value={"USER_EXC"}>Exclusive</Radio.Button>
                    <Radio.Button value={"USER_GEN"}>General</Radio.Button>
                    <Radio.Button value={"USER_NONE"}>
                      Not Purchased
                    </Radio.Button>
                    <Radio.Button value={"USER_COMP"}>Completed</Radio.Button>
                    <Radio.Button value={"USER_CON"}>Contacted</Radio.Button>
                    <Radio.Button value={"USER_PEND"}>Pending</Radio.Button>
                  </Radio.Group>
                </div>
              </div>

              <div className="grid grid-cols-1 md:grid-cols-3 gap-3">
                <div className="col-span-2">
                  <Divider className="font-bold" style={{ marginTop: "10px" }}>
                    Sort By
                  </Divider>
                  <div className="grid grid-cols-3 gap-x-1">
                    <div className="col-span-2">
                      <SelectField
                        showSearch={false}
                        id={"sortByField"}
                        value={sortBy.field}
                        onChange={(value) =>
                          setSortBy({ ...sortBy, field: value })
                        }
                        required={false}
                        data={sortData}
                        displayError={false}
                      />
                    </div>
                    <div>
                      <SelectField
                        showSearch={false}
                        id={"sortByOrder"}
                        value={sortBy.order}
                        onChange={(value) =>
                          setSortBy({ ...sortBy, order: value })
                        }
                        required={false}
                        data={[
                          {
                            key: "ASC",
                            label: "Ascending",
                            disabled: true,
                          },
                          {
                            key: "DESC",
                            label: "Descending",
                            disabled: true,
                          },
                        ]}
                        displayError={false}
                      />
                    </div>
                  </div>
                </div>

                <div>
                  <Divider className="font-bold" style={{ marginTop: "10px" }}>
                    Status
                  </Divider>
                  <div>
                    <Radio.Group
                      className="flex flex-row items-center justify-center"
                      value={jobStatus}
                      onChange={(e) => setJobStatus(e.target.value)}
                      buttonStyle="solid"
                    >
                      <Radio.Button value={"all"}>All</Radio.Button>
                      <Radio.Button value={"true"}>Active</Radio.Button>
                      <Radio.Button value={"false"}>Blocked</Radio.Button>
                    </Radio.Group>
                  </div>
                </div>
              </div>

              <div className="grid grid-cols-1 md:grid-cols-2 gap-2">
                <div className="">
                  <Divider className="font-bold" style={{ marginTop: "10px" }}>
                    Posted Date
                  </Divider>
                  <div>
                    <DatePicker.RangePicker
                      allowClear={true}
                      className="w-full"
                      format={"DD-MMM-YYYY"}
                      disabledDate={(date) =>
                        date && date > dayjs().endOf("day")
                      }
                      onChange={(values, formatString) => {
                        setPostedDate({
                          start: dayjs(formatString[0]).format("YYYY-MM-DD"),
                          end: dayjs(formatString[1]).format("YYYY-MM-DD"),
                        });
                      }}
                    />
                  </div>
                </div>

                <div>
                  <Divider className="font-bold" style={{ marginTop: "10px" }}>
                    Skill
                  </Divider>
                  <SelectField
                    id={"skills"}
                    allowClear={true}
                    value={skills}
                    placeholder="All Skills"
                    onChange={(value) => setSkills(value)}
                    required={false}
                    data={skillsSelect}
                    displayError={false}
                  />
                </div>
              </div>
            </div>

            <div className="text-right">
              <Button type="primary" onClick={sendFilter}>
                Filter
              </Button>
            </div>
          </div>
        }
      />
    </>
  );
};

const AdminJobFilter = ({
  setRq,
  setShowFilter,
  showFilter,
  userId,
  total,
  fresh,
}: FilterProps) => {
  const [jobType, setJobType] = useState<
    "ALL" | "NONE" | "GEN" | "EXC" | "COMP" | "CON" | "PEND"
  >("ALL");

  const [postedDate, setPostedDate] = useState<{ start: string; end: string }>({
    start: "",
    end: "",
  });

  const [sortBy, setSortBy] = useState<{ field: string; order: string }>({
    field: "createdAt",
    order: "DESC",
  });

  const [jobStatus, setJobStatus] = useState<"true" | "false" | "all">("all");
  const [skills, setSkills] = useState<string>();
  const [postedBy, setPostedBy] = useState<string>();
  const [purchasedBy, setPurchasedBy] = useState<string>();

  const skillsSelect = useGetSelectSubCategories();
  const { spSelect, srSelect } = useGetSelectUsers();

  useEffect(() => {
    setJobType("ALL");
    setSortBy({
      field: "createdAt",
      order: "DESC",
    });
    setPostedDate({
      start: "",
      end: "",
    });
    setJobStatus("all");
    setSkills(undefined);
    setPostedBy(undefined);
    setPurchasedBy(undefined);
  }, []);

  useEffect(() => {
    if (showFilter)
      setPostedDate({
        start: "",
        end: "",
      });
  }, [showFilter]);

  const sortData = [
    {
      key: "createdAt",
      label: "Posted Date",
      disabled: true,
    },
    {
      key: "jobPurchaseType",
      label: "Purchased Types",
      disabled: true,
    },
  ];

  const sendFilter = () => {
    let query: JobQuery = { summary: true, limit: 12, skip: 0 };

    switch (jobType) {
      case "ALL":
        query = { ...query };
        break;
      case "NONE":
        query = { ...query, purchaseType: JobPurchaseType.NONE };
        break;
      case "GEN":
        query = { ...query, purchaseType: JobPurchaseType.GENERAL };
        break;
      case "EXC":
        query = { ...query, purchaseType: JobPurchaseType.EXCLUSIVE };
        break;
      case "COMP":
        query = { ...query, status: JobStatusEnum.COMPLETED };
        break;
      case "CON":
        query = { ...query, status: JobStatusEnum.CONTACTED };
        break;
      case "PEND":
        query = { ...query, status: JobStatusEnum.PURCHASED };
        break;
      default:
        query = { ...query };
        break;
    }

    switch (jobStatus) {
      case "all":
        query = { ...query, isActive: undefined };
        break;
      case "true":
        query = { ...query, isActive: "true" };
        break;
      case "false":
        query = { ...query, isActive: "false" };
        break;
      default:
        query = { ...query };
        break;
    }

    query = {
      ...query,
      sortBy: `${sortBy.order === "DESC" ? `-${sortBy.field}` : sortBy.field}`,
      postedDateStart: postedDate.start,
      postedDateEnd: postedDate.end,
      postedById: postedBy,
      purchasedById: purchasedBy,
      skillId: skills,
    };

    setRq(query);
    setShowFilter(false);
  };

  const clear = () => {
    setJobType("ALL");
    setSortBy({
      field: "createdAt",
      order: "DESC",
    });
    setPostedDate({
      start: "",
      end: "",
    });
    setJobStatus("all");
    setSkills(undefined);
    setPostedBy(undefined);
    setPurchasedBy(undefined);
    fresh();
  };

  return (
    <>
      <div className="filter-for-all flex flex-row gap-2 justify-between text-md mb-4 py-2 px-4 bg-gray-700 text-white rounded shadow-md">
        <div className="flex flex-row flex-wrap">
          <div>
            Jobs:{" "}
            <b>
              {jobType === "ALL" ? "All" : null}
              {jobType === "NONE" ? "Not Purchased" : null}
              {jobType === "EXC" ? "Exclusive Purchased" : null}
              {jobType === "GEN" ? "General Purchased" : null}
              {jobType === "COMP" ? "Completed" : null}
              {jobType === "CON" ? "Contacted" : null}
              {jobType === "PEND" ? "Pending" : null}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
          <div>
            Sorted By:{" "}
            <b>
              {sortData.filter((d) => d.key === sortBy.field)[0]["label"]}{" "}
              {sortBy.order === "ASC" ? (
                <FontAwesomeIcon icon={faChevronCircleUp} />
              ) : (
                <FontAwesomeIcon icon={faChevronCircleDown} />
              )}
              <Divider type="vertical" className="border-white" />
            </b>
          </div>
          <div>
            Posted Date:{" "}
            <b>
              {postedDate.start !== ""
                ? `${dayjs(postedDate.start).format("DD-MMM-YYYY")}
                to
                ${dayjs(postedDate.end).format("DD-MMM-YYYY")}`
                : "All"}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
          <div>
            Active/Blocked:{" "}
            <b>
              {jobStatus === "all" ? "Both" : null}
              {jobStatus === "true" ? "Active Jobs" : null}
              {jobStatus === "false" ? "Blocked Jobs" : null}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
          <div>
            Skill:{" "}
            <b>
              {skills
                ? skillsSelect.filter((d) => d.key === skills)[0].label
                : "All"}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
          <div>
            Posted By:{" "}
            <b>
              {postedBy
                ? srSelect.filter((d) => d.key === postedBy)[0].label
                : "All"}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
          <div>
            Purchased By:{" "}
            <b>
              {purchasedBy
                ? spSelect.filter((d) => d.key === purchasedBy)[0].label
                : "All"}
            </b>
            <Divider type="vertical" className="border-white" />
          </div>
        </div>
        <div className="flex flex-row gap-2">
          <div className="border-1 w-10 px-4">
            <Button onClick={() => clear()} size="small" danger type="primary">
              <FontAwesomeIcon icon={faTimes} />
            </Button>
          </div>
          <div className="border-l-2 pl-2">
            Total: <b>{total} Jobs</b>
          </div>
        </div>
      </div>
      <ModalReact
        id="sp-job-filter-options"
        isOpen={showFilter}
        onClose={() => setShowFilter(false)}
        body={
          <div
            className="flex flex-col space-y-8"
            style={{ maxWidth: "625px" }}
          >
            <div className="flex flex-col">
              <div>
                <Divider
                  className="font-bold"
                  style={{ marginTop: "0px", paddingTop: "0" }}
                >
                  Jobs
                </Divider>
                <div>
                  <Radio.Group
                    className="flex flex-row flex-wrap gap-1 items-center justify-center"
                    value={jobType}
                    onChange={(e) => setJobType(e.target.value)}
                    buttonStyle="solid"
                  >
                    <Radio.Button value={"ALL"}>All Jobs</Radio.Button>
                    <Radio.Button value={"NONE"}>Not Purchased</Radio.Button>
                    <Radio.Button value={"EXC"}>All Exclusive</Radio.Button>
                    <Radio.Button value={"GEN"}>All General</Radio.Button>
                    <Radio.Button value={"COMP"}>Completed Jobs</Radio.Button>
                    <Radio.Button value={"CON"}>Contacted Jobs</Radio.Button>
                    <Radio.Button value={"PEND"}>Pending Jobs</Radio.Button>
                  </Radio.Group>
                </div>
              </div>

              <div className="grid grid-cols-1 md:grid-cols-2 gap-2">
                <div className="">
                  <Divider className="font-bold" style={{ marginTop: "10px" }}>
                    Purchased By
                  </Divider>
                  <SelectField
                    id={"sp-users"}
                    allowClear={true}
                    value={purchasedBy}
                    placeholder="All Service Providers"
                    onChange={(value) => setPurchasedBy(value)}
                    required={false}
                    data={spSelect}
                    displayError={false}
                  />
                </div>

                <div>
                  <Divider className="font-bold" style={{ marginTop: "10px" }}>
                    Posted By
                  </Divider>
                  <SelectField
                    id={"sr-users"}
                    allowClear={true}
                    value={postedBy}
                    placeholder="All Service Receivers"
                    onChange={(value) => setPostedBy(value)}
                    required={false}
                    data={srSelect}
                    displayError={false}
                  />
                </div>
              </div>

              <div className="grid grid-cols-1 md:grid-cols-3 gap-3">
                <div className="col-span-2">
                  <Divider className="font-bold" style={{ marginTop: "10px" }}>
                    Sort By
                  </Divider>
                  <div className="grid grid-cols-3 gap-x-1">
                    <div className="col-span-2">
                      <SelectField
                        showSearch={false}
                        id={"sortByField"}
                        value={sortBy.field}
                        onChange={(value) =>
                          setSortBy({ ...sortBy, field: value })
                        }
                        required={false}
                        data={sortData}
                        displayError={false}
                      />
                    </div>
                    <div>
                      <SelectField
                        showSearch={false}
                        id={"sortByOrder"}
                        value={sortBy.order}
                        onChange={(value) =>
                          setSortBy({ ...sortBy, order: value })
                        }
                        required={false}
                        data={[
                          {
                            key: "ASC",
                            label: "Ascending",
                            disabled: true,
                          },
                          {
                            key: "DESC",
                            label: "Descending",
                            disabled: true,
                          },
                        ]}
                        displayError={false}
                      />
                    </div>
                  </div>
                </div>

                <div>
                  <Divider className="font-bold" style={{ marginTop: "10px" }}>
                    Status
                  </Divider>
                  <div>
                    <Radio.Group
                      className="flex flex-row items-center justify-center"
                      value={jobStatus}
                      onChange={(e) => setJobStatus(e.target.value)}
                      buttonStyle="solid"
                    >
                      <Radio.Button value={"all"}>All</Radio.Button>
                      <Radio.Button value={"true"}>Active</Radio.Button>
                      <Radio.Button value={"false"}>Blocked</Radio.Button>
                    </Radio.Group>
                  </div>
                </div>
              </div>

              <div className="grid grid-cols-1 md:grid-cols-2 gap-2">
                <div className="">
                  <Divider className="font-bold" style={{ marginTop: "10px" }}>
                    Posted Date
                  </Divider>
                  <div>
                    <DatePicker.RangePicker
                      allowClear={true}
                      className="w-full"
                      format={"DD-MMM-YYYY"}
                      disabledDate={(date) =>
                        date && date > dayjs().endOf("day")
                      }
                      onChange={(values, formatString) => {
                        setPostedDate({
                          start: dayjs(formatString[0]).format("YYYY-MM-DD"),
                          end: dayjs(formatString[1]).format("YYYY-MM-DD"),
                        });
                      }}
                    />
                  </div>
                </div>

                <div>
                  <Divider className="font-bold" style={{ marginTop: "10px" }}>
                    Skill
                  </Divider>
                  <SelectField
                    id={"skills"}
                    allowClear={true}
                    value={skills}
                    placeholder="All Skills"
                    onChange={(value) => setSkills(value)}
                    required={false}
                    data={skillsSelect}
                    displayError={false}
                  />
                </div>
              </div>
            </div>

            <div className="text-right">
              <Button type="primary" onClick={sendFilter}>
                Filter
              </Button>
            </div>
          </div>
        }
      />
    </>
  );
};
