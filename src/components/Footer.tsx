import { motion } from "framer-motion";
import React from "react";
import { Link } from "react-router-dom";

export default function Footer() {
  return (
    <motion.div className="bg-white p-5 mt-20 overflow-hidden">
      <motion.div
        className="container mx-auto text-gray-600 "
        initial={{ y: 100 }}
        animate={{ y: 0 }}
        transition={{ duration: 1 }}
        exit={{ opacity: 0 }}
      >
        <motion.div
          className="flex flex-row flex-wrap items-center justify-center"
          transition={{ staggerChildren: 0.5 }}
        >
          <FooterLink to="#" title="About Us" />
          <FooterLink to="#" title="Terms & Conditions" />
          <FooterLink to="#" title="Privacy Policy" />
          <FooterLink to="#" title="Contact Us" />
        </motion.div>
        <motion.div
          className="flex flex-row flex-wrap pt-8 items-center justify-center"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ delay: 0.5, duration: 1 }}
          exit={{ opacity: 0 }}
        >
          <div>
            <img
              src="/images/logo-text.png"
              alt="Makini Leads"
              className="w-20 mr-4"
            />
          </div>
          <div>&copy; {new Date().getFullYear()} Copy rights are reserved.</div>
        </motion.div>
      </motion.div>
    </motion.div>
  );
}

function FooterLink(props: { to: string; title: string }) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ delay: 0.5, duration: 1 }}
      exit={{ opacity: 0 }}
    >
      <Link
        to={props.to}
        className="transition-colors duration-300 ease-in hover:text-black text-gray-500 pr-5"
      >
        {props.title}
      </Link>
    </motion.div>
  );
}
