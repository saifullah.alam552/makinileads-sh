import { motion } from "framer-motion";
import React from "react";
import { NavLink } from "react-router-dom";
import { useIsLogin } from "../hooks";

export default function Header() {
  const isLogin = useIsLogin();

  return (
    <div className="flex justify-center pb-10 pt-6 overflow-hidden">
      <motion.div
        initial={{ opacity: 0, y: -100 }}
        animate={{ opacity: 1, y: 0 }}
        transition={{ duration: 1 }}
        exit={{ opacity: 0 }}
      >
        <NavLink to={isLogin ? "/dashboard" : "/"}>
          <img
            src="/images/logo-text.png"
            alt="Makini Leads"
            className="w-40 md:w-56"
          />
        </NavLink>
      </motion.div>
    </div>
  );
}
