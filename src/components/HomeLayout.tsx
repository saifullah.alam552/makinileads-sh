import { Button, Divider } from "antd";
import { motion } from "framer-motion";
import React from "react";
import { NavLink } from "react-router-dom";

export default function HomeLayout(props: { children?: React.ReactNode }) {
  return (
    <motion.div exit={{ opacity: 0 }}>
      <div className="grid grid-cols-1 lg:grid-cols-2 gap-4 justify-center items-center justify-items-center">
        <motion.div
          className="order-2 lg:order-1"
          initial={{ opacity: 0, x: -10 }}
          animate={{ opacity: 1, x: 0 }}
          transition={{ delay: 0, duration: 0.5 }}
        >
          <img src="/images/services.png" alt="Services" className="" />
        </motion.div>
        <motion.div
          className="order-1 lg:order-2 w-96"
          initial={{ opacity: 0, x: 10 }}
          animate={{ opacity: 1, x: 0}}
          transition={{ delay: 0.25, duration: 0.5 }}
        >
          {props.children}
          <Divider plain>or</Divider>
            <div className="text-center">
              <NavLink to="/service-receiver-signup">
                <Button type="default" shape="round">
                  Create Account
                </Button>
              </NavLink>

              <Divider type={"vertical"} />

              <NavLink to="/create-job">
                <Button type="default" shape="round">
                  Want to Post Job?
                </Button>
              </NavLink>
              <br />
              <br />
              <NavLink to="/service-provider-signup">
                <Button type="default" shape="round">
                  Register as Professional
                </Button>
              </NavLink>
            </div>
        </motion.div>
      </div>
    </motion.div>
  );
}
