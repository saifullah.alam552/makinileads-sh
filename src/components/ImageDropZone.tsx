import { Button } from "antd";
import React, { SetStateAction } from "react";
import { useDropzone } from "react-dropzone";
import ImageThumbs from "./ImageThumbs";

interface Props {
  files: any[];
  setFiles: (value: SetStateAction<any[]>) => void;
}

export default function ImageDropZone(props: Props) {
  const { getRootProps, getInputProps, open } = useDropzone({
    noClick: true,
    noDrag: true,
    maxFiles: 20,
    maxSize: 5000000,
    noKeyboard: true,
    accept: "image/jpeg, image/png, image.jpg",
    onDrop: (acceptedFiles) => {
      props.setFiles((prev) => [
        ...prev,
        ...acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        ),
      ]);
    },
  });

  return (
    <>
      <div {...getRootProps({ className: "dropzone" })}>
        <input {...getInputProps()} />
        <div className="text-center">
          <Button htmlType="button" onClick={open} type="primary">
            Add Documents
          </Button>
          <div className="my-1 text-gray-500 text-xs">
            Max size is 5mb each. Accepted formats: jpg | png | jpeg
            <br /> Please correct the filename according to document before
            uploading. e.g. if uploading a Certificate file name should be
            certificate-xyz.jpg
          </div>
        </div>
      </div>

      <ImageThumbs
        files={props.files}
        setFiles={props.setFiles}
        showDelete={true}
      />
    </>
  );
}
