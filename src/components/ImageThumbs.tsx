import { motion } from "framer-motion";
import React, { SetStateAction, useState } from "react";
import { faEye } from "@fortawesome/free-solid-svg-icons/faEye";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons/faTrashAlt";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Modal } from "antd";

interface Props {
  files: any[];
  setFiles: (value: SetStateAction<any[]>) => void;
  showDelete: boolean;
}

export default function ImageThumbs(props: Props) {
  const [previewVisible, setPreviewVisible] = useState<boolean>(false);
  const [previewImage, setPreviewImage] = useState<string>("");
  const [previewTitle, setPreviewTitle] = useState<string>("");

  const removeFile = (index: number) => {
    props.setFiles((prev: any) =>
      prev.filter((v: any, i: number) => i !== index)
    );
  };

  const handlePreview = (file: any) => {
    setPreviewImage(file.preview);
    setPreviewTitle(file.name);
    setPreviewVisible(true);
  };

  const thumbs = props.files.map((file: any, index: number) => (
    <motion.div
      key={file.name}
      className="rounded border-2 border-gray-400 w-60 h-60 box-border p-3 m-2 relative"
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 1 }}
      exit={{ opacity: 0 }}
    >
      <div className="flex min-w-0 overflow-hidden items-center justify-center">
        <img
          src={file.preview}
          alt={file.name}
          className="block w-auto max-h-56"
        />
      </div>

      <span className="absolute bottom-0 left-0 bg-gray-400 p-2 rounded-tr">
        <FontAwesomeIcon
          icon={faEye}
          size="1x"
          className="text-gray-200 hover:text-green-800 cursor-pointer"
          onClick={() => handlePreview(file)}
        />
        {props.showDelete ? (
          <FontAwesomeIcon
            icon={faTrashAlt}
            size="1x"
            className="text-gray-200 ml-3 hover:text-red-800 cursor-pointer"
            onClick={() => removeFile(index)}
          />
        ) : null}
      </span>
    </motion.div>
  ));
  return (
    <>
      <div className="flex flex-row flex-wrap my-6 justify-center">
        {thumbs}
      </div>
      <Modal
        visible={previewVisible}
        title={previewTitle}
        footer={null}
        onCancel={() => setPreviewVisible(false)}
        width="auto"
        destroyOnClose={true}
      >
        <img
          alt={previewTitle}
          style={{ width: "100%", height: "100%" }}
          src={previewImage}
        />
      </Modal>
    </>
  );
}
