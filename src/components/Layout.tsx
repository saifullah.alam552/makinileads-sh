import { motion } from "framer-motion";
import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useGetAppStatus, useIsLogin } from "../hooks";
import { useModalHook } from "../hooks/useModalHook";
import DashboardLayout from "./DashboardLayout";
import Footer from "./Footer";
import Header from "./Header";
import Loader from "./Loader";

export default function Layout(props: { children?: React.ReactNode }) {
  const history = useHistory();
  const isLogin = useIsLogin();
  const { error, loading, responseText } = useGetAppStatus();

  useEffect(() => {
    if (isLogin) history.push("/dashboard");
  }, [isLogin, history]);

  useModalHook(error, responseText, "error");
  useModalHook(responseText !== "" && error === false, responseText, "success");

  const siteLayout = (
    <div className="bg-gray-200 min-h-screen relative">
      <Header />
      <motion.div
        className="container mx-auto"
        style={{ paddingBottom: "200px" }}
      >
        {props.children}
      </motion.div>
      <div className="absolute bottom-0 w-full">
        <Footer />
      </div>
    </div>
  );

  const dashboardLayout = <DashboardLayout>{props.children}</DashboardLayout>;

  return (
    <>
      {loading ? <Loader /> : null}
      {isLogin ? dashboardLayout : siteLayout}
    </>
  );
}
