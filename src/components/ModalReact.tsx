import { faTimes } from "@fortawesome/free-solid-svg-icons/faTimes";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "antd";
import React from "react";
import Modal from "react-modal";

const modalStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: "auto",
  },
  overlay: {
    backgroundColor: "rgba(0, 0, 0, .8)",
  },
};

Modal.setAppElement(document.getElementById("root")!);

export default function ModalReact(props: {
  id: string;
  isOpen: boolean;
  onClose: () => void;
  body?: JSX.Element;
}) {
  return (
    <Modal
      id={props.id}
      isOpen={props.isOpen}
      style={modalStyles}
      shouldCloseOnEsc={true}
      shouldCloseOnOverlayClick={true}
      onRequestClose={props.onClose}
    >
      <div className="text-right mb-4">
        <Button onClick={props.onClose} danger>
          <FontAwesomeIcon icon={faTimes} />
        </Button>
      </div>
      <div>{props.body}</div>
    </Modal>
  );
}
