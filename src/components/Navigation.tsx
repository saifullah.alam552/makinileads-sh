import { Menu } from "antd";
import React, { SetStateAction } from "react";
import { Link, useLocation } from "react-router-dom";
import { IUserData, UserTypeEnum } from "../types";

interface Props {
  collapsed: boolean;
  drawerClose?: (value: SetStateAction<boolean>) => void;
  user: IUserData;
}

const Navigation: React.FC<Props> = ({ collapsed, drawerClose, user }) => {
  let location = useLocation();

  const defaultLocation = (): string[] => {
    if (location.pathname.length === 1) return ["/dashboard"];
    if (location.pathname.length > 1) return [location.pathname.slice(1)];
    return [];
  };

  return (
    <Menu
      defaultSelectedKeys={defaultLocation()}
      mode="inline"
      className=""
      selectedKeys={defaultLocation()}
    >
      <div className="flex justify-center m-3 items-center">
        {collapsed ? (
          <img src={"/images/logo.png"} className="h-14" alt="Logo" />
        ) : (
          <img src={"/images/logo-text.png"} className="h-14" alt="Logo" />
        )}
      </div>

      <div className="mb-8" />
      <Menu.Item
        key="dashboard"
        title="Dashboard"
        onClick={drawerClose ? () => drawerClose(false) : undefined}
      >
        <Link to="/dashboard">
          <div className="flex items-center">
            <div className="text-center text-2xl mr-2">
              <i className={"pe-7s-note2"}></i>
            </div>
            {!collapsed && (
              <span>
                {user.userType === UserTypeEnum.ADMIN ? "Manage Jobs" : "Jobs"}
              </span>
            )}
          </div>
        </Link>
      </Menu.Item>

      {user.userType === UserTypeEnum.ADMIN && (
        <>
          <Menu.Item
            key="dashboard/manage-users"
            title="Manage Users"
            onClick={drawerClose ? () => drawerClose(false) : undefined}
          >
            <Link to="/dashboard/manage-users">
              <div className="flex items-center">
                <div className="text-center text-2xl mr-2">
                  <i className="pe-7s-users"></i>
                </div>
                {!collapsed && <span>Manage Users</span>}
              </div>
            </Link>
          </Menu.Item>
          <Menu.Item
            key="dashboard/manage-skills"
            title="Manage Skills"
            onClick={drawerClose ? () => drawerClose(false) : undefined}
          >
            <Link to="/dashboard/manage-skills">
              <div className="flex items-center">
                <div className="text-center text-2xl mr-2">
                  <i className="pe-7s-tools"></i>
                </div>
                {!collapsed && <span>Manage Skills</span>}
              </div>
            </Link>
          </Menu.Item>
        </>
      )}

      <Menu.Item
        key="dashboard/account-settings"
        title="Account Settings"
        onClick={drawerClose ? () => drawerClose(false) : undefined}
      >
        <Link to="/dashboard/account-settings">
          <div className="flex items-center">
            <div className="text-center text-2xl mr-2">
              <i className="pe-7s-settings"></i>
            </div>
            {!collapsed && <span>Account Settings</span>}
          </div>
        </Link>
      </Menu.Item>
    </Menu>
  );
};

export default Navigation;
