import { Space } from "antd";
import React from "react";

interface Props {
  heading: string;
  subheading?: string;
  icon?: string;
  buttons?: JSX.Element;
}

const PageTitle: React.FC<Props> = (props) => {
  return (
    <div className="bg-blue-100 px-8 py-6 ">
      <div className="flex items-center justify-between flex-col space-y-8 lg:flex-row lg:space-y-0">
        <div className="flex items-center justify-center text-xl font-normal">
          <div
            className={
              "text-4xl flex items-center text-center p-2 bg-white rounded shadow-lg mr-4"
            }
          >
            {props.icon ? <i className={`${props.icon}`}></i> : null}
          </div>
          <div>
            <Space size="large">{props.heading}</Space>
            <div className="text-sm opacity-80">{props.subheading}</div>
          </div>
        </div>
        <div className="">{props.buttons}</div>
      </div>
    </div>
  );
};

export default PageTitle;
