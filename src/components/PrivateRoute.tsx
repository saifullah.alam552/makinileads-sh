import { motion } from "framer-motion";
import React from "react";
import { Redirect, Route, RouteProps } from "react-router-dom";
import { useIsLogin } from "../hooks";

const PrivateRoute = ({ ...rest }: RouteProps) => {
  const isLogin = useIsLogin();

  if (isLogin) {
    return <Route {...rest} />;
  } else {
    return (
      <motion.div exit="undefined">
        <Redirect to="/" />
      </motion.div>
    );
  }
};

export default PrivateRoute;
