import React from "react";
import { useGetServiceData } from "../hooks";

export default function SelectedSkills(props: {
  jobSkills: string[];
  small?: boolean;
}) {
  const data = useGetServiceData(props.jobSkills);

  return (
    <div className={props.small ? "" : "mb-4 mt-4"}>
      {!props.small && <div className="my-2 text-sm">Selected Skills:</div>}
      <div className="flex flex-wrap gap-2">
        {data.map((d) => (
          <div
            key={d.key}
            className="whitespace-pre-wrap text-white rounded"
            style={{ backgroundColor: "#806ed8" }}
          >
            {!props.small && (
              <div
                className="py-1 px-3 rounded"
                style={{ backgroundColor: "#6346f3" }}
              >
                {d.categoryName}
              </div>
            )}
            <div
              className={props.small ? "text-xs p-1 px-2 rounded" : "py-1 px-3"}
              style={props.small ? { backgroundColor: "#6346f3" } : undefined}
            >
              {d.subCategoryName}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
