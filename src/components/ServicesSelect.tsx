import React, { SetStateAction, useEffect, useState } from "react";
import SelectField from "../antd/SelectField";
import {
  useGetSelectCategories,
  useGetSelectSubCategoriesByCategory,
} from "../hooks";
import { useModalHook } from "../hooks/useModalHook";

interface SelectService {
  category: string;
  subCategory: string;
}

interface Props {
  categoryLabel: string;
  subCategoryLabel: string;
  values: any;
  setValues: (value: SetStateAction<any>) => void;
}

export default function ServiceSelect(props: Props) {
  const [serviceSelect, setServiceSelect] = useState<SelectService>({
    category: "",
    subCategory: "",
  });

  const [err, setErr] = useState<{ err: boolean; msg: string }>({
    err: false,
    msg: "",
  });

  const categorySelect = useGetSelectCategories();
  const subCategorySelectOnCategory = useGetSelectSubCategoriesByCategory(
    serviceSelect.category
  );

  useEffect(() => {
    if (
      props.values.skills[props.values.skills.length - 1] !==
      serviceSelect.subCategory
    ) {
      setServiceSelect((prev) => ({ ...prev, subCategory: "" }));
    }
  }, [props.values.skills, serviceSelect.subCategory]);

  const selectChange = (value: string, fieldName: string) => {
    if (fieldName === "category") {
      setServiceSelect({
        category: value,
        subCategory: "",
      });
    } else {
      const check = props.values.skills.findIndex((d: any) => d === value);
      if (check !== -1) {
        setErr({
          err: true,
          msg: "You have already selected this skill.",
        });
        setServiceSelect({
          ...serviceSelect,
          subCategory: "",
        });
        return;
      }
      props.setValues((prev: any) => {
        return {
          ...prev,
          skills: [...prev.skills, value],
        };
      });
      setServiceSelect({
        ...serviceSelect,
        subCategory: value,
      });
    }
  };

  useModalHook(err.err, err.msg, "warning", () =>
    setErr({ err: false, msg: "" })
  );

  return (
    <div className="flex flex-col md:flex-row md:space-x-2 w-full md:items-center">
      <div className="md:w-1/2">
        <SelectField
          showSearch={false}
          label={props.categoryLabel}
          id={"category"}
          value={serviceSelect.category}
          onChange={(value) => selectChange(value, "category")}
          required={true}
          data={categorySelect}
          displayError={true}
        />
      </div>
      <div className="md:w-1/2">
        <SelectField
          showSearch={false}
          label={props.subCategoryLabel}
          id={"subCategory"}
          value={serviceSelect.subCategory}
          onChange={(value) => selectChange(value, "subCategory")}
          required={true}
          data={subCategorySelectOnCategory}
          displayError={true}
        />
      </div>
    </div>
  );
}
