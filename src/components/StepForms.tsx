import { Button, Radio } from "antd";
import { motion } from "framer-motion";
import React, { SetStateAction, useState } from "react";
import InputField from "../antd/InputField";
import { FormErrors, useForm } from "../hooks/useForm";
import { useModalHook } from "../hooks/useModalHook";
import {
  JobFormModel,
  LoginFormInput,
  UserSignupFormModel,
  UserTypeEnum,
} from "../types";
import {
  addressValidation,
  loginFormValidate,
  SPCompanyValidation,
  SPIndividualValidation,
  UserCompanyValidation,
  UserIndividualValidation,
  validateInfoEduCompany,
  validateInfoEduIndividual,
  validateJobDetail,
  validatePassword,
  validateTypeForm,
} from "../utils/validations";
import ImageDropZone from "./ImageDropZone";
import SelectedSkills from "./SelectedSkills";
import {
  AddressForm,
  CompanyForm,
  CompanyInfoEduForm,
  IndividualForm,
  IndividualInfoEduForm,
  JobDetailForm,
  LoginForm,
  ServicesForm,
} from "./SubForms";

interface Props<T> {
  values: T;
  current: number;
  length: number;
  initValues: T;
  initErrors: FormErrors<T>;
  setValues: (value: SetStateAction<T>) => void;
  setCurrent: (value: SetStateAction<number>) => void;
  onFinish?: (files: any[]) => void;
}

function FormButtons(props: {
  current: number;
  length: number;
  next: () => void;
  prev: () => void;
  loginClick?: () => void;
  login?: boolean;
}) {
  return (
    <motion.div
      className="mt-6"
      initial={{ opacity: 0, y: 10 }}
      animate={{ opacity: 1, y: 0 }}
      transition={{ duration: 0.5 }}
    >
      {props.login && (
        <Button
          type="primary"
          className="logo-purple-color"
          onClick={props.loginClick}
          style={{ marginRight: "8px" }}
        >
          Login
        </Button>
      )}
      {props.current < props.length - 1 && (
        <Button className="logo-purple-color" type="primary" onClick={props.next}>
          {props.login ? "Create new Account" : "Next"}
        </Button>
      )}
      {props.current === props.length - 1 && (
        <Button className="logo-purple-color" type="primary" onClick={props.next}>
          Submit
        </Button>
      )}
      {props.current > 0 && (
        <Button style={{ margin: "0 8px" }} onClick={props.prev}>
          Previous
        </Button>
      )}
    </motion.div>
  );
}

export function SPTypeStep(props: Props<UserSignupFormModel>) {
  const { handleChange } = useForm(
    props.initValues,
    props.initErrors,
    validateTypeForm,
    props.setValues,
    props.values
  );

  const next = () => {
    props.setCurrent(props.current + 1);
  };

  const prev = () => {
    props.setCurrent(props.current - 1);
  };

  return (
    <>
      <motion.div
        className="text-center"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <Radio.Group
          onChange={handleChange}
          value={props.values.userType}
          name="userType"
        >
          <Radio value={UserTypeEnum.SP_IND}>Individual</Radio>
          <Radio value={UserTypeEnum.SP_COM}>Company</Radio>
        </Radio.Group>
      </motion.div>
      <FormButtons
        current={props.current}
        length={props.length}
        next={() => next()}
        prev={() => prev()}
      />
    </>
  );
}

export function SRTypeStep(props: Props<UserSignupFormModel>) {
  const { handleChange } = useForm(
    props.initValues,
    props.initErrors,
    validateTypeForm,
    props.setValues,
    props.values
  );

  const next = () => {
    props.setCurrent(props.current + 1);
  };

  const prev = () => {
    props.setCurrent(props.current - 1);
  };

  return (
    <>
      <motion.div
        className="text-center"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <Radio.Group
          onChange={handleChange}
          value={props.values.userType}
          name="userType"
        >
          <Radio value={UserTypeEnum.SR_IND}>Individual</Radio>
          <Radio value={UserTypeEnum.SR_COM}>Company</Radio>
        </Radio.Group>
      </motion.div>
      <FormButtons
        current={props.current}
        length={props.length}
        next={() => next()}
        prev={() => prev()}
      />
    </>
  );
}

export function DetailsStep(props: Props<UserSignupFormModel>) {
  const indValid =
    props.values.userType === UserTypeEnum.SP_IND
      ? SPIndividualValidation
      : UserIndividualValidation;

  const comValid =
    props.values.userType === UserTypeEnum.SP_COM
      ? SPCompanyValidation
      : UserCompanyValidation;

  const individual = useForm(
    props.initValues,
    props.initErrors,
    indValid,
    props.setValues,
    props.values
  );

  const company = useForm(
    props.initValues,
    props.initErrors,
    comValid,
    props.setValues,
    props.values
  );

  const next = async () => {
    const formValid =
      props.values.userType === UserTypeEnum.SP_COM ||
      props.values.userType === UserTypeEnum.SR_COM
        ? await company.formValidation()
        : await individual.formValidation();
    if (!formValid) return;
    props.setCurrent(props.current + 1);
  };

  const prev = () => {
    props.setCurrent(props.current - 1);
  };

  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        {props.values.userType === UserTypeEnum.SP_COM ||
        props.values.userType === UserTypeEnum.SR_COM ? (
          <CompanyForm
            values={props.values}
            errors={company.formErrors}
            handleChange={company.handleChange}
            handleBlur={company.handleBlur}
            fieldDisable={false}
            disableEmailPhone={false}
          />
        ) : (
          <IndividualForm
            values={props.values}
            errors={individual.formErrors}
            handleChange={individual.handleChange}
            handleBlur={individual.handleBlur}
            fieldDisable={false}
            disableEmailPhone={false}
          />
        )}
      </motion.div>

      <FormButtons
        current={props.current}
        length={props.length}
        next={() => next()}
        prev={() => prev()}
      />
    </>
  );
}

export function AddressStep(props: Props<any>) {
  const {
    formErrors,
    formValidation,
    handleChange,
    handleBlur,
    cascaderSelect,
  } = useForm(
    props.initValues,
    props.initErrors,
    addressValidation,
    props.setValues,
    props.values
  );

  const next = async () => {
    const formValid = await formValidation();
    if (!formValid) return;
    props.setCurrent(props.current + 1);
  };

  const prev = () => {
    props.setCurrent(props.current - 1);
  };

  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <AddressForm
          values={props.values}
          errors={formErrors}
          handleBlur={handleBlur}
          handleChange={handleChange}
          cascaderSelect={cascaderSelect}
          fieldDisable={false}
        />
      </motion.div>
      <FormButtons
        current={props.current}
        length={props.length}
        next={() => next()}
        prev={() => prev()}
      />
    </>
  );
}

export function ServiceStep(props: Props<any>) {
  const [err, setErr] = useState<{ err: boolean; msg: string }>({
    err: false,
    msg: "",
  });

  const next = () => {
    if (props.values.skills.length === 0) {
      setErr({
        err: true,
        msg: "Please enter Professional and Type of Skills",
      });
      return;
    }
    props.setCurrent(props.current + 1);
  };

  const prev = () => {
    props.setCurrent(props.current - 1);
  };

  useModalHook(err.err, err.msg, "warning", () =>
    setErr({ err: false, msg: "" })
  );

  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <ServicesForm setValues={props.setValues} values={props.values} />
      </motion.div>
      <FormButtons
        current={props.current}
        length={props.length}
        next={() => next()}
        prev={() => prev()}
      />
    </>
  );
}

export function InfoEduStep(props: Props<UserSignupFormModel>) {
  const individual = useForm(
    props.initValues,
    props.initErrors,
    validateInfoEduIndividual,
    props.setValues,
    props.values
  );

  const company = useForm(
    props.initValues,
    props.initErrors,
    validateInfoEduCompany,
    props.setValues,
    props.values
  );

  const next = async () => {
    const formValid =
      props.values.userType === UserTypeEnum.SP_IND
        ? await individual.formValidation()
        : await company.formValidation();
    if (!formValid) return;
    props.setCurrent(props.current + 1);
  };

  const prev = () => {
    props.setCurrent(props.current - 1);
  };

  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        {props.values.userType === UserTypeEnum.SP_IND ? (
          <IndividualInfoEduForm
            values={props.values}
            errors={individual.formErrors}
            handleBlur={individual.handleBlur}
            handleChange={individual.handleChange}
            numberChange={individual.numberChange}
            fieldDisable={false}
          />
        ) : (
          <CompanyInfoEduForm
            values={props.values}
            errors={company.formErrors}
            handleBlur={company.handleBlur}
            handleChange={company.handleChange}
            numberChange={company.numberChange}
            fieldDisable={false}
          />
        )}
      </motion.div>
      <FormButtons
        current={props.current}
        length={props.length}
        next={() => next()}
        prev={() => prev()}
      />
    </>
  );
}

export function PasswordStep(props: Props<UserSignupFormModel>) {
  const [err, setErr] = useState<{ err: boolean; msg: string }>({
    err: false,
    msg: "",
  });

  const { formErrors, formValidation, handleChange, handleBlur } = useForm(
    props.initValues,
    props.initErrors,
    validatePassword,
    props.setValues,
    props.values
  );

  const next = async () => {
    const formValid = await formValidation();
    if (!formValid) return;
    if (props.values.password !== props.values.confirmPassword) {
      setErr({
        err: true,
        msg: "Password & Confirm Password are not same. Please check",
      });
      return;
    }

    props.onFinish! ? props.onFinish([]) : props.setCurrent(props.current + 1);
  };

  const prev = () => {
    props.setCurrent(props.current - 1);
  };

  useModalHook(err.err, err.msg, "warning", () =>
    setErr({ err: false, msg: "" })
  );

  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <div className="flex flex-col md:flex-row md:space-x-2 w-full">
          <div className="md:w-1/2">
            <InputField
              id="email"
              name="email"
              type="simple"
              label="Email"
              placeHolder="Enter email address"
              value={props.values.email}
              required={true}
              disabled={true}
              displayError={true}
            />
          </div>
          <div className="md:w-1/2">
            <InputField
              id="phoneNumber"
              name="phoneNumber"
              type="simple"
              label="Phone Number"
              placeHolder="Enter phone number"
              value={props.values.phoneNumber}
              disabled={true}
              required={true}
              displayError={true}
            />
          </div>
        </div>
        <div className="flex flex-col md:flex-row md:space-x-2 w-full">
          <div className="md:w-1/2">
            <InputField
              type="password"
              id="password"
              name="password"
              label="Password"
              placeHolder="Enter your password"
              value={props.values.password}
              error={formErrors.password}
              onBlur={handleBlur}
              onChange={handleChange}
              required={true}
              displayError={true}
            />
          </div>
          <div className="md:w-1/2">
            <InputField
              type="password"
              id="confirmPassword"
              name="confirmPassword"
              label="Confirm Password"
              placeHolder="Enter your confirm password"
              value={props.values.confirmPassword}
              error={formErrors.confirmPassword}
              onBlur={handleBlur}
              onChange={handleChange}
              required={true}
              displayError={true}
            />
          </div>
        </div>
      </motion.div>
      <FormButtons
        current={props.current}
        length={props.length}
        next={() => next()}
        prev={() => prev()}
      />
    </>
  );
}

export function DocumentsStep(props: Props<UserSignupFormModel>) {
  const [files, setFiles] = useState<any[]>([]);
  const [err, setErr] = useState<{ err: boolean; msg: string }>({
    err: false,
    msg: "",
  });

  const next = async () => {
    if (files.length === 0) {
      setErr({
        err: true,
        msg: "Please upload relevent documents.",
      });
      return;
    }

    props.onFinish!
      ? props.onFinish(files)
      : props.setCurrent(props.current + 1);
  };

  const prev = () => {
    props.setCurrent(props.current - 1);
  };

  useModalHook(err.err, err.msg, "warning", () =>
    setErr({ err: false, msg: "" })
  );

  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <ImageDropZone files={files} setFiles={setFiles} />
      </motion.div>
      <FormButtons
        current={props.current}
        length={props.length}
        next={() => next()}
        prev={() => prev()}
      />
    </>
  );
}

export function JobDetailStep(props: Props<JobFormModel>) {
  const {
    handleBlur,
    handleChange,
    formErrors,
    numberChange,
    formValidation,
  } = useForm(
    props.initValues,
    props.initErrors,
    validateJobDetail,
    props.setValues,
    props.values
  );

  const next = async () => {
    const formValid = await formValidation();
    if (!formValid) return;
    props.setCurrent(props.current + 1);
  };

  const prev = () => {
    props.setCurrent(props.current - 1);
  };

  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <JobDetailForm
          values={props.values}
          errors={formErrors}
          handleBlur={handleBlur}
          handleChange={handleChange}
          numberChange={numberChange}
          fieldDisable={false}
        />

        <SelectedSkills jobSkills={props.values.skills} />
      </motion.div>
      <FormButtons
        current={props.current}
        length={props.length}
        next={() => next()}
        prev={() => prev()}
      />
    </>
  );
}

export function JobLocationStep(props: Props<JobFormModel>) {
  const {
    formErrors,
    formValidation,
    handleChange,
    handleBlur,
    cascaderSelect,
  } = useForm(
    props.initValues,
    props.initErrors,
    addressValidation,
    props.setValues,
    props.values
  );

  const next = async () => {
    const formValid = await formValidation();
    if (!formValid) return;
    props.setCurrent(props.current + 1);
  };

  const prev = () => {
    props.setCurrent(props.current - 1);
  };

  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <AddressForm
          values={props.values}
          errors={formErrors}
          cascaderSelect={cascaderSelect}
          handleBlur={handleBlur}
          handleChange={handleChange}
          fieldDisable={false}
        />
      </motion.div>
      <FormButtons
        current={props.current}
        length={props.length}
        next={() => next()}
        prev={() => prev()}
      />
    </>
  );
}

export function LoginOrNextStep(props: Props<LoginFormInput>) {
  const { formErrors, formValidation, handleChange, handleBlur } = useForm(
    props.initValues,
    props.initErrors,
    loginFormValidate,
    props.setValues,
    props.values
  );

  const next = async () => {
    props.setCurrent(props.current + 1);
    props.setValues(props.initValues);
  };

  const prev = () => {
    props.setCurrent(props.current - 1);
    props.setValues(props.initValues);
  };

  const onLogin = async () => {
    const formValid = await formValidation();
    if (!formValid) return;
    props.onFinish && props.onFinish([]);
  };

  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <LoginForm
          values={props.values}
          errors={formErrors}
          handleBlur={handleBlur}
          handleChange={handleChange}
        />
      </motion.div>
      <FormButtons
        current={props.current}
        length={props.length}
        next={() => next()}
        prev={() => prev()}
        login={true}
        loginClick={() => onLogin()}
      />
    </>
  );
}
