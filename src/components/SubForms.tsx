import { faCamera } from "@fortawesome/free-solid-svg-icons/faCamera";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Empty, Radio } from "antd";
import React, { SetStateAction, useEffect } from "react";
import CascaderField from "../antd/CascaderField";
import InputField from "../antd/InputField";
import { location } from "../data/location";
import { useGetServiceData } from "../hooks";
import { FormErrors } from "../hooks/useForm";
import {
  ChangePasswdInput,
  ForgetPasswdInput,
  JobFormModel,
  LoginFormInput,
  UserSignupFormModel,
  UserTypeEnum,
} from "../types";
import ServiceSelect from "./ServicesSelect";

export const ForgetPasswordForm = (props: {
  values: ForgetPasswdInput;
  errors: FormErrors<ForgetPasswdInput>;
  handleChange?: (event: any) => void;
  handleBlur?: (event: any) => void;
}) => (
  <>
    <div>
      <InputField
        id="username"
        name="username"
        type="simple"
        label="Email / Phone number"
        placeHolder="Enter email / phone number"
        value={props.values.username}
        error={props.errors.username}
        onBlur={props.handleBlur}
        onChange={props.handleChange}
        required={true}
        displayError={true}
      />
    </div>
  </>
);

export const LoginForm = (props: {
  values: LoginFormInput;
  errors: FormErrors<LoginFormInput>;
  handleChange?: (event: any) => void;
  handleBlur?: (event: any) => void;
}) => (
  <>
    <div>
      <InputField
        id="username"
        name="username"
        type="simple"
        label="Email / Phone number"
        placeHolder="Enter email / phone number"
        value={props.values.username}
        error={props.errors.username}
        onBlur={props.handleBlur}
        onChange={props.handleChange}
        required={true}
        displayError={true}
      />
    </div>
    <div>
      <InputField
        type="password"
        id="password"
        name="password"
        label="Password"
        placeHolder="Enter your password"
        value={props.values.password}
        error={props.errors.password}
        onBlur={props.handleBlur}
        onChange={props.handleChange}
        required={true}
        displayError={true}
      />
    </div>
  </>
);

export const ChangePasswordForm = (props: {
  values: ChangePasswdInput;
  errors: FormErrors<ChangePasswdInput>;
  handleChange?: (event: any) => void;
  handleBlur?: (event: any) => void;
}) => (
  <>
    <div>
      <InputField
        id="currentPassword"
        name="currentPassword"
        type="password"
        label="Current Password"
        placeHolder="Enter current password"
        value={props.values.currentPassword}
        error={props.errors.currentPassword}
        onBlur={props.handleBlur}
        onChange={props.handleChange}
        required={true}
        displayError={true}
      />
    </div>
    <div>
      <InputField
        name="newPassword"
        id="newPassword"
        type="password"
        label="New Password"
        placeHolder="Enter your new password"
        value={props.values.newPassword}
        error={props.errors.newPassword}
        onBlur={props.handleBlur}
        onChange={props.handleChange}
        required={true}
        displayError={true}
      />
    </div>
    <div>
      <InputField
        name="confirmPassword"
        id="confirmPassword"
        type="password"
        label="Confirm Password"
        placeHolder="Enter your confirm password"
        value={props.values.confirmPassword}
        error={props.errors.confirmPassword}
        onBlur={props.handleBlur}
        onChange={props.handleChange}
        required={true}
        displayError={true}
      />
    </div>
  </>
);

export const IndividualForm = (props: {
  values: UserSignupFormModel;
  errors: FormErrors<UserSignupFormModel>;
  handleChange: (event: any) => void;
  handleBlur: (event: any) => void;
  fieldDisable: boolean;
  disableEmailPhone: boolean;
}) => (
  <>
    <div className="flex flex-col md:flex-row md:space-x-2 w-full">
      <div className="md:w-1/2">
        <InputField
          id="firstName"
          name="firstName"
          type="simple"
          label="First Name"
          placeHolder="Enter first name"
          value={props.values.firstName}
          error={props.errors.firstName}
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          required={true}
          displayError={true}
          disabled={props.fieldDisable}
        />
      </div>
      <div className="md:w-1/2">
        <InputField
          id="lastName"
          name="lastName"
          type="simple"
          label="Last Name"
          placeHolder="Enter last name"
          value={props.values.lastName}
          error={props.errors.lastName}
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          required={true}
          displayError={true}
          disabled={props.fieldDisable}
        />
      </div>
    </div>
    <div className="flex flex-col md:flex-row md:space-x-2 w-full">
      <div className="md:w-1/2">
        <InputField
          id="email"
          name="email"
          type="simple"
          label="Email"
          placeHolder="Enter email address"
          value={props.values.email}
          onChange={props.handleChange}
          error={props.errors.email}
          onBlur={props.handleBlur}
          required={true}
          displayError={true}
          disabled={props.fieldDisable || props.disableEmailPhone}
        />
      </div>
      <div className="md:w-1/2">
        <InputField
          id="phoneNumber"
          name="phoneNumber"
          type="simple"
          label="Phone Number"
          placeHolder="Enter phone number"
          value={props.values.phoneNumber}
          onChange={props.handleChange}
          error={props.errors.phoneNumber}
          onBlur={props.handleBlur}
          required={true}
          displayError={true}
          disabled={props.fieldDisable || props.disableEmailPhone}
        />
      </div>
    </div>
    {props.values.userType === UserTypeEnum.SP_IND && (
      <>
        <div className="flex flex-col w-full">
          <InputField
            id="NIDA"
            name="NIDA"
            type="simple"
            label="NIDA"
            placeHolder="Enter NIDA"
            value={props.values.NIDA}
            onChange={props.handleChange}
            error={props.errors.NIDA}
            onBlur={props.handleBlur}
            required={true}
            displayError={true}
            disabled={props.fieldDisable}
          />
        </div>
        <div className="flex flex-col md:flex-row md:space-x-2 w-full">
          <div className="md:w-1/2">
            <InputField
              id="refPersonName"
              name="refPersonName"
              type="simple"
              label="Reference Person"
              placeHolder="Enter reference person name"
              value={props.values.refPersonName}
              onChange={props.handleChange}
              error={props.errors.refPersonName}
              onBlur={props.handleBlur}
              required={true}
              displayError={true}
              disabled={props.fieldDisable}
            />
          </div>
          <div className="md:w-1/2">
            <InputField
              id="refPhoneNumber"
              name="refPhoneNumber"
              type="simple"
              label="Reference Phone Number"
              placeHolder="Enter reference person phone number"
              value={props.values.refPhoneNumber}
              onChange={props.handleChange}
              error={props.errors.refPhoneNumber}
              onBlur={props.handleBlur}
              required={true}
              displayError={true}
              disabled={props.fieldDisable}
            />
          </div>
        </div>
      </>
    )}
  </>
);

export const CompanyForm = (props: {
  values: UserSignupFormModel;
  errors: FormErrors<UserSignupFormModel>;
  handleChange: (event: any) => void;
  handleBlur: (event: any) => void;
  fieldDisable: boolean;
  disableEmailPhone: boolean;
}) => (
  <>
    <div className="flex flex-col w-full">
      <InputField
        id="companyName"
        name="companyName"
        type="simple"
        label="Company Name"
        placeHolder="Enter company name"
        value={props.values.companyName}
        onChange={props.handleChange}
        error={props.errors.companyName}
        onBlur={props.handleBlur}
        required={true}
        displayError={true}
        disabled={props.fieldDisable}
      />
    </div>
    <div className="flex flex-col md:flex-row md:space-x-2 w-full">
      <div className="md:w-1/2">
        <InputField
          id="contactPerson"
          name="contactPerson"
          type="simple"
          label="Contact Person"
          placeHolder="Enter contact person name"
          value={props.values.contactPerson}
          onChange={props.handleChange}
          error={props.errors.contactPerson}
          onBlur={props.handleBlur}
          required={true}
          displayError={true}
          disabled={props.fieldDisable}
        />
      </div>
      <div className="md:w-1/2">
        <InputField
          id="jobTitle"
          name="jobTitle"
          type="simple"
          label="Job Title"
          placeHolder="Enter job title"
          value={props.values.jobTitle}
          onChange={props.handleChange}
          error={props.errors.jobTitle}
          onBlur={props.handleBlur}
          required={true}
          displayError={true}
          disabled={props.fieldDisable}
        />
      </div>
    </div>
    <div className="flex flex-col md:flex-row md:space-x-2 w-full">
      <div className="md:w-1/2">
        <InputField
          id="email"
          name="email"
          type="simple"
          label="Email"
          placeHolder="Enter email address"
          value={props.values.email}
          onChange={props.handleChange}
          error={props.errors.email}
          onBlur={props.handleBlur}
          required={true}
          displayError={true}
          disabled={props.fieldDisable || props.disableEmailPhone}
        />
      </div>
      <div className="md:w-1/2">
        <InputField
          id="phoneNumber"
          name="phoneNumber"
          type="simple"
          label="Phone Number"
          placeHolder="Enter phone number"
          value={props.values.phoneNumber}
          onChange={props.handleChange}
          error={props.errors.phoneNumber}
          onBlur={props.handleBlur}
          required={true}
          displayError={true}
          disabled={props.fieldDisable || props.disableEmailPhone}
        />
      </div>
    </div>
    {props.values.userType === UserTypeEnum.SP_COM && (
      <div className="flex flex-col w-full">
        <InputField
          id="BRELA"
          name="BRELA"
          type="simple"
          label="BRELA"
          placeHolder="Enter BRELA"
          value={props.values.BRELA}
          onChange={props.handleChange}
          error={props.errors.BRELA}
          onBlur={props.handleBlur}
          required={true}
          displayError={true}
          disabled={props.fieldDisable}
        />
      </div>
    )}
  </>
);

export const AddressForm = (props: {
  values: UserSignupFormModel | JobFormModel;
  errors: FormErrors<UserSignupFormModel | JobFormModel>;
  handleChange: (event: any) => void;
  handleBlur: (event: any) => void;
  cascaderSelect: (value: string[], fieldName: string[]) => void;
  fieldDisable: boolean;
}) => (
  <>
    <div>
      <InputField
        id="streetAddress"
        name="streetAddress"
        type="simple"
        label="Street Address"
        placeHolder="Enter street address"
        value={props.values.streetAddress}
        onChange={props.handleChange}
        error={props.errors.streetAddress}
        onBlur={props.handleBlur}
        required={true}
        displayError={true}
        disabled={props.fieldDisable}
      />
    </div>
    <div>
      <CascaderField
        id="location"
        name="location"
        option={location}
        displayError={true}
        error={
          props.errors.region || props.errors.district || props.errors.ward
        }
        label="Region / District / Ward"
        onChange={(value: any) =>
          props.cascaderSelect(value, ["region", "district", "ward"])
        }
        value={[props.values.region, props.values.district, props.values.ward]}
        required={true}
        disabled={props.fieldDisable}
      />
    </div>
  </>
);

export const IndividualInfoEduForm = (props: {
  values: UserSignupFormModel;
  errors: FormErrors<UserSignupFormModel>;
  handleChange: (event: any) => void;
  handleBlur: (event: any) => void;
  numberChange: (value: any, fieldName: string) => void;
  fieldDisable: boolean;
}) => (
  <>
    <div className="flex flex-row w-full space-x-4 mb-4">
      <div>Vocation Training: </div>
      <Radio.Group
        onChange={props.handleChange}
        value={props.values.vocationTraining}
        name="vocationTraining"
        disabled={props.fieldDisable}
      >
        <Radio value={"Yes"}>Yes</Radio>
        <Radio value={"No"}>No</Radio>
      </Radio.Group>
    </div>
    <div className="flex flex-col w-full">
      <InputField
        id="experience"
        name="experience"
        type="number"
        label="Experience in No. of Years"
        value={props.values.experience}
        onChange={(value) => props.numberChange(value, "experience")}
        error={props.errors.experience}
        required={true}
        displayError={true}
        disabled={props.fieldDisable}
      />
    </div>
    <div className="flex flex-col w-full">
      <InputField
        id="expDetails"
        name="expDetails"
        type="textarea"
        label="Experience Details"
        placeHolder="Enter experience details / specilizations etc..."
        value={props.values.expDetails}
        onChange={props.handleChange}
        error={props.errors.expDetails}
        onBlur={props.handleBlur}
        required={true}
        displayError={true}
        textAreaMaxLength={2500}
        disabled={props.fieldDisable}
      />
    </div>
  </>
);

export const CompanyInfoEduForm = (props: {
  values: UserSignupFormModel;
  errors: FormErrors<UserSignupFormModel>;
  handleChange: (event: any) => void;
  handleBlur: (event: any) => void;
  numberChange: (value: any, fieldName: string) => void;
  fieldDisable: boolean;
}) => (
  <>
    <div className="flex flex-col w-full">
      <InputField
        id="noOfEmpoyees"
        name="noOfEmpoyees"
        type="number"
        label="No. of Employees"
        value={props.values.noOfEmpoyees}
        onChange={(value) => props.numberChange(value, "noOfEmpoyees")}
        error={props.errors.noOfEmpoyees}
        required={true}
        displayError={true}
        disabled={props.fieldDisable}
      />
    </div>
    <div className="flex flex-col w-full">
      <InputField
        id="experience"
        name="experience"
        type="number"
        label="Experience in No. of Years"
        value={props.values.experience}
        onChange={(value) => props.numberChange(value, "experience")}
        error={props.errors.experience}
        required={true}
        displayError={true}
        disabled={props.fieldDisable}
      />
    </div>
    <div className="flex flex-col w-full">
      <InputField
        id="expDetails"
        name="expDetails"
        type="textarea"
        label="Experience Details"
        placeHolder="Enter experience details / specilizations etc..."
        value={props.values.expDetails}
        onChange={props.handleChange}
        error={props.errors.expDetails}
        onBlur={props.handleBlur}
        required={true}
        displayError={true}
        textAreaMaxLength={2500}
        disabled={props.fieldDisable}
      />
    </div>
  </>
);

export const JobDetailForm = (props: {
  values: JobFormModel;
  errors: FormErrors<JobFormModel>;
  handleChange: (event: any) => void;
  handleBlur: (event: any) => void;
  numberChange: (value: any, fieldName: string) => void;
  fieldDisable: boolean;
}) => (
  <>
    <div className="flex flex-col w-full">
      <InputField
        id="jobTitle"
        name="jobTitle"
        type="simple"
        label="Job Title"
        placeHolder="Enter job title"
        value={props.values.jobTitle}
        onChange={props.handleChange}
        error={props.errors.jobTitle}
        onBlur={props.handleBlur}
        required={true}
        displayError={true}
        disabled={props.fieldDisable}
      />
    </div>
    <div className="flex flex-col w-full">
      <InputField
        id="jobDescription"
        name="jobDescription"
        type="textarea"
        label="Job Description"
        placeHolder="Enter job description"
        value={props.values.jobDescription}
        onChange={props.handleChange}
        error={props.errors.jobDescription}
        onBlur={props.handleBlur}
        required={true}
        displayError={true}
        textAreaMaxLength={2500}
        disabled={props.fieldDisable}
      />
    </div>
    <div className="flex flex-col w-full">
      <InputField
        id="jobAmount"
        name="jobAmount"
        type="currency"
        label="I am willing to pay? "
        placeHolder="Enter amount your are willing to pay"
        value={props.values.jobAmount}
        onChange={(value) => props.numberChange(value, "jobAmount")}
        error={props.errors.jobAmount}
        numSteps={10}
        required={false}
        displayError={true}
        disabled={props.fieldDisable}
      />
    </div>
  </>
);

export const ServicesForm = (props: {
  values: any;
  setValues: (value: SetStateAction<any>) => void;
}) => {
  const data = useGetServiceData(props.values.skills);

  const removeSkill = (id: string) => {
    props.setValues((prev: any) => {
      return {
        ...prev,
        skills: prev.skills.filter((d: any) => d !== id),
      };
    });
  };

  return (
    <>
      <ServiceSelect
        categoryLabel="Services"
        subCategoryLabel="Type of Skills"
        setValues={props.setValues}
        values={props.values}
      />
      <div>
        {data.length === 0 ? (
          <Empty />
        ) : (
          <>
            {data.map((d) => (
              <div
                className="p-2 mb-2 mt-2 flex flex-col md:flex-row md:space-x-2 w-full md:items-center bg-blue-100"
                key={d.key}
              >
                <div className="md:w-1/2 font-bold md:font-normal">
                  {d.categoryName}
                </div>
                <div className="md:w-1/2">{d.subCategoryName}</div>
                <div className="text-right">
                  <Button
                    type="primary"
                    danger={true}
                    onClick={() => removeSkill(d.key)}
                  >
                    Remove
                  </Button>
                </div>
              </div>
            ))}
          </>
        )}
      </div>
    </>
  );
};

interface ProfilePicFormProps {
  url: string;
  profilePic: {
    file: any;
    preview: any;
    url: string;
  };
  setProfilePic: (
    value: SetStateAction<{
      file: any;
      preview: any;
      url: string;
    }>
  ) => void;
}

export const ProfilePicForm = ({
  url,
  profilePic,
  setProfilePic,
}: ProfilePicFormProps) => {
  useEffect(() => {
    if (url !== null && url !== "") {
      setProfilePic((prevState) => {
        return {
          ...prevState,
          url: `${url}`,
        };
      });
    }

    return () =>
      setProfilePic({
        file: "",
        preview: "",
        url: url !== null && url !== "" ? url : "/images/profile_pic.jpg",
      });
  }, [url, setProfilePic]);

  const handleProfilePicChange = (event: any) => {
    event.preventDefault();

    const reader = new FileReader();
    const file = event.target.files[0];
    const maxSize = 1024 * 1024 * 2;
    if (
      file &&
      (file.type === "image/jpeg" || file.type === "image/png") &&
      file.size <= maxSize
    ) {
      reader.onloadend = () => {
        setProfilePic((prevState) => {
          return {
            ...prevState,
            file: file,
            preview: reader.result,
          };
        });
      };
      reader.readAsDataURL(file);
    }
  };

  return (
    <>
      <div
        className={
          "group relative w-44 h-44 overflow-hidden rounded-full bg-gray-300 cursor-pointer border-2 border-white m-auto shadow-md"
        }
      >
        <label htmlFor="profile-pic">
          <img
            src={profilePic.preview ? profilePic.preview : profilePic.url}
            alt={"Profile"}
            className="object-cover w-full h-full block"
          />
          <span className="absolute top-1/2 text-center w-44 h-44 opacity-0 group-hover:opacity-100 transition-opacity duration-300 ease-in bg-gray-400 bg-opacity-70 text-white p-2">
            <FontAwesomeIcon icon={faCamera} size="2x" />
            <div className="text-xs">
              Change Profile
              <br /> Picture
            </div>
          </span>
        </label>
        <input
          id="profile-pic"
          type="file"
          onChange={handleProfilePicChange}
          accept=".jpg,.jpeg,.png"
          className={"hidden"}
        />
      </div>
    </>
  );
};
