import { Button, Divider, Empty, Typography } from "antd";
import { motion } from "framer-motion";
import React, { SetStateAction, useCallback, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Card } from "../../components/Card";
import ImageDropZone from "../../components/ImageDropZone";
import ImageThumbs from "../../components/ImageThumbs";
import PageTitle from "../../components/PageTitle";
import {
  AddressForm,
  ChangePasswordForm,
  CompanyForm,
  CompanyInfoEduForm,
  IndividualForm,
  IndividualInfoEduForm,
  ProfilePicForm,
  ServicesForm,
} from "../../components/SubForms";
import { useGetUserInfo } from "../../hooks";
import { useForm } from "../../hooks/useForm";
import { useModalHook } from "../../hooks/useModalHook";
import { ChangePasswordEffect, UpdateUserInfo } from "../../store/Effects";
import {
  ChangePasswdInput,
  UserSignupFormModel,
  UserTypeEnum,
} from "../../types";
import {
  changePasswordInitErrors,
  changePasswordInitValues,
  userInitErrors,
  userInitValues,
} from "../../utils/formInits";
import {
  changePasswdValidate,
  companyFormSPValidate,
  companyFormValidate,
  individualFormSPValidate,
  individualFormValidate,
} from "../../utils/validations";


export default function AccountSettings() {
  const user = useGetUserInfo();
  const [formValues, setFormValues] = useState<UserSignupFormModel>(
    userInitValues
  );
  const [
    changePasswdValues,
    setChangePasswdValues,
  ] = useState<ChangePasswdInput>(changePasswordInitValues);

  const [showForm, setShowForm] = useState<
    "INFO" | "PASSWORD" | "SKILLS" | "DOCUMENTS"
  >(user.forgetPassword ? "PASSWORD" : "INFO");

  const [profilePic, setProfilePic] = useState<{
    file: any;
    preview: any;
    url: string;
  }>({
    file: "",
    preview: "",
    url: "/images/profile_pic.jpg",
  });

  const setValuesToData = useCallback(
    () =>
      setFormValues({
        BRELA: user.BRELA,
        NIDA: user.NIDA,
        companyName: user.companyName,
        confirmPassword: user.confirmPassword,
        contactPerson: user.contactPerson,
        district: user.district,
        documentsUrl: user.documentsUrl,
        email: user.email,
        experience: user.experience,
        firstName: user.firstName,
        isActive: user.isActive,
        jobTitle: user.jobTitle,
        lastName: user.lastName,
        noOfEmpoyees: user.noOfEmpoyees,
        password: user.password,
        phoneNumber: user.phoneNumber,
        profilePicUrl: user.profilePicUrl,
        region: user.region,
        skills: user.skills,
        streetAddress: user.streetAddress,
        userType: user.userType,
        vocationTraining: user.vocationTraining,
        ward: user.ward,
        expDetails: user.expDetails,
        jobsCanBuy: user.jobsCanBuy,
        refPersonName: user.refPersonName,
        refPhoneNumber: user.refPhoneNumber,
      }),
    [user]
  );

  useEffect(() => {
    if (user._id !== "") {
      setValuesToData();
    } else {
      setFormValues(userInitValues);
    }
  }, [setValuesToData, user]);

  const changeShowForm = (
    data: "INFO" | "PASSWORD" | "SKILLS" | "DOCUMENTS"
  ) => {
    setValuesToData();
    setShowForm(data);
  };

  const Info = () => (
    <>
      <div className="-mb-16">
        <ProfilePicForm
          url={user.profilePicUrl}
          profilePic={profilePic}
          setProfilePic={setProfilePic}
        />
      </div>
      <Card>
        <div className="mt-16">
          {(user.userType === UserTypeEnum.SP_IND ||
            user.userType === UserTypeEnum.SR_IND ||
            user.userType === UserTypeEnum.ADMIN) && (
            <IndividualInfo
              setValues={setFormValues}
              values={formValues}
              id={user._id}
              profilePic={profilePic.file}
            />
          )}

          {(user.userType === UserTypeEnum.SR_COM ||
            user.userType === UserTypeEnum.SP_COM) && (
            <CompanyInfo
              setValues={setFormValues}
              values={formValues}
              id={user._id}
              profilePic={profilePic.file}
            />
          )}
        </div>
      </Card>
    </>
  );
  const Password = () => (
    <>
      <Card>
        <ChangePassword
          setValues={setChangePasswdValues}
          values={changePasswdValues}
        />
      </Card>
    </>
  );
  const Skills = () => (
    <>
      <Card>
        <ChangeSkills
          setValues={setFormValues}
          values={formValues}
          id={user._id}
        />
      </Card>
    </>
  );
  const Documents = () => (
    <>
      <Card>
        <ChangeDocuments
          setValues={setFormValues}
          values={formValues}
          id={user._id}
        />
      </Card>
    </>
  );

  return (
    <motion.div exit={{ opacity: 0 }}>
      <PageTitle
        heading="Account Settings"
        subheading={
          showForm === "INFO"
            ? "Change your basic information"
            : showForm === "DOCUMENTS"
            ? "Add or remove relevent documents"
            : showForm === "PASSWORD"
            ? "Change Password"
            : "Add or remove skills"
        }
        icon={"pe-7s-settings"}
        buttons={
          <div className="flex flex-row flex-wrap justify-center gap-2">
            {showForm !== "INFO" ? (
              <Button
                onClick={() => changeShowForm("INFO")}
                disabled={user.forgetPassword}
              >
                Info
              </Button>
            ) : null}
            {showForm !== "PASSWORD" ? (
              <Button onClick={() => changeShowForm("PASSWORD")}>
                Password
              </Button>
            ) : null}
            {showForm !== "SKILLS" &&
            (user.userType === UserTypeEnum.SP_COM ||
              user.userType === UserTypeEnum.SP_IND) ? (
              <Button
                onClick={() => changeShowForm("SKILLS")}
                disabled={user.forgetPassword}
              >
                Skills
              </Button>
            ) : null}
            {showForm !== "DOCUMENTS" &&
            (user.userType === UserTypeEnum.SP_COM ||
              user.userType === UserTypeEnum.SP_IND) ? (
              <Button
                onClick={() => changeShowForm("DOCUMENTS")}
                disabled={user.forgetPassword}
              >
                Documents
              </Button>
            ) : null}
          </div>
        }
      />
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <div className="p-8 max-w-5xl m-auto">
          {showForm === "INFO"
            ? Info()
            : showForm === "PASSWORD"
            ? Password()
            : showForm === "DOCUMENTS"
            ? Documents()
            : Skills()}
        </div>
      </motion.div>
    </motion.div>
  );
}

const IndividualInfo = (props: {
  id: string;
  values: UserSignupFormModel;
  setValues: (value: SetStateAction<UserSignupFormModel>) => void;
  profilePic: any;
}) => {
  const form = useForm(
    userInitValues,
    userInitErrors,
    props.values.userType === UserTypeEnum.SP_IND
      ? individualFormSPValidate
      : individualFormValidate,
    props.setValues,
    props.values
  );
  const dispatch = useDispatch();

  const handleSave = async () => {
    const valid = await form.formValidation();
    if (!valid) return;
    dispatch(UpdateUserInfo(props.id, props.values, props.profilePic));
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 1 }}
    >
      <IndividualForm
        values={props.values}
        fieldDisable={false}
        errors={form.formErrors}
        handleBlur={form.handleBlur}
        handleChange={form.handleChange}
        disableEmailPhone={true}
      />
      <AddressForm
        values={props.values}
        fieldDisable={false}
        errors={form.formErrors}
        handleBlur={form.handleBlur}
        handleChange={form.handleChange}
        cascaderSelect={form.cascaderSelect}
      />
      {props.values.userType === UserTypeEnum.SP_IND && (
        <IndividualInfoEduForm
          values={props.values}
          fieldDisable={false}
          errors={form.formErrors}
          handleBlur={form.handleBlur}
          handleChange={form.handleChange}
          numberChange={form.numberChange}
        />
      )}

      <Button type="primary" onClick={handleSave} className="logo-purple-color mt-4">
        Save Changes
      </Button>
    </motion.div>
  );
};

const CompanyInfo = (props: {
  id: string;
  values: UserSignupFormModel;
  setValues: (value: SetStateAction<UserSignupFormModel>) => void;
  profilePic: any;
}) => {
  const form = useForm(
    userInitValues,
    userInitErrors,
    props.values.userType === UserTypeEnum.SP_COM
      ? companyFormSPValidate
      : companyFormValidate,
    props.setValues,
    props.values
  );

  const dispatch = useDispatch();

  const handleSave = async () => {
    const valid = await form.formValidation();
    if (!valid) return;
    dispatch(UpdateUserInfo(props.id, props.values, props.profilePic));
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 1 }}
    >
      <CompanyForm
        values={props.values}
        fieldDisable={false}
        errors={form.formErrors}
        handleBlur={form.handleBlur}
        handleChange={form.handleChange}
        disableEmailPhone={true}
      />
      <AddressForm
        values={props.values}
        fieldDisable={false}
        errors={form.formErrors}
        handleBlur={form.handleBlur}
        handleChange={form.handleChange}
        cascaderSelect={form.cascaderSelect}
      />

      {props.values.userType === UserTypeEnum.SP_COM && (
        <CompanyInfoEduForm
          values={props.values}
          fieldDisable={false}
          errors={form.formErrors}
          handleBlur={form.handleBlur}
          handleChange={form.handleChange}
          numberChange={form.numberChange}
        />
      )}

      <Button type="primary" onClick={handleSave} className="logo-purple-color mt-4">
        Save Changes
      </Button>
    </motion.div>
  );
};

const ChangePassword = (props: {
  values: ChangePasswdInput;
  setValues: (value: SetStateAction<ChangePasswdInput>) => void;
}) => {
  const [err, setErr] = useState<{ err: boolean; msg: string }>({
    err: false,
    msg: "",
  });

  const form = useForm(
    changePasswordInitValues,
    changePasswordInitErrors,
    changePasswdValidate,
    props.setValues,
    props.values
  );
  const dispatch = useDispatch();

  const handleSave = async () => {
    const valid = await form.formValidation();
    if (!valid) return;

    if (props.values.newPassword !== props.values.confirmPassword) {
      setErr({
        err: true,
        msg: "Password & Confirm Password are not same. Please check",
      });
      return;
    }

    dispatch(ChangePasswordEffect(props.values));
    props.setValues(changePasswordInitValues);
  };

  useModalHook(err.err, err.msg, "warning", () =>
    setErr({ err: false, msg: "" })
  );

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 1 }}
    >
      <form>
        <ChangePasswordForm
          values={props.values}
          errors={form.formErrors}
          handleBlur={form.handleBlur}
          handleChange={form.handleChange}
        />
      </form>

      <Button type="primary" onClick={handleSave} className="logo-purple-color mt-4">
        Save Changes
      </Button>
    </motion.div>
  );
};

const ChangeSkills = (props: {
  id: string;
  values: UserSignupFormModel;
  setValues: (value: SetStateAction<UserSignupFormModel>) => void;
}) => {
  const [err, setErr] = useState<{ err: boolean; msg: string }>({
    err: false,
    msg: "",
  });

  const dispatch = useDispatch();

  const handleSave = () => {
    if (props.values.skills.length === 0) {
      setErr({
        err: true,
        msg: "Please enter Professional and Type of Skills",
      });
      return;
    }
    dispatch(UpdateUserInfo(props.id, props.values));
  };

  useModalHook(err.err, err.msg, "warning", () =>
    setErr({ err: false, msg: "" })
  );

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 1 }}
    >
      <ServicesForm setValues={props.setValues} values={props.values} />
      <Button  type="primary" onClick={handleSave} className="logo-purple-color mt-4">
        Save Changes
      </Button>
    </motion.div>
  );
};
interface ChangeDocumentsProps {
  id: string;
  values: UserSignupFormModel;
  setValues: (value: SetStateAction<UserSignupFormModel>) => void;
}

const ChangeDocuments = ({ id, values, setValues }: ChangeDocumentsProps) => {
  const [files, setFiles] = useState<any[]>([]);
  const [newFiles, setNewFiles] = useState<any[]>([]);

  const [err, setErr] = useState<{ err: boolean; msg: string }>({
    err: false,
    msg: "",
  });

  useEffect(() => {
    setFiles([
      ...values.documentsUrl.map((file) =>
        Object.assign({
          preview: file,
          name: file.split("/")[file.split("/").length - 1],
        })
      ),
    ]);
  }, [values.documentsUrl]);

  const dispatch = useDispatch();

  const handleSave = () => {
    if (files.length === 0 && newFiles.length === 0) {
      setErr({
        err: true,
        msg: "Please upload relevent documents.",
      });
      return;
    }
    dispatch(UpdateUserInfo(id, values, "", files, newFiles));
    setNewFiles([]);
  };

  useModalHook(err.err, err.msg, "warning", () =>
    setErr({ err: false, msg: "" })
  );

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      transition={{ duration: 1 }}
    >
      <Typography.Title level={5}>Already Uploaded Documents:</Typography.Title>
      {files.length > 0 ? (
        <ImageThumbs files={files} setFiles={setFiles} showDelete={true} />
      ) : (
        <Empty />
      )}
      <Divider />
      <div className="mt-8">
        <ImageDropZone files={newFiles} setFiles={setNewFiles} />
      </div>
      <Button type="primary" onClick={handleSave} className="logo-purple-color mt-4">
        Save Changes
      </Button>
    </motion.div>
  );
};
