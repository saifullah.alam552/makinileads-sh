import { faArrowLeft } from "@fortawesome/free-solid-svg-icons/faArrowLeft";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Radio } from "antd";
import { motion } from "framer-motion";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import InputField from "../../antd/InputField";
import { Card } from "../../components/Card";
import PageTitle from "../../components/PageTitle";
import { useGetCategoryDetailById } from "../../hooks";
import { useForm } from "../../hooks/useForm";
import { AddEditCategory } from "../../store/Effects";
import { CategoryFormModel } from "../../types";
import { categoryInitValues, cateogoryInitErrors } from "../../utils/formInits";
import { validateCategoryForm } from "../../utils/validations";

export default function CategoryForm() {
  const [formValues, setFormValues] = useState<CategoryFormModel>(
    categoryInitValues
  );

  const { catId } = useParams<any>();
  const history = useHistory();
  const dispatch = useDispatch();
  const category = useGetCategoryDetailById(catId);

  const form = useForm(
    categoryInitValues,
    cateogoryInitErrors,
    validateCategoryForm,
    setFormValues,
    formValues
  );

  const setData = useCallback(
    () => setFormValues({ title: category.title, isActive: category.isActive }),
    [category]
  );

  useEffect(() => {
    if (catId && catId !== "" && category._id) {
      setData();
    } else {
      setFormValues(categoryInitValues);
    }
  }, [catId, category._id, setData]);

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    const valid = await form.formValidation();
    if (!valid) return;
    dispatch(AddEditCategory(formValues, catId))
  };

  return (
    <motion.div exit={{ opacity: 0 }}>
      <PageTitle
        heading={catId ? "Edit Category" : "Add Category"}
        icon={"pe-7s-tools"}
        buttons={
          <div>
            <Button onClick={() => history.goBack()}>
              <FontAwesomeIcon icon={faArrowLeft} size="1x" />
            </Button>
          </div>
        }
      />

      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <div className="p-8 max-w-6xl m-auto">
          <Card>
            <form onSubmit={handleSubmit}>
              <div className="flex flex-col w-full">
                <InputField
                  id="title"
                  name="title"
                  type="simple"
                  label="Cateogry Title"
                  placeHolder="Enter category title"
                  value={formValues.title}
                  onChange={form.handleChange}
                  error={form.formErrors.title}
                  onBlur={form.handleBlur}
                  required={true}
                  displayError={true}
                />
              </div>
              <div className="flex items-center gap-4">
                <div>Status:</div>
                <Radio.Group
                  value={formValues.isActive}
                  name={"isActive"}
                  onChange={form.handleChange}
                >
                  <Radio value={true}>Active</Radio>
                  <Radio value={false}>Blocked</Radio>
                </Radio.Group>
              </div>
              <div className="space-x-4 mt-5">
                <Button  className="logo-purple-color" type="primary" htmlType="submit">
                  Save
                </Button>
                <Button onClick={() => history.goBack()}>Cancel</Button>
              </div>
            </form>
          </Card>
        </div>
      </motion.div>
    </motion.div>
  );
}
