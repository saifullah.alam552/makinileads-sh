import { motion } from "framer-motion";
import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useGetUserInfo } from "../../hooks";
import ManageJobs from "./ManageJobs";

export default function Dashboard() {
  const user = useGetUserInfo();
  const history = useHistory();

  useEffect(() => {
    if (user.forgetPassword) {
      history.push("/dashboard/account-settings");
    }
  }, [history, user.forgetPassword]);

  return (
    <>
      <motion.div exit={{ opacity: 0 }}>
        <ManageJobs />
      </motion.div>
      
    </>
  );
}
