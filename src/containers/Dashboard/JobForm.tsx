import { faArrowLeft } from "@fortawesome/free-solid-svg-icons/faArrowLeft";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button } from "antd";
import { motion } from "framer-motion";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { Card, UserCard } from "../../components/Card";
import PageTitle from "../../components/PageTitle";
import {
  AddressForm,
  JobDetailForm,
  ServicesForm,
} from "../../components/SubForms";
import { useGetJobs, useGetUserInfo } from "../../hooks";
import { useForm } from "../../hooks/useForm";
import { useModalHook } from "../../hooks/useModalHook";
import { AddEditJob } from "../../store/Effects";
import { JobFormModel } from "../../types";
import { JobQuery } from "../../utils/apiUrls";
import { jobInitErrors, jobInitValues } from "../../utils/formInits";
import { jobFormValidation } from "../../utils/validations";

export default function JobForm() {
  const [formValues, setFormValues] = useState<JobFormModel>(jobInitValues);
  const [err, setErr] = useState<{ err: boolean; msg: string }>({
    err: false,
    msg: "",
  });
  const { action, jobId } = useParams<any>();
  const history = useHistory();
  const dispatch = useDispatch();

  const [rq] = useState<JobQuery>({
    id: jobId,
    postedBy: true,
  });
  const { job } = useGetJobs(rq);
  const user = useGetUserInfo();

  const form = useForm(
    jobInitValues,
    jobInitErrors,
    jobFormValidation,
    setFormValues,
    formValues
  );

  const setData = useCallback(
    () =>
      setFormValues({
        district: job.district,
        isActive: job.isActive,
        jobAmount: job.jobAmount,
        jobDescription: job.jobDescription,
        jobStatus: job.jobStatus,
        jobTitle: job.jobTitle,
        postedBy: job.postedBy._id,
        region: job.region,
        skills: job.skills,
        streetAddress: job.streetAddress,
        ward: job.ward,
      }),
    [job]
  );

  useEffect(() => {
    if (jobId && jobId !== "new" && job._id) {
      setData();
    } else {
      setFormValues(jobInitValues);
    }
  }, [job._id, jobId, setData]);

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    const valid = await form.formValidation();
    if (!valid) return;
    if (formValues.skills.length === 0) {
      setErr({
        err: true,
        msg: "Please enter Professional and Type of Skills",
      });
      return;
    }
    if (jobId === "new") formValues.postedBy = user._id;
    dispatch(AddEditJob(formValues, jobId, history));
  };

  useModalHook(err.err, err.msg, "warning", () =>
    setErr({ err: false, msg: "" })
  );

  return (
    <motion.div exit={{ opacity: 0 }}>
      <PageTitle
        heading={action === "add" ? "Add Job" : "Edit Job"}
        icon={"pe-7s-note2"}
        buttons={
          <div>
            <Button onClick={() => history.goBack()}>
              <FontAwesomeIcon icon={faArrowLeft} size="1x" />
            </Button>
          </div>
        }
      />
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <div className="p-8 max-w-6xl m-auto">
          <Card>
            <form onSubmit={handleSubmit}>
              <JobDetailForm
                values={formValues}
                errors={form.formErrors}
                handleBlur={form.handleBlur}
                handleChange={form.handleChange}
                numberChange={form.numberChange}
                fieldDisable={false}
              />
              <AddressForm
                values={formValues}
                errors={form.formErrors}
                handleBlur={form.handleBlur}
                handleChange={form.handleChange}
                cascaderSelect={form.cascaderSelect}
                fieldDisable={false}
              />
              <ServicesForm setValues={setFormValues} values={formValues} />
              <div className="space-x-4 mt-5">
                <Button className="logo-purple-color" type="primary" htmlType="submit">
                  Save
                </Button>
                <Button onClick={() => history.goBack()}>Cancel</Button>
              </div>
            </form>
          </Card>

          <div className="grid grid-cols-1 lg:grid-cols-2 gap-4 mt-4">
            {jobId !== "new" && (
              <div>
                <div className="font-semibold m-2">Posted by:</div>
                <UserCard
                  user={job.postedBy}
                  pathname={"/dashboard/user-details"}
                />
              </div>
            )}
          </div>
        </div>
      </motion.div>
    </motion.div>
  );
}
