import { faArrowLeft } from "@fortawesome/free-solid-svg-icons/faArrowLeft";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Descriptions, Modal, Radio, Select } from "antd";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import { motion } from "framer-motion";
import React, { useEffect, useLayoutEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Link, useHistory, useParams } from "react-router-dom";
import {
  Card,
  SRComUserDetails,
  SRIndUserDetails,
  UserCard,
} from "../../components/Card";
import ModalReact from "../../components/ModalReact";
import PageTitle from "../../components/PageTitle";
import SelectedSkills from "../../components/SelectedSkills";
import { useGetJobs, useGetUserInfo } from "../../hooks";
import { ChangeJobStatus, PurchaseJob } from "../../store/Effects";
import {
  JobPurchaseForm,
  JobPurchaseType,
  JobStatusChangeForm,
  JobStatusEnum,
  UserTypeEnum,
} from "../../types";
import { JobQuery } from "../../utils/apiUrls";
import { JobPurchaseInitValues } from "../../utils/formInits";
import { currencyFormat } from "../../utils/helpers";

dayjs.extend(relativeTime);

export default function JobView() {
  const { jobId } = useParams<any>();
  const history = useHistory();
  const user = useGetUserInfo();
  const [showModal, setShowModal] = useState<boolean>(false);
  const [purchased, setPurchased] = useState<boolean>(false);
  const [buyOptions, setBuyOptions] = useState<JobPurchaseForm>(
    JobPurchaseInitValues
  );
  const [showBuyModal, setShowBuyModal] = useState<boolean>(false);
  const [jobStatusValues, setJobStatusValues] = useState<JobStatusChangeForm>({
    isActive: false,
    jobStatus: JobStatusEnum.OPEN,
  });
  const [rq] = useState<JobQuery>({
    id: jobId,
    postedBy: true,
    purchasedBy: true,
  });
  const { job } = useGetJobs(rq);
  const dispatch = useDispatch();
  const [isSp, setIsSp] = useState<boolean>(false);

  useEffect(() => {
    if (
      user.userType === UserTypeEnum.SP_IND ||
      user.userType === UserTypeEnum.SP_COM
    ) {
      setIsSp(true);
    } else {
      setIsSp(false);
    }
  }, [user.userType]);

  useLayoutEffect(() => {
    const index = job.purchasedBy.findIndex((d) => d._id === user._id);
    if (index !== -1) {
      setPurchased(true);
    } else {
      setPurchased(false);
    }
  }, [job.purchasedBy, user._id]);

  useEffect(() => {
    setJobStatusValues({ jobStatus: job.jobStatus, isActive: job.isActive });
  }, [job.isActive, job.jobStatus]);

  const onChangeIsActive = (e: any) => {
    const value = e.target.value;
    setJobStatusValues({ ...jobStatusValues, isActive: value });
    console.log(value);
  };

  const onChangeJobStatus = (value: any) => {
    setJobStatusValues({ ...jobStatusValues, jobStatus: value });
  };

  const onCancel = () => {
    setJobStatusValues({ jobStatus: job.jobStatus, isActive: job.isActive });
    setShowModal(false);
  };

  const onOkay = () => {
    dispatch(ChangeJobStatus(jobStatusValues, jobId));
    setShowModal(false);
  };

  const showBuyOptions = (options: JobPurchaseForm) => {
    setBuyOptions(options);
    setShowBuyModal(true);
  };

  const onPurchsae = (data: JobPurchaseForm) => {
    setShowBuyModal(false);
    dispatch(PurchaseJob(data, rq));
  };

  return (
    <motion.div exit={{ opacity: 0 }}>
      <PageTitle
        heading={job.jobTitle}
        subheading={"Job Details"}
        icon={"pe-7s-note2"}
        buttons={
          <div className="flex space-x-4">
            <Button onClick={() => history.goBack()}>
              <FontAwesomeIcon icon={faArrowLeft} size="1x" />
            </Button>
            {(user.userType === UserTypeEnum.ADMIN ||
              user.userType === UserTypeEnum.SR_COM ||
              user.userType === UserTypeEnum.SR_IND) && (
              <>
                <Link to={`/dashboard/job/edit/${job._id}`}>
                  <Button type="primary">Edit</Button>
                </Link>
                <Button
                  type="primary"
                  danger
                  onClick={() => setShowModal(true)}
                >
                  Change Status
                </Button>
              </>
            )}
            {isSp ? (
              !purchased ? (
                <Button
                  type="primary"
                  className="w-full"
                  onClick={() =>
                    showBuyOptions({
                      jobId: job._id,
                      jobPurchaseType: job.jobPurchaseType,
                    })
                  }
                  disabled={
                    job.purchasedBy.length === 3 ||
                    job.isActive === false ||
                    job.jobPurchaseType === JobPurchaseType.EXCLUSIVE
                  }
                >
                  Buy this Job
                </Button>
              ) : null
            ) : null}
          </div>
        }
      />
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <div className="p-8 max-w-6xl m-auto">
          <div className="grid grid-cols-1 lg:grid-cols-3 gap-4">
            <div className="col-span-2">
              <div className="">
                <div>
                  <Card>
                    <Descriptions
                      size="small"
                      column={{ xs: 1, sm: 1, md: 2 }}
                      className="w-full bg-white p-2 rounded"
                    >
                      <Descriptions.Item label="Job Title">
                        <div className="font-semibold">{job.jobTitle}</div>
                      </Descriptions.Item>
                      <Descriptions.Item label="Job Amount">
                        <div className="font-semibold">
                          $ {currencyFormat(job.jobAmount)}
                        </div>
                      </Descriptions.Item>
                      {isSp ? (
                        purchased ? (
                          <Descriptions.Item label="Address">
                            <div className="font-semibold">
                              {job.streetAddress}
                            </div>
                          </Descriptions.Item>
                        ) : null
                      ) : (
                        <Descriptions.Item label="Address">
                          <div className="font-semibold">
                            {job.streetAddress}
                          </div>
                        </Descriptions.Item>
                      )}
                      <Descriptions.Item label="Ward">
                        <div className="font-semibold">{job.ward}</div>
                      </Descriptions.Item>
                      <Descriptions.Item label="District">
                        <div className="font-semibold">{job.district}</div>
                      </Descriptions.Item>
                      <Descriptions.Item label="Region">
                        <div className="font-semibold"> {job.region}</div>
                      </Descriptions.Item>
                      <Descriptions.Item label="Created">
                        <div className="font-semibold">
                          {dayjs(job.createdAt).fromNow()}
                        </div>
                      </Descriptions.Item>
                      {job.jobPurchaseType !== JobPurchaseType.NONE && (
                        <Descriptions.Item label="Purchase Type">
                          <div className="font-semibold">
                            {job.jobPurchaseType}
                          </div>
                        </Descriptions.Item>
                      )}
                    </Descriptions>
                    <div className="my-2">
                      <div className="font-normal">Job Description:</div>
                      <div className="font-semibold mx-6 text-justify overflow-hidden">
                        {job.jobDescription}
                      </div>
                    </div>
                    <SelectedSkills jobSkills={job.skills} />
                  </Card>
                </div>

                <div>
                  {!isSp && job.purchasedBy.length > 0 ? (
                    <>
                      <div className="font-semibold text-md m-2 mt-4">
                        Purchased By:
                      </div>
                      {job.purchasedBy.map((d, i) => (
                        <div key={i} className="mb-4">
                          <UserCard
                            key={d._id}
                            user={d}
                            pathname={"/dashboard/user-details"}
                          />
                        </div>
                      ))}
                    </>
                  ) : null}
                </div>
              </div>
            </div>

            <div className="">
              <div className="font-semibold text-md m-2">
                Job Status:
                <span className="m-2">
                  {!isSp ? (
                    job.isActive ? (
                      <span className="bg-green-300 px-2 py-1 rounded">
                        ACTIVE
                      </span>
                    ) : (
                      <span className="bg-red-300 px-2 py-1 rounded">
                        BLOCKED
                      </span>
                    )
                  ) : null}
                </span>
                <span className="bg-blue-600 px-2 py-1 rounded text-white">
                  {job.jobStatus}
                </span>
              </div>

              <div className="font-semibold text-md m-2 mt-4">
                Purchase Type:
                <span className="bg-yellow-800 px-2 py-1 m-2 rounded text-white ">
                  {job.jobPurchaseType}
                </span>
              </div>
              {isSp ? (
                purchased ? (
                  <>
                    <div className="font-semibold text-md m-2 mt-4">
                      Posted by:
                    </div>
                    <Card>
                      {job.postedBy.userType === UserTypeEnum.SR_IND && (
                        <SRIndUserDetails user={job.postedBy} small={true} />
                      )}

                      {job.postedBy.userType === UserTypeEnum.SR_COM && (
                        <SRComUserDetails user={job.postedBy} small={true} />
                      )}

                      {job.postedBy.userType === UserTypeEnum.ADMIN && (
                        <SRIndUserDetails user={job.postedBy} small={true} />
                      )}
                    </Card>
                  </>
                ) : null
              ) : (
                <>
                  <div className="font-semibold text-md m-2 mt-4">
                    Posted by:
                  </div>
                  <Card>
                    {job.postedBy.userType === UserTypeEnum.SR_IND && (
                      <SRIndUserDetails user={job.postedBy} small={true} />
                    )}

                    {job.postedBy.userType === UserTypeEnum.SR_COM && (
                      <SRComUserDetails user={job.postedBy} small={true} />
                    )}

                    {job.postedBy.userType === UserTypeEnum.ADMIN && (
                      <SRIndUserDetails user={job.postedBy} small={true} />
                    )}
                  </Card>
                </>
              )}
            </div>
          </div>
        </div>
      </motion.div>
      <Modal
        title="Change Status"
        visible={showModal}
        onCancel={onCancel}
        onOk={onOkay}
      >
        <div className="flex flex-col w-full justify-items-end gap-4">
          {user.userType === UserTypeEnum.ADMIN && (
            <div>
              <Radio.Group
                value={jobStatusValues.isActive}
                buttonStyle="solid"
                onChange={onChangeIsActive}
              >
                <Radio.Button value={true}>Active</Radio.Button>
                <Radio.Button value={false}>Blocked</Radio.Button>
              </Radio.Group>
            </div>
          )}
          <div>
            <Select
              value={jobStatusValues.jobStatus}
              className="w-full"
              onChange={onChangeJobStatus}
            >
              <Select.Option value={JobStatusEnum.CONTACTED}>
                CONTACTED
              </Select.Option>
              <Select.Option value={JobStatusEnum.COMPLETED}>
                COMPLETED
              </Select.Option>
              {user.userType === UserTypeEnum.ADMIN && (
                <>
                  <Select.Option
                    value={JobStatusEnum.REPOSTED}
                    disabled={user.userType !== UserTypeEnum.ADMIN}
                  >
                    REPOSTED
                  </Select.Option>
                </>
              )}
            </Select>
          </div>
        </div>
      </Modal>

      <ModalReact
        id="buy-options"
        isOpen={showBuyModal}
        onClose={() => setShowBuyModal(false)}
        body={
          <div className="flex flex-col w-60 md:w-96 space-y-4">
            {buyOptions.jobPurchaseType !== JobPurchaseType.GENERAL && (
              <Button
                type="primary"
                block
                size="large"
                className="bg-indigo-800 border-indigo-600 hover:bg-indigo-700 hover:border-indigo-700"
                onClick={() =>
                  onPurchsae({
                    jobId: buyOptions.jobId,
                    jobPurchaseType: JobPurchaseType.EXCLUSIVE,
                  })
                }
              >
                Buy as Exclusive
              </Button>
            )}
            <Button
              type="primary"
              block
              size="large"
              className="bg-green-800 border-green-600 hover:bg-green-700 hover:border-green-700"
              onClick={() =>
                onPurchsae({
                  jobId: buyOptions.jobId,
                  jobPurchaseType: JobPurchaseType.GENERAL,
                })
              }
            >
              Buy as General
            </Button>
          </div>
        }
      />
    </motion.div>
  );
}
