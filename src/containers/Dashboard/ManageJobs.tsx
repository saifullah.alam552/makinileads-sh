import { faArrowLeft } from "@fortawesome/free-solid-svg-icons/faArrowLeft";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons/faArrowRight";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Pagination } from "antd";
import { motion } from "framer-motion";
import React, { useCallback, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { JobSummaryCard } from "../../components/Card";
import { JobFilterOptions } from "../../components/Filters";
import PageTitle from "../../components/PageTitle";
import { useGetJobs, useGetUserInfo } from "../../hooks";
import { UserTypeEnum } from "../../types";
import { JobQuery } from "../../utils/apiUrls";

export default function ManageJobs() {
  const user = useGetUserInfo();

  const [rq, setRq] = useState<JobQuery>({});
  const { jobSummaries } = useGetJobs(rq);
  const [isSp, setIsSp] = useState<boolean>(false);
  const [showFilter, setShowFilter] = useState<boolean>(false);
  const [currentPage, setCurrentPage] = useState<number>(1);

  const freshRequest = useCallback(() => {
    if (
      user.userType === UserTypeEnum.SR_COM ||
      user.userType === UserTypeEnum.SR_IND
    ) {
      setRq({ summary: true, limit: 12, skip: 0, postedById: user._id });
    } else {
      setRq({ summary: true, limit: 12, skip: 0 });
    }
  }, [user._id, user.userType]);

  useEffect(() => {
    if (
      user.userType === UserTypeEnum.SP_COM ||
      user.userType === UserTypeEnum.SP_IND
    ) {
      setIsSp(true);
    } else {
      setIsSp(false);
    }

    freshRequest();
  }, [freshRequest, user._id, user.userType]);

  const pageItemRender = (
    page: number,
    type: "page" | "prev" | "next" | "jump-prev" | "jump-next",
    originalElement: React.ReactElement<HTMLElement>
  ) => {
    if (type === "prev") {
      return (
        <Button>
          <FontAwesomeIcon icon={faArrowLeft} />
        </Button>
      );
    }
    if (type === "next") {
      return (
        <Button>
          <FontAwesomeIcon icon={faArrowRight} />
        </Button>
      );
    }

    return originalElement;
  };

  const pageOnChange = (page: number, pageSize?: number) => {
    setCurrentPage(page);
    setRq({
      ...rq,
      limit: pageSize ? pageSize : 12,
      skip: (page - 1) * (pageSize ? pageSize : 12),
    });
  };

  const pageOnPageSizeChange = (current: number, size: number) => {
    setCurrentPage(current);
    setRq({
      ...rq,
      limit: size,
      skip: (current - 1) * size,
    });
  };

  return (
    <motion.div exit={{ opacity: 0 }}>
      <PageTitle
        heading={isSp ? "Jobs" : "Manage Jobs"}
        icon={"pe-7s-note2"}
        buttons={
          <div className="space-x-2">
            {!isSp && (
              <Link to={`/dashboard/job/add/new`}>
                <Button className="logo-purple-color">Add Job</Button>
              </Link>
            )}
            <Button onClick={() => setShowFilter(true)}>Filter Options</Button>
          </div>
        }
      />
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <div className="p-8 max-w-6xl m-auto">
          <JobFilterOptions
            showFilter={showFilter}
            setShowFilter={setShowFilter}
            rq={rq}
            setRq={setRq}
            userType={user.userType}
            userId={user._id}
            total={jobSummaries.total}
            fresh={freshRequest}
          />
          <div className="flex flex-row flex-wrap gap-4 justify-center xl:justify-start">
            {jobSummaries.data.map((job) => (
              <div key={job._id}>
                <JobSummaryCard
                  job={job}
                  extra={
                    <div className="flex flex-row flex-wrap gap-2 md:w-16">
                      <Link
                        to={`/dashboard/job/view/${job._id}`}
                        className="md:w-full"
                      >
                        <Button type="default" className="md:w-full">
                          View
                        </Button>
                      </Link>
                    </div>
                    // <>
                    //   {isSp ? (
                    //     <div className="flex flex-row flex-wrap gap-2 md:w-16">
                    //       {job.purchasedBy.includes(user._id) ? (
                    //         <Link
                    //           to={`/dashboard/job/view/${job._id}`}
                    //           className="md:w-full"
                    //         >
                    //           <Button type="default" className="md:w-full">
                    //             View
                    //           </Button>
                    //         </Link>
                    //       ) : (
                    //         <Button
                    //           type="primary"
                    //           className="md:w-full"
                    //           onClick={}
                    //           disabled={
                    //             job.purchasedBy.length === 3 ||
                    //             job.isActive === false ||
                    //             job.jobPurchaseType ===
                    //               JobPurchaseType.EXCLUSIVE
                    //           }
                    //         >
                    //           View
                    //         </Button>
                    //       )}
                    //     </div>
                    //   ) : (
                    //     <div className="flex flex-row flex-wrap gap-2 md:w-16">
                    //       <Link
                    //         to={`/dashboard/job/view/${job._id}`}
                    //         className="md:w-full"
                    //       >
                    //         <Button type="default" className="md:w-full">
                    //           View
                    //         </Button>
                    //       </Link>
                    //     </div>
                    //   )}
                    // </>
                  }
                />
              </div>
            ))}
          </div>
          <div className="text-center m-10">
            <Pagination
              current={currentPage}
              defaultCurrent={1}
              total={jobSummaries.total}
              itemRender={pageItemRender}
              showTotal={(total, range) =>
                `${range[0]}-${range[1]} of ${total} jobs`
              }
              onChange={pageOnChange}
              defaultPageSize={12}
              pageSizeOptions={["12", "24", "48", "96"]}
              onShowSizeChange={pageOnPageSizeChange}
              showSizeChanger
            />
          </div>
        </div>
      </motion.div>
      
    </motion.div>
  );
}
