import { faEdit } from "@fortawesome/free-regular-svg-icons/faEdit";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons/faTrashAlt";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Tag } from "antd";
import { motion } from "framer-motion";
import React from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import PageTitle from "../../components/PageTitle";
import { IManageSkillsData, useGetManageSkillsData } from "../../hooks";
import { DeleteCategory, DeleteSubCategory } from "../../store/Effects";

export default function ManageSkills() {
  const data = useGetManageSkillsData();

  return (
    <motion.div exit={{ opacity: 0 }}>
      <PageTitle
        heading="Manage Skills"
        icon={"pe-7s-tools"}
        buttons={
          <div className="space-x-2">
            <Link to={`/dashboard/manage-skills/category`}>
              <Button>Add Category</Button>
            </Link>
            <Link to={`/dashboard/manage-skills/skill`}>
              <Button className="logo-purple-color">Add Skill</Button>
            </Link>
          </div>
        }
      />

      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <div className="p-8 max-w-6xl m-auto">
          <div className="grid grid-cols-1 lg:grid-cols-1 gap-4">
            {data.map((skills) => (
              <SkillsCard
                skills={skills}
                pathname="/dashboard/manage-skills"
                key={skills._id}
              />
            ))}
          </div>
        </div>
      </motion.div>
    </motion.div>
  );
}

const SkillsCard = (props: { skills: IManageSkillsData; pathname: string }) => {
  const dispatch = useDispatch();
  return (
    <div>
      <div className="bg-gray-50 px-4 py-2 flex flex-row items-center w-md justify-between rounded shadow-md">
        <div className="flex flex-row flex-wrap gap-2">
          <div className="font-semibold">{props.skills.title}</div>
          <div>
            <Tag color={props.skills.isActive ? "green" : "red"}>
              {props.skills.isActive ? "Active" : "Blocked"}
            </Tag>
          </div>
        </div>

        <div className="text-center w-auto space-x-1">
          <Link to={`${props.pathname}/category/${props.skills._id}`}>
            <Button type="default">
              <FontAwesomeIcon icon={faEdit} />
            </Button>
          </Link>
          <Button
            type="default"
            danger
            onClick={() => dispatch(DeleteCategory(props.skills._id))}
            disabled={props.skills.subCategory.length > 0}
          >
            <FontAwesomeIcon icon={faTrashAlt} />
          </Button>
        </div>
      </div>

      <div className="grid grid-cols-1 lg:grid-cols-2 gap-2 ml-10 mt-2">
        {props.skills.subCategory.map((d) => (
          <div key={d._id}>
            <div className="bg-white px-4 py-2 flex flex-row items-center w-md justify-between rounded shadow-sm">
              <div className="flex flex-row flex-wrap gap-2">
                <div className="font-semibold">{d.title}</div>
                <div>
                  <Tag color={d.isActive ? "green" : "red"}>
                    {d.isActive ? "Active" : "Blocked"}
                  </Tag>
                </div>
              </div>

              <div className="text-center w-auto space-x-1">
                <Link to={`${props.pathname}/skill/${d._id}`}>
                  <Button type="default">
                    <FontAwesomeIcon icon={faEdit} />
                  </Button>
                </Link>
                <Button
                  type="default"
                  danger
                  onClick={() => dispatch(DeleteSubCategory(d._id))}
                >
                  <FontAwesomeIcon icon={faTrashAlt} />
                </Button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};
