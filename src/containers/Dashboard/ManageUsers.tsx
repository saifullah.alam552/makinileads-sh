import { Select } from "antd";
import { motion } from "framer-motion";
import React, { useEffect, useMemo, useState } from "react";
import InputField from "../../antd/InputField";
import { UserCard } from "../../components/Card";
import PageTitle from "../../components/PageTitle";
import { useDebounce, useGetAllUsers } from "../../hooks";
import { IUserData, UserTypeEnum } from "../../types";
import { UserQuery } from "../../utils/apiUrls";

export default function ManageUsers() {
  const [rq, setRq] = useState<UserQuery>({ type: undefined, name: undefined });
  const [userType, setUserType] = useState<UserTypeEnum | "ALL">("ALL");
  const [username, setUsername] = useState<string>("");
  const allUsers = useGetAllUsers(rq);

  const spInd: IUserData[] = useMemo(() => {
    return allUsers.filter((d) => d.userType === UserTypeEnum.SP_IND);
  }, [allUsers]);

  const spCom: IUserData[] = useMemo(() => {
    return allUsers.filter((d) => d.userType === UserTypeEnum.SP_COM);
  }, [allUsers]);

  const srInd: IUserData[] = useMemo(() => {
    return allUsers.filter((d) => d.userType === UserTypeEnum.SR_IND);
  }, [allUsers]);

  const srCom: IUserData[] = useMemo(() => {
    return allUsers.filter((d) => d.userType === UserTypeEnum.SR_COM);
  }, [allUsers]);

  const debouncedUsername = useDebounce(username, 1000);

  useEffect(() => {
    if (userType === "ALL") {
      setRq((prev) => ({ ...prev, type: undefined }));
    } else {
      setRq((prev) => ({ ...prev, type: userType }));
    }

    if (debouncedUsername) {
      setRq((prev) => ({ ...prev, name: debouncedUsername }));
    } else {
      setRq((prev) => ({ ...prev, name: undefined }));
    }
  }, [userType, debouncedUsername]);

  return (
    <motion.div exit={{ opacity: 0 }}>
      <PageTitle heading="Manage Users" icon={"pe-7s-users"} />

      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <div className="p-8 max-w-6xl m-auto">
          <div className="filter-for-all flex flex-wrap md:flex-row md:flex-nowrap  items-end gap-4 mb-4 font-bold text-md p-2 bg-gray-700 text-white rounded">
            <div className="flex flex-col w-full">
              <div>Select user type:</div>
              <div>
                <Select
                  value={userType}
                  className="w-full"
                  onChange={(value) => setUserType(value)}
                >
                  <Select.Option value={"ALL"}>ALL</Select.Option>
                  <Select.Option value={UserTypeEnum.SP_IND}>
                    Providers (Ind)
                  </Select.Option>
                  <Select.Option value={UserTypeEnum.SP_COM}>
                    Providers (Com)
                  </Select.Option>
                  <Select.Option value={UserTypeEnum.SR_IND}>
                    Receivers (Ind)
                  </Select.Option>
                  <Select.Option value={UserTypeEnum.SR_COM}>
                    Receivers (Com)
                  </Select.Option>
                </Select>
              </div>
            </div>
            <div className="flex flex-col w-full">
              <div>Search by user name: </div>
              <div>
                <InputField
                  id="username"
                  name="username"
                  type="simple"
                  placeHolder=""
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                  required={false}
                  displayError={false}
                />
              </div>
            </div>
          </div>

          {spInd.length > 0 && (
            <>
              <div className="mb-4 font-bold text-md p-2 bg-gray-300 rounded">
                Service Providers - Individuals
              </div>
              <div className="grid grid-cols-1 lg:grid-cols-2 gap-4">
                {spInd.map(
                  (user) =>
                    user.userType !== UserTypeEnum.ADMIN && (
                      <UserCard
                        key={user._id}
                        user={user}
                        pathname={"/dashboard/user-details"}
                      />
                    )
                )}
              </div>
            </>
          )}

          {spCom.length > 0 && (
            <>
              <div className="mb-4 mt-8 font-bold text-md p-2 bg-gray-300 rounded">
                Service Providers - Company
              </div>
              <div className="grid grid-cols-1 lg:grid-cols-2 gap-4">
                {spCom.map(
                  (user) =>
                    user.userType !== UserTypeEnum.ADMIN && (
                      <UserCard
                        key={user._id}
                        user={user}
                        pathname={"/dashboard/user-details"}
                      />
                    )
                )}
              </div>
            </>
          )}

          {srInd.length > 0 && (
            <>
              <div className="mb-4 mt-8 font-bold text-md p-2 bg-gray-300 rounded">
                Service Receivers - Individuals
              </div>
              <div className="grid grid-cols-1 lg:grid-cols-2 gap-4">
                {srInd.map(
                  (user) =>
                    user.userType !== UserTypeEnum.ADMIN && (
                      <UserCard
                        key={user._id}
                        user={user}
                        pathname={"/dashboard/user-details"}
                      />
                    )
                )}
              </div>
            </>
          )}

          {srCom.length > 0 && (
            <>
              <div className="mb-4 mt-8 font-bold text-md p-2 bg-gray-300 rounded">
                Service Receivers - Company
              </div>
              <div className="grid grid-cols-1 lg:grid-cols-2 gap-4">
                {srCom.map(
                  (user) =>
                    user.userType !== UserTypeEnum.ADMIN && (
                      <UserCard
                        key={user._id}
                        user={user}
                        pathname={"/dashboard/user-details"}
                      />
                    )
                )}
              </div>
            </>
          )}
        </div>
      </motion.div>
    </motion.div>
  );
}
