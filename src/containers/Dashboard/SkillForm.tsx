import { faArrowLeft } from "@fortawesome/free-solid-svg-icons/faArrowLeft";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Radio } from "antd";
import { motion } from "framer-motion";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import InputField from "../../antd/InputField";
import SelectField from "../../antd/SelectField";
import { Card } from "../../components/Card";
import PageTitle from "../../components/PageTitle";
import {
  useGetSelectCategories,
  useGetSubCategoryDetailById,
} from "../../hooks";
import { useForm } from "../../hooks/useForm";
import { AddEditSubCategory } from "../../store/Effects";
import { SubCategoryFormModel } from "../../types";
import {
  subCategoryInitValues,
  subCateogoryInitErrors,
} from "../../utils/formInits";
import { validateSubCategoryForm } from "../../utils/validations";

export default function SkillForm() {
  const [formValues, setFormValues] = useState<SubCategoryFormModel>(
    subCategoryInitValues
  );
  const categorySelect = useGetSelectCategories();
  const { skillId } = useParams<any>();
  const history = useHistory();
  const dispatch = useDispatch();
  const skill = useGetSubCategoryDetailById(skillId);

  const form = useForm(
    subCategoryInitValues,
    subCateogoryInitErrors,
    validateSubCategoryForm,
    setFormValues,
    formValues
  );

  const setData = useCallback(
    () =>
      setFormValues({
        title: skill.title,
        isActive: skill.isActive,
        category: skill.category._id,
      }),
    [skill]
  );

  useEffect(() => {
    if (skillId && skillId !== "" && skill._id) {
      setData();
    } else {
      setFormValues(subCategoryInitValues);
    }
  }, [setData, skill._id, skillId]);

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    const valid = await form.formValidation();
    if (!valid) return;
    dispatch(AddEditSubCategory(formValues, skillId));
  };

  return (
    <motion.div exit={{ opacity: 0 }}>
      <PageTitle
        heading={skillId ? "Edit Skill" : "Add Skill"}
        icon={"pe-7s-tools"}
        buttons={
          <div>
            <Button onClick={() => history.goBack()}>
              <FontAwesomeIcon icon={faArrowLeft} size="1x" />
            </Button>
          </div>
        }
      />

      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <div className="p-8 max-w-6xl m-auto">
          <Card>
            <form onSubmit={handleSubmit}>
              <div className="flex flex-col w-full">
                <SelectField
                  showSearch={false}
                  label={"Select Category"}
                  id={"category"}
                  value={formValues.category}
                  onChange={(value) => form.selectChange(value, "category")}
                  error={form.formErrors.category}
                  required={true}
                  data={categorySelect}
                  displayError={true}
                />
              </div>
              <div className="flex flex-col w-full">
                <InputField
                  id="title"
                  name="title"
                  type="simple"
                  label="Skill Title"
                  placeHolder="Enter skill title"
                  value={formValues.title}
                  onChange={form.handleChange}
                  error={form.formErrors.title}
                  onBlur={form.handleBlur}
                  required={true}
                  displayError={true}
                />
              </div>
              <div className="flex items-center gap-4">
                <div>Status:</div>
                <Radio.Group
                  value={formValues.isActive}
                  name={"isActive"}
                  onChange={form.handleChange}
                >
                  <Radio value={true}>Active</Radio>
                  <Radio value={false}>Blocked</Radio>
                </Radio.Group>
              </div>
              <div className="space-x-4 mt-5">
                <Button  className="logo-purple-color" type="primary" htmlType="submit">
                  Save
                </Button>
                <Button onClick={() => history.goBack()}>Cancel</Button>
              </div>
            </form>
          </Card>
        </div>
      </motion.div>
    </motion.div>
  );
}
