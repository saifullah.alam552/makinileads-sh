import { faArrowLeft } from "@fortawesome/free-solid-svg-icons/faArrowLeft";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Modal, Radio } from "antd";
import { motion } from "framer-motion";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import InputField from "../../antd/InputField";
import {
  Card,
  SPComUserDetails,
  SPIndUserDetails,
  SRComUserDetails,
  SRIndUserDetails
} from "../../components/Card";
import PageTitle from "../../components/PageTitle";
import { useGetUserDetailById } from "../../hooks";
import { ChangeIsActive, ChangeUserJobs } from "../../store/Effects";
import { UserTypeEnum } from "../../types";

export default function UserDetails() {
  const { userId } = useParams<any>();
  const user = useGetUserDetailById(userId ? userId : "");
  const history = useHistory();
  const [showModal, setShowModal] = useState<boolean>(false);
  const [isActive, setIsActive] = useState<boolean>(false);
  const [jobsCanBuy, setJobsCanBuy] = useState<number>(0);
  const dispatch = useDispatch();
  const [isSr, setIsSr] = useState<boolean>(false);

  useEffect(() => {
    setIsActive(user.isActive);
    setJobsCanBuy(user.jobsCanBuy);
  }, [user.isActive, user.jobsCanBuy]);

  useEffect(() => {
    if (
      user.userType === UserTypeEnum.SR_IND ||
      user.userType === UserTypeEnum.SR_COM
    ) {
      setIsSr(true);
    } else {
      setIsSr(false);
    }
  }, [user.userType]);

  const userName =
    user.userType === UserTypeEnum.SP_COM ||
    user.userType === UserTypeEnum.SR_COM
      ? user.companyName
      : `${user.firstName} ${user.lastName}`;

  const changeActive = (e: any) => {
    const value = e.target.value;
    setIsActive(value);

    if (value !== user.isActive) {
      dispatch(ChangeIsActive(user._id, value));
    }
  };

  const changeJobsCanBuy = () => {
    if (jobsCanBuy >= 0 && jobsCanBuy !== user.jobsCanBuy) {
      setShowModal(false);
      dispatch(ChangeUserJobs(user._id, jobsCanBuy));
    }
  };

  const onCancel = () => {
    setShowModal(false);
    setJobsCanBuy(user.jobsCanBuy);
  };

  return (
    <motion.div exit={{ opacity: 0 }}>
      <PageTitle
        heading={userName}
        subheading={
          user.userType === UserTypeEnum.SP_COM
            ? "Serivce Provider Company"
            : user.userType === UserTypeEnum.SP_IND
            ? "Service Provider Individual"
            : user.userType === UserTypeEnum.SR_IND
            ? "Service Receiver Individual"
            : user.userType === UserTypeEnum.SR_COM
            ? "Service Receiver Company"
            : ""
        }
        icon={"pe-7s-user"}
        buttons={
          <div className="space-x-4">
            <Button onClick={() => history.goBack()}>
              <FontAwesomeIcon icon={faArrowLeft} size="1x" />
            </Button>
            {!isSr && (
              <Radio.Group
                value={isActive}
                buttonStyle="solid"
                onChange={changeActive}
              >
                <Radio.Button value={true}>Active</Radio.Button>
                <Radio.Button value={false}>Blocked</Radio.Button>
              </Radio.Group>
            )}
          </div>
        }
      />

      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <div className="p-8 max-w-6xl m-auto">
          <Card>
            {user.userType === UserTypeEnum.SP_COM && (
              <SPComUserDetails
                user={user}
                showModal={() => setShowModal(true)}
                isSr={isSr}
              />
            )}

            {user.userType === UserTypeEnum.SP_IND && (
              <SPIndUserDetails
                user={user}
                showModal={() => setShowModal(true)}
                isSr={isSr}
              />
            )}

            {user.userType === UserTypeEnum.SR_IND && (
              <SRIndUserDetails user={user} />
            )}

            {user.userType === UserTypeEnum.SR_COM && (
              <SRComUserDetails user={user} />
            )}

            {user.userType === UserTypeEnum.ADMIN && (
              <SRIndUserDetails user={user} />
            )}
          </Card>
        </div>
      </motion.div>
      <Modal
        title="No. of Jobs User Can buy"
        visible={showModal}
        onCancel={onCancel}
        onOk={changeJobsCanBuy}
      >
        <div className="flex flex-col w-full">
          <InputField
            id="experience"
            name="experience"
            type="number"
            value={jobsCanBuy}
            onChange={(value) => setJobsCanBuy(value)}
          />
        </div>
      </Modal>
    </motion.div>
  );
}
