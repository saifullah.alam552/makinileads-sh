import { Button, Divider, Typography } from "antd";
import { motion } from "framer-motion";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { NavLink, useHistory } from "react-router-dom";
import { Card } from "../components/Card";
import HomeLayout from "../components/HomeLayout";
import { ForgetPasswordForm } from "../components/SubForms";
import { useForm } from "../hooks/useForm";
import { ForgetPassword } from "../store/Effects";
import { ForgetPasswdInput } from "../types";
import { forgetInitErrors, forgetInitValues } from "../utils/formInits";
import { forgetPasswdValidate } from "../utils/validations";

export default function ForgetPasswordPage() {
  const [formValues, setFormValues] = useState<ForgetPasswdInput>(
    forgetInitValues
  );

  const history = useHistory();
  const dispatch = useDispatch();

  const form = useForm(
    forgetInitValues,
    forgetInitErrors,
    forgetPasswdValidate,
    setFormValues,
    formValues
  );

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    const formValid = await form.formValidation();
    if (!formValid) return;
    dispatch(ForgetPassword(formValues, history));
  };

  return (
    <HomeLayout>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1.5 }}
      >
        <Card>
          <form
            onSubmit={handleSubmit}
            onReset={form.handleReset}
            autoComplete="off"
          >
            <Typography.Title level={4}>Forget Password</Typography.Title>
            <ForgetPasswordForm
              values={formValues}
              errors={form.formErrors}
              handleBlur={form.handleBlur}
              handleChange={form.handleChange}
            />

            <div className="pt-4">
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
              <Divider type={"vertical"} />
              <Button type="default" htmlType="reset">
                Cancel
              </Button>

              <div className="mt-4 text-center">
                <NavLink to="/">Go Back!</NavLink>
              </div>
            </div>
          </form>
        </Card>
      </motion.div>
    </HomeLayout>
  );
}
