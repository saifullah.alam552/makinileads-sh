import { Button, Divider, Typography } from "antd";
import { motion } from "framer-motion";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { NavLink, useHistory } from "react-router-dom";
import { Card } from "../components/Card";
import HomeLayout from "../components/HomeLayout";
import { LoginForm } from "../components/SubForms";
import { useForm } from "../hooks/useForm";
import { Login } from "../store/Effects";
import { LoginFormInput } from "../types";
import { loginInitErrors, loginInitValues } from "../utils/formInits";
import { loginFormValidate } from "../utils/validations";

export default function Home() {
  const [formValues, setFormValues] = useState<LoginFormInput>(loginInitValues);
  const dispatch = useDispatch();
  const history = useHistory();

  const form = useForm(
    loginInitValues,
    loginInitErrors,
    loginFormValidate,
    setFormValues,
    formValues
  );

  const handleSubmit = async (event: any) => {
    event.preventDefault();
    const formValid = await form.formValidation();
    if (!formValid) return;
    dispatch(Login(formValues, history));
  };

  return (
    <HomeLayout>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1.5 }}
      >
        <Card>
          <Typography.Title level={4}>Login</Typography.Title>
          <form
            onSubmit={handleSubmit}
            onReset={form.handleReset}
            autoComplete="off"
          >
            <LoginForm
              values={formValues}
              errors={form.formErrors}
              handleChange={form.handleChange}
              handleBlur={form.handleBlur}
            />

            <div className="pt-4">
              <Button className="logo-purple-color" type="primary" htmlType="submit">
                Login
              </Button>
              <Divider type={"vertical"} />
              <Button type="default" htmlType="reset">
                Cancel
              </Button>

              <div className="mt-4 text-center">
                <NavLink to="/forget-password">Forget Password?</NavLink>
              </div>
            </div>
          </form>
        </Card>
      </motion.div>
    </HomeLayout>
  );
}
