import { Button, Result } from "antd";
import { motion } from "framer-motion";
import React from "react";
import { useHistory } from "react-router-dom";

export default function Page404() {
  const history = useHistory();
  return (
    <motion.div exit={{ opacity: 0 }}>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <Result
          status="404"
          title="404"
          subTitle="Sorry, the page you visited does not exist."
          extra={
            <Button type="primary" onClick={() => history.goBack()}>
              Go Back!
            </Button>
          }
        />
      </motion.div>
    </motion.div>
  );
}
