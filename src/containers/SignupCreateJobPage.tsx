import { Radio, Typography } from "antd";
import { motion } from "framer-motion";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { Card } from "../components/Card";
import {
  AddressStep,
  DetailsStep,
  DocumentsStep,
  InfoEduStep,
  JobDetailStep,
  JobLocationStep,
  LoginOrNextStep,
  PasswordStep,
  ServiceStep,
  SPTypeStep,
  SRTypeStep,
} from "../components/StepForms";
import { JobPostLogin, JobPostNewAcct, Register } from "../store/Effects";
import {
  JobFormModel,
  LoginFormInput,
  UserSignupFormModel,
  UserTypeEnum,
} from "../types";
import {
  jobInitErrors,
  jobInitValues,
  loginInitErrors,
  loginInitValues,
  userInitErrors,
  userInitValues,
} from "../utils/formInits";

interface Props {
  formType: "SP" | "SR" | "JOB";
}

export default function SignupCreateJob(props: Props) {
  const [current, setCurrent] = useState<number>(0);
  const dispatch = useDispatch();
  const history = useHistory();

  const [formValues, setFormValues] = useState<UserSignupFormModel>(
    userInitValues
  );
  const [jobValues, setJobValues] = useState<JobFormModel>(jobInitValues);
  const [loginFormValues, setLoginFormValues] = useState<LoginFormInput>(
    loginInitValues
  );

  const [useJobAdd, setUseJobAdd] = useState<boolean>(false);

  useEffect(() => {
    setFormValues((prev) => {
      return {
        ...prev,
        userType:
          props.formType === "SP"
            ? UserTypeEnum.SP_IND
            : props.formType === "SR"
            ? UserTypeEnum.SR_IND
            : props.formType === "JOB"
            ? UserTypeEnum.SR_IND
            : UserTypeEnum.SP_IND,
      };
    });
  }, [props.formType]);

  const onSubmit = (files: any[]) => {
    if (props.formType === "SP")
      dispatch(Register(formValues, history, props.formType, files));
    if (props.formType === "SR")
      dispatch(Register(formValues, history, props.formType, files));
    if (props.formType === "JOB") {
      if (loginFormValues.username !== "") {
        dispatch(JobPostLogin(loginFormValues, jobValues, history));
      } else {
        dispatch(JobPostNewAcct(formValues, jobValues, history));
      }
    }
  };

  const onChangeUseJobAdd = (e: any) => {
    const value = e.target.value;
    setUseJobAdd(value);
    if (value) {
      setFormValues({
        ...formValues,
        district: jobValues.district,
        region: jobValues.region,
        ward: jobValues.ward,
        streetAddress: jobValues.streetAddress,
      });
    } else {
      setFormValues({
        ...formValues,
        district: "",
        region: "",
        ward: "",
        streetAddress: "",
      });
    }
  };

  const spSteps = [
    {
      title: "Type",
      content: () => (
        <SPTypeStep
          current={current}
          length={spSteps.length}
          setCurrent={setCurrent}
          initValues={userInitValues}
          initErrors={userInitErrors}
          values={formValues}
          setValues={setFormValues}
        />
      ),
    },
    {
      title:
        formValues.userType === UserTypeEnum.SP_IND
          ? "Personal Details"
          : "Company Details",
      content: () => (
        <DetailsStep
          current={current}
          length={spSteps.length}
          setCurrent={setCurrent}
          initValues={userInitValues}
          initErrors={userInitErrors}
          values={formValues}
          setValues={setFormValues}
        />
      ),
    },
    {
      title:
        formValues.userType === UserTypeEnum.SP_IND
          ? "Personal Address"
          : "Company Address",
      content: () => (
        <AddressStep
          current={current}
          length={spSteps.length}
          setCurrent={setCurrent}
          initValues={userInitValues}
          initErrors={userInitErrors}
          values={formValues}
          setValues={setFormValues}
        />
      ),
    },
    {
      title: "Services Offered",
      content: () => (
        <ServiceStep
          current={current}
          length={spSteps.length}
          setCurrent={setCurrent}
          initValues={userInitValues}
          initErrors={userInitErrors}
          values={formValues}
          setValues={setFormValues}
        />
      ),
    },
    {
      title:
        formValues.userType === UserTypeEnum.SP_IND
          ? "Education"
          : "Company Information",
      content: () => (
        <InfoEduStep
          current={current}
          length={spSteps.length}
          setCurrent={setCurrent}
          initValues={userInitValues}
          initErrors={userInitErrors}
          values={formValues}
          setValues={setFormValues}
        />
      ),
    },
    {
      title: "Password",
      content: () => (
        <PasswordStep
          current={current}
          length={spSteps.length}
          setCurrent={setCurrent}
          initValues={userInitValues}
          initErrors={userInitErrors}
          values={formValues}
          setValues={setFormValues}
        />
      ),
    },
    {
      title: "Documents Upload",
      content: () => (
        <DocumentsStep
          current={current}
          length={spSteps.length}
          setCurrent={setCurrent}
          initValues={userInitValues}
          initErrors={userInitErrors}
          values={formValues}
          setValues={setFormValues}
          onFinish={onSubmit}
        />
      ),
    },
  ];

  const srSteps = [
    {
      title: "Type",
      content: () => (
        <SRTypeStep
          current={current}
          length={srSteps.length}
          setCurrent={setCurrent}
          initValues={userInitValues}
          initErrors={userInitErrors}
          values={formValues}
          setValues={setFormValues}
        />
      ),
    },
    {
      title:
        formValues.userType === UserTypeEnum.SR_IND
          ? "Personal Details"
          : "Company Details",
      content: () => (
        <DetailsStep
          current={current}
          length={srSteps.length}
          setCurrent={setCurrent}
          initValues={userInitValues}
          initErrors={userInitErrors}
          values={formValues}
          setValues={setFormValues}
        />
      ),
    },
    {
      title:
        formValues.userType === UserTypeEnum.SR_IND
          ? "Personal Address"
          : "Company Address",
      content: () => (
        <AddressStep
          current={current}
          length={srSteps.length}
          setCurrent={setCurrent}
          initValues={userInitValues}
          initErrors={userInitErrors}
          values={formValues}
          setValues={setFormValues}
        />
      ),
    },
    {
      title: "Password",
      content: () => (
        <PasswordStep
          current={current}
          length={srSteps.length}
          setCurrent={setCurrent}
          initValues={userInitValues}
          initErrors={userInitErrors}
          values={formValues}
          setValues={setFormValues}
          onFinish={onSubmit}
        />
      ),
    },
  ];

  const jobSteps = [
    {
      title: "What services are you looking for?",
      content: () => (
        <ServiceStep
          current={current}
          length={jobSteps.length}
          setCurrent={setCurrent}
          initValues={jobInitValues}
          initErrors={jobInitErrors}
          values={jobValues}
          setValues={setJobValues}
        />
      ),
    },
    {
      title: "Enter Job Details",
      content: () => (
        <JobDetailStep
          current={current}
          length={jobSteps.length}
          setCurrent={setCurrent}
          initValues={jobInitValues}
          initErrors={jobInitErrors}
          values={jobValues}
          setValues={setJobValues}
        />
      ),
    },
    {
      title: "Job Location Details",
      content: () => (
        <JobLocationStep
          current={current}
          length={jobSteps.length}
          setCurrent={setCurrent}
          initValues={jobInitValues}
          initErrors={jobInitErrors}
          values={jobValues}
          setValues={setJobValues}
        />
      ),
    },
    {
      title: "Login / Create new Account",
      content: () => (
        <LoginOrNextStep
          current={current}
          length={jobSteps.length}
          setCurrent={setCurrent}
          initValues={loginInitValues}
          initErrors={loginInitErrors}
          values={loginFormValues}
          setValues={setLoginFormValues}
          onFinish={onSubmit}
        />
      ),
    },
    {
      title: "Type",
      content: () => (
        <SRTypeStep
          current={current}
          length={jobSteps.length}
          setCurrent={setCurrent}
          initValues={userInitValues}
          initErrors={userInitErrors}
          values={formValues}
          setValues={setFormValues}
        />
      ),
    },
    {
      title:
        formValues.userType === UserTypeEnum.SR_IND
          ? "Personal Details"
          : "Company Details",
      content: () => (
        <DetailsStep
          current={current}
          length={jobSteps.length}
          setCurrent={setCurrent}
          initValues={userInitValues}
          initErrors={userInitErrors}
          values={formValues}
          setValues={setFormValues}
        />
      ),
    },
    {
      title:
        formValues.userType === UserTypeEnum.SR_IND
          ? "Personal Address"
          : "Company Address",
      content: () => (
        <>
          <div className="mb-2">
            <span className="mr-4">Same address as Job address:</span>
            <Radio.Group
              onChange={onChangeUseJobAdd}
              value={useJobAdd}
              name="useJobAdd"
            >
              <Radio value={true}>Yes</Radio>
              <Radio value={false}>No</Radio>
            </Radio.Group>
          </div>
          <AddressStep
            current={current}
            length={jobSteps.length}
            setCurrent={setCurrent}
            initValues={userInitValues}
            initErrors={userInitErrors}
            values={formValues}
            setValues={setFormValues}
          />
        </>
      ),
    },
    {
      title: "Password",
      content: () => (
        <PasswordStep
          current={current}
          length={jobSteps.length}
          setCurrent={setCurrent}
          initValues={userInitValues}
          initErrors={userInitErrors}
          values={formValues}
          setValues={setFormValues}
          onFinish={onSubmit}
        />
      ),
    },
  ];

  return (
    <motion.div
      className="grid grid-cols-1 justify-center items-center justify-items-center max-w-6xl mx-auto p-4"
      exit={{ opacity: 0 }}
    >
      <motion.div
        className="w-full"
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <Card>
          <div className="flex justify-between items-center overflow-hidden flex-wrap ">
            <motion.div
              initial={{ opacity: 0, x: -100 }}
              animate={{ opacity: 1, x: 0 }}
              transition={{ delay: 0.25, duration: 1 }}
            >
              <Typography.Title level={5}>
                {props.formType === "SP"
                  ? spSteps[current].title
                  : props.formType === "SR"
                  ? srSteps[current].title
                  : props.formType === "JOB"
                  ? jobSteps[current].title
                  : "Incorrect formType"}
              </Typography.Title>
              {/* <Typography.Title level={4}>
                {props.formType === "SP"
                  ? "Register as Professional"
                  : props.formType === "SR"
                  ? "Create an account"
                  : props.formType === "JOB"
                  ? "Create a Job"
                  : "Incorrect formType"}
              </Typography.Title> */}
            </motion.div>
            <motion.div
              initial={{ opacity: 0, x: 100 }}
              animate={{ opacity: 1, x: 0 }}
              transition={{ delay: 0.25, duration: 1 }}
            >
              {/* <Typography.Title level={5}>
                {props.formType === "SP"
                  ? spSteps[current].title
                  : props.formType === "SR"
                  ? srSteps[current].title
                  : props.formType === "JOB"
                  ? jobSteps[current].title
                  : "Incorrect formType"}
              </Typography.Title> */}
            </motion.div>
          </div>
          <div className="container mt-6">
            {props.formType === "SP"
              ? spSteps[current].content()
              : props.formType === "SR"
              ? srSteps[current].content()
              : props.formType === "JOB"
              ? jobSteps[current].content()
              : "Incorrect formType"}
          </div>
        </Card>
      </motion.div>
    </motion.div>
  );
}
