import { Button, Result } from "antd";
import { motion } from "framer-motion";
import React from "react";
import { NavLink } from "react-router-dom";

export default function JobSuccessPageLogin() {
  return (
    <motion.div exit={{ opacity: 0 }}>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <Result
          status="success"
          title="Your job is created."
          subTitle="Login to your account to view."
          extra={
            <NavLink to="/">
              <Button type="primary">Go to Login Page</Button>
            </NavLink>
          }
        />
      </motion.div>
    </motion.div>
  );
}
