import { Button, Result } from "antd";
import { motion } from "framer-motion";
import React from "react";
import { NavLink } from "react-router-dom";

export default function SRSuccessPage() {
  return (
    <motion.div exit={{ opacity: 0 }}>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 1 }}
      >
        <Result
          status="success"
          title="Please check your email for confirmation."
          subTitle="Your account is created as Service Receiver."
          extra={
            <NavLink to="/">
              <Button className="logo-purple-color" type="primary">Go to Login Page</Button>
            </NavLink>
          }
        />
      </motion.div>
    </motion.div>
  );
}
