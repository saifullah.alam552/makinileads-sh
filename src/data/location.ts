export const location = [
  {
    value: "Arusha",
    label: "Arusha",
    children: [
      {
        value: "Meru Distt.",
        label: "Meru Distt.",
        children: [
          {
            value: "Akheri",
            label: "Akheri",
          },
          {
            value: "Bangata",
            label: "Bangata",
          },
          {
            value: "Bwawani",
            label: "Bwawani",
          },
          {
            value: "Ilkiding'a",
            label: "Ilkiding'a",
          },
          {
            value: "Kikatiti",
            label: "Kikatiti",
          },
          {
            value: "Kikwe",
            label: "Kikwe",
          },
          {
            value: "Kimnyaki",
            label: "Kimnyaki",
          },
          {
            value: "King'ori",
            label: "King'ori",
          },
          {
            value: "Kiranyi",
            label: "Kiranyi",
          },
          {
            value: "Kisongo",
            label: "Kisongo",
          },
          {
            value: "Leguruki",
            label: "Leguruki",
          },
          {
            value: "Makiba",
            label: "Makiba",
          },
          {
            value: "Maji ya Chai",
            label: "Maji ya Chai",
          },
          {
            value: "Maroroni",
            label: "Maroroni",
          },
          {
            value: "Mateves",
            label: "Mateves",
          },
          {
            value: "Mbuguni",
            label: "Mbuguni",
          },
          {
            value: "Mlangarini",
            label: "Mlangarini",
          },
          {
            value: "Moivo",
            label: "Moivo",
          },
          {
            value: "Murieti",
            label: "Murieti",
          },
          {
            value: "Mussa",
            label: "Mussa",
          },
          {
            value: "Mwandeti",
            label: "Mwandeti",
          },
          {
            value: "Nduruma",
            label: "Nduruma",
          },
          {
            value: "Ngarenanyuki",
            label: "Ngarenanyuki",
          },
          {
            value: "Nkoanrua",
            label: "Nkoanrua",
          },
          {
            value: "Nkoaranga",
            label: "Nkoaranga",
          },
          {
            value: "Nkoarisambu",
            label: "Nkoarisambu",
          },
          {
            value: "Oldonyosambu",
            label: "Oldonyosambu",
          },
          {
            value: "Oljoro",
            label: "Oljoro",
          },
          {
            value: "Olkokola",
            label: "Olkokola",
          },
          {
            value: "Oltroto",
            label: "Oltroto",
          },
          {
            value: "Oltrumet",
            label: "Oltrumet",
          },
          {
            value: "Poli",
            label: "Poli",
          },
          {
            value: "Singisi",
            label: "Singisi",
          },
          {
            value: "Sokoni II",
            label: "Sokoni II",
          },
          {
            value: "Songoro",
            label: "Songoro",
          },
          {
            value: "Usa River",
            label: "Usa River",
          },
          {
            value: "Baraa",
            label: "Baraa",
          },
          {
            value: "Daraja Mbili",
            label: "Daraja Mbili",
          },
          {
            value: "Elerai",
            label: "Elerai",
          },
          {
            value: "Engosheraton",
            label: "Engosheraton",
          },
          {
            value: "Engutoto",
            label: "Engutoto",
          },
          {
            value: "Kaloleni",
            label: "Kaloleni",
          },
          {
            value: "Kati",
            label: "Kati",
          },
          {
            value: "Kimandolu",
            label: "Kimandolu",
          },
          {
            value: "Lemara",
            label: "Lemara",
          },
          {
            value: "Levolosi",
            label: "Levolosi",
          },
          {
            value: "Moshono",
            label: "Moshono",
          },
          {
            value: "Ngarenaro",
            label: "Ngarenaro",
          },
          {
            value: "Oloirien",
            label: "Oloirien",
          },
          {
            value: "Sekei",
            label: "Sekei",
          },
          {
            value: "Sokon I",
            label: "Sokon I",
          },
          {
            value: "Sombetini",
            label: "Sombetini",
          },
          {
            value: "Terrat",
            label: "Terrat",
          },
          {
            value: "Themi",
            label: "Themi",
          },
          {
            value: "Unga L.T.D.",
            label: "Unga L.T.D.",
          },
        ],
      },
      {
        value: "Arusha Distt.",
        label: "Arushad Distt.",
        children: [
          {
            value: "Bangata",
            label: "Bangata",
          },
          {
            value: "Bwawani",
            label: "Bwawani",
          },
          {
            value: "Ilboru",
            label: "Ilboru",
          },
          {
            value: "Ilkiding'a",
            label: "Ilkiding'a",
          },
          {
            value: "Kiranyi",
            label: "Kiranyi",
          },
          {
            value: "Kisongo",
            label: "Kisongo",
          },
          {
            value: "Kiutu",
            label: "Kiutu",
          },
          {
            value: "Laroi",
            label: "Laroi",
          },
          {
            value: "Lemanyata",
            label: "Lemanyata",
          },
          {
            value: "Mateves",
            label: "Mateves",
          },
          {
            value: "Mlangarini",
            label: "Mlangarini",
          },
          {
            value: "Moivo",
            label: "Moivo",
          },
          {
            value: "Musa",
            label: "Musa",
          },
          {
            value: "Mwandeti",
            label: "Mwandeti",
          },
          {
            value: "Nduruma",
            label: "Nduruma",
          },
          {
            value: "Oldadai",
            label: "Oldadai",
          },
          {
            value: "Oldonyomaasi",
            label: "Oldonyomaasi",
          },
          {
            value: "Oldonyosambu",
            label: "Oldonyosambu",
          },
          {
            value: "Oljoro",
            label: "Oljoro",
          },
          {
            value: "Olmotonyi",
            label: "Olmotonyi",
          },
          {
            value: "Olorieni",
            label: "Olorieni",
          },
          {
            value: "Oltoroto",
            label: "Oltoroto",
          },
          {
            value: "Oltrumet",
            label: "Oltrumet",
          },
          {
            value: "Sambasha",
            label: "Sambasha",
          },
          {
            value: "Sokon II",
            label: "Sokon II",
          },
          {
            value: "Tarakwa",
            label: "Tarakwa",
          },
          {
            value: "Timbolo",
            label: "Timbolo",
          },
        ],
      },
      {
        value: "Karatu Distt.",
        label: "Karatu Distt.",
        children: [
          {
            value: "Baray",
            label: "Baray",
          },
          {
            value: "Buger",
            label: "Buger",
          },
          {
            value: "Daa",
            label: "Daa",
          },
          {
            value: "Endabash",
            label: "Endabash",
          },
          {
            value: "Endamarariek",
            label: "Endamarariek",
          },
          {
            value: "Kansay",
            label: "Kansay",
          },
          {
            value: "Karatu",
            label: "Karatu",
          },
          {
            value: "Mang'ola",
            label: "Mang'ola",
          },
          {
            value: "Mbulumbulu",
            label: "Mbulumbulu",
          },
          {
            value: "Oldeani",
            label: "Oldeani",
          },
          {
            value: "Qurus",
            label: "Qurus",
          },
          {
            value: "Rhotia",
            label: "Rhotia",
          },
        ],
      },
      {
        value: "Longido Distt.",
        label: "Longido Distt.",
        children: [
          {
            value: "Elang'atadapash",
            label: "Elang'atadapash",
          },
          {
            value: "Engarenaibo",
            label: "Engarenaibo",
          },
          {
            value: "Engikaret",
            label: "Engikaret",
          },
          {
            value: "Gelai Lumbwa",
            label: "Gelai Lumbwa",
          },
          {
            value: "Gelai Meirugoi",
            label: "Gelai Meirugoi",
          },
          {
            value: "Iloirienito",
            label: "Iloirienito",
          },
          {
            value: "Kamwanga",
            label: "Kamwanga",
          },
          {
            value: "Ketumbeine",
            label: "Ketumbeine",
          },
          {
            value: "Kimokouwa",
            label: "Kimokouwa",
          },
          {
            value: "Longido",
            label: "Longido",
          },
          {
            value: "Matale",
            label: "Matale",
          },
          {
            value: "Mundarara",
            label: "Mundarara",
          },
          {
            value: "Namanga",
            label: "Namanga",
          },
          {
            value: "Noondoto",
            label: "Noondoto",
          },
          {
            value: "Ol?molog",
            label: "Ol?molog",
          },
          {
            value: "Orbomba",
            label: "Orbomba",
          },
          {
            value: "Sinya",
            label: "Sinya",
          },
          {
            value: "Tingatinga",
            label: "Tingatinga",
          },
        ],
      },
      {
        value: "Monduli Distt.",
        label: "Monduli Distt.",
        children: [
          {
            value: "Engarenaibor",
            label: "Engarenaibor",
          },
          {
            value: "Engaruka",
            label: "Engaruka",
          },
          {
            value: "Engutoto",
            label: "Engutoto",
          },
          {
            value: "Esilalei",
            label: "Esilalei",
          },
          {
            value: "Gelai Lumbwa",
            label: "Gelai Lumbwa",
          },
          {
            value: "Gelai Meirugoi",
            label: "Gelai Meirugoi",
          },
          {
            value: "Kitumbeine",
            label: "Kitumbeine",
          },
          {
            value: "Lolkisale",
            label: "Lolkisale",
          },
          {
            value: "Longido",
            label: "Longido",
          },
          {
            value: "Makuyuni",
            label: "Makuyuni",
          },
          {
            value: "Matale",
            label: "Matale",
          },
          {
            value: "Moita",
            label: "Moita",
          },
          {
            value: "Monduli Juu",
            label: "Monduli Juu",
          },
          {
            value: "Monduli Mjini",
            label: "Monduli Mjini",
          },
          {
            value: "Mto wa Mbu[2]",
            label: "Mto wa Mbu[2]",
          },
          {
            value: "Ol-molog",
            label: "Ol-molog",
          },
          {
            value: "Selela",
            label: "Selela",
          },
          {
            value: "Sepeko",
            label: "Sepeko",
          },
          {
            value: "Tingatinga",
            label: "Tingatinga",
          },
        ],
      },
      {
        value: "Ngorongoro Distt.",
        label: "Ngorongoro Distt.",
        children: [
          {
            value: "Arash",
            label: "Arash",
          },
          {
            value: "Digodigo",
            label: "Digodigo",
          },
          {
            value: "Enduleni",
            label: "Enduleni",
          },
          {
            value: "Kakesio",
            label: "Kakesio",
          },
          {
            value: "Malambo",
            label: "Malambo",
          },
          {
            value: "Nainokanoka",
            label: "Nainokanoka",
          },
          {
            value: "Olbalbal",
            label: "Olbalbal",
          },
          {
            value: "Oldonyo-Sambu",
            label: "Oldonyo-Sambu",
          },
          {
            value: "Ngorongoro",
            label: "Ngorongoro",
          },
          {
            value: "Pinyinyi",
            label: "Pinyinyi",
          },
          {
            value: "Sale",
            label: "Sale",
          },
          {
            value: "Soit Sambu",
            label: "Soit Sambu",
          },
          {
            value: "Ololosokwan",
            label: "Ololosokwan",
          },
          {
            value: "Oloipiri",
            label: "Oloipiri",
          },
          {
            value: "Enguserosambu",
            label: "Enguserosambu",
          },
          {
            value: "Orgosorok",
            label: "Orgosorok",
          },
          {
            value: "Nayobi",
            label: "Nayobi",
          },
        ],
      },
    ],
  },
];
