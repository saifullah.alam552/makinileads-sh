import { useCallback, useEffect, useMemo, useState } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import {
  GetAllCategories,
  GetAllSubCategories,
  GetAllUsers,
  GetCategoryDetailById,
  GetJobsDataEffect,
  GetSubCategoryDetailById,
  GetUserDetailById,
  GetUserInfo,
} from "../store/Effects";
import { AppStateType, UserTypeEnum } from "../types";
import { JobQuery, UserQuery } from "../utils/apiUrls";
import {
  CategorySelect,
  SPUserSelect,
  SRUserSelect,
  SubCategorySelect,
  SubCategorySelectOnCategory,
} from "../utils/helpers";

export function useIsLogin() {
  const token = useSelector(
    (state: AppStateType) => state.mainStore.accessToken,
    shallowEqual
  );

  const isLogin = useMemo(() => (token.length !== 0 ? true : false), [token]);

  return isLogin;
}

export function useIsAdmin() {
  const userType = useSelector(
    (state: AppStateType) => state.mainStore.user.userType,
    shallowEqual
  );

  return userType === UserTypeEnum.ADMIN ? true : false;
}

export function useGetAppStatus() {
  const { error, loading, responseText } = useSelector(
    (state: AppStateType) => state.mainStore.appStatus,
    shallowEqual
  );

  return { error, loading, responseText };
}

export function useGetUserInfo() {
  const user = useSelector(
    (state: AppStateType) => state.mainStore.user,
    shallowEqual
  );

  const dispatch = useDispatch();

  useEffect(() => {
    if (user._id === "") dispatch(GetUserInfo());
  }, [user._id, dispatch]);

  return user;
}

export function useGetAllUsers(rq: UserQuery) {
  const allUsers = useSelector(
    (state: AppStateType) => state.mainStore.allUsers,
    shallowEqual
  );

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(GetAllUsers(rq));
  }, [allUsers.length, dispatch, rq]);

  return allUsers;
}

export function useGetSelectUsers() {
  const [rq] = useState<UserQuery>({});
  const users = useGetAllUsers(rq);

  const spSelect = useMemo(() => SPUserSelect(users), [users]);
  const srSelect = useMemo(() => SRUserSelect(users), [users]);

  return { spSelect, srSelect };
}

export function useGetUserDetailById(id: string) {
  const userDetail = useSelector(
    (state: AppStateType) => state.mainStore.userDetail,
    shallowEqual
  );

  const dispatch = useDispatch();

  useEffect(() => {
    if (id && id !== "") dispatch(GetUserDetailById(id));
  }, [dispatch, id, userDetail._id]);

  return userDetail;
}

export function useGetCategoryDetailById(id: string) {
  const category = useSelector(
    (state: AppStateType) => state.mainStore.category,
    shallowEqual
  );

  const dispatch = useDispatch();

  useEffect(() => {
    if (id && id !== "") dispatch(GetCategoryDetailById(id));
  }, [dispatch, id]);

  return category;
}

export function useGetSubCategoryDetailById(id: string) {
  const subCategory = useSelector(
    (state: AppStateType) => state.mainStore.subCategory,
    shallowEqual
  );

  const dispatch = useDispatch();

  useEffect(() => {
    if (id && id !== "") dispatch(GetSubCategoryDetailById(id));
  }, [dispatch, id]);

  return subCategory;
}

export function useGetAllCategories() {
  const categories = useSelector(
    (state: AppStateType) => state.mainStore.categories,
    shallowEqual
  );

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(GetAllCategories());
  }, [categories.length, dispatch]);

  return categories;
}

export function useGetAllSubCategories() {
  const subCategories = useSelector(
    (state: AppStateType) => state.mainStore.subCategories,
    shallowEqual
  );

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(GetAllSubCategories());
  }, [dispatch, subCategories.length]);

  return subCategories;
}

export function useGetSelectCategories() {
  const categories = useGetAllCategories();

  const categorySelect = useMemo(() => CategorySelect(categories), [
    categories,
  ]);
  return categorySelect;
}

export function useGetSelectSubCategories() {
  const subCategories = useGetAllSubCategories();

  const subCategorySelect = useMemo(() => SubCategorySelect(subCategories), [
    subCategories,
  ]);

  return subCategorySelect;
}

export function useGetSelectSubCategoriesByCategory(catId: string) {
  const subCategories = useGetAllSubCategories();

  const subCategorySelectOnCategory = useMemo(
    () => SubCategorySelectOnCategory(subCategories, catId),
    [catId, subCategories]
  );
  return subCategorySelectOnCategory;
}

export interface GetServiceData {
  key: string;
  categoryName: string;
  subCategoryName: string;
}

export function useGetServiceData(skills: string[]): GetServiceData[] {
  const [data, setData] = useState<GetServiceData[]>([]);

  const subCategories = useGetAllSubCategories();

  useEffect(() => {
    if (skills.length > 0) {
      const test: any[] = skills.reduce((acc: any, cur: any) => {
        const index = subCategories.findIndex((d) => d._id === cur);
        if (index === -1) return [...acc];
        return [
          ...acc,
          {
            key: subCategories[index]._id,
            subCategoryName: subCategories[index].title,
            categoryName: subCategories[index].category.title,
          },
        ];
      }, []);

      setData(test);
    } else {
      setData([]);
    }
  }, [skills, skills.length, subCategories]);

  return data;
}

export interface IManageSkillsData {
  _id: string;
  title: string;
  isActive: boolean;
  subCategory: { _id: string; title: string; isActive: boolean }[];
}

export function useGetManageSkillsData() {
  const categories = useGetAllCategories();
  const subCategories = useGetAllSubCategories();

  const data: IManageSkillsData[] = useMemo(
    () =>
      categories.map((cat) => {
        return {
          _id: cat._id,
          title: cat.title,
          isActive: cat.isActive,
          subCategory: subCategories
            .filter((sub) => sub.category._id === cat._id)
            .map((s) => {
              return {
                _id: s._id,
                title: s.title,
                isActive: s.isActive,
              };
            }),
        };
      }),
    [categories, subCategories]
  );

  return data;
}

export function useGetJobs(rq: JobQuery) {
  const jobSummaries = useSelector(
    (state: AppStateType) => state.mainStore.jobSummaries,
    shallowEqual
  );

  const job = useSelector(
    (state: AppStateType) => state.mainStore.jobData,
    shallowEqual
  );

  const dispatch = useDispatch();

  useEffect(() => {
    if (rq.id !== "new") dispatch(GetJobsDataEffect(rq));
  }, [dispatch, rq]);

  return { jobSummaries, job };
}

export function useDebounce(value: string, delay: number) {
  const [debouncedValue, setDebouncedValue] = useState(value);

  const handler = useCallback(
    () =>
      setTimeout(() => {
        setDebouncedValue(value);
      }, delay),
    [delay, value]
  );

  useEffect(() => {
    handler();

    return () => {
      clearTimeout(handler());
    };
  }, [handler]);

  return debouncedValue;
}
