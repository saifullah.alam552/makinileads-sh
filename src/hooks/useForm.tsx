import dayjs, { Dayjs } from "dayjs";
import { SetStateAction, useState } from "react";
import * as yup from "yup";

export type FormErrors<T> = {
  [K in keyof T]: string | "";
};

export function useForm<T>(
  initValues: T,
  initErrors: FormErrors<T>,
  validateSchema: any,
  setFormValues: (value: SetStateAction<T>) => void,
  formValues: T
) {
  const [formErrors, setFormErrors] = useState<FormErrors<T>>(initErrors);

  const handleChange = (event: any) => {
    const name = event.target.name;

    const value =
      event.target.type === "checkbox"
        ? event.target.checked
        : event.target.value;
    setFormValues({
      ...formValues,
      [name]: value,
    });
    setFormErrors(initErrors);
  };

  const dateChange = (
    value: Dayjs | null,
    dateString: string,
    fieldName: string
  ) => {
    const date = dayjs(dateString).format("YYYY-MM-DD");
    setFormValues({
      ...formValues,
      [fieldName]: date,
    });
  };

  const selectChange = (value: any, fieldName: string) => {
    setFormValues({
      ...formValues,
      [fieldName]: value,
    });
  };

  const cascaderSelect = (value: string[], fieldName: string[]) => {
    const fieldValues = fieldName.reduce((acc: any, cur: any, index: any) => {
      return { ...acc, [cur]: value[index] };
    }, {});

    setFormValues({
      ...formValues,
      ...fieldValues,
    });
  };

  const numberChange = (value: any, fieldName: string) => {
    setFormValues({
      ...formValues,
      [fieldName]: isNaN(value) ? 0 : value,
    });
  };

  const handleBlur = (event: any) => {
    const name = event.target.name;
    const value = event.target.value;
    yup
      .reach(validateSchema, name, "", "")
      .validate(value)
      .catch((err: any) => {
        setFormErrors({
          ...formErrors,
          [name]: err.message,
        });
      });
  };

  const formValidation = async (): Promise<boolean> => {
    try {
      await yup
        .reach(validateSchema, "", "", "")
        .validate(formValues, { abortEarly: false });
      return true;
    } catch (err) {
      let errors = err.inner.reduce((acc: any, current: any) => {
        if (acc[current.path]) return { ...acc };
        return { ...acc, [current.path]: current.message };
      }, {});
      setFormErrors({ ...formErrors, ...errors });
      return false;
    }
  };


  const handleReset = () => setFormValues(initValues);

  return {
    handleChange,
    handleBlur,
    handleReset,
    formValidation,
    formErrors,
    selectChange,
    numberChange,
    cascaderSelect,
    dateChange,
  };
}
