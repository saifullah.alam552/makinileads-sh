import { Modal } from "antd";
import { useCallback, useEffect } from "react";
import { useDispatch } from "react-redux";
import { appStatusAction } from "../store/Actions";

export function useModalHook(
  showMessage: boolean,
  message: string,
  type: "info" | "success" | "warning" | "error",
  okayHandle?: () => void
) {
  const dispatch = useDispatch();

  const Okay = useCallback(() => {
    dispatch(
      appStatusAction({ error: false, loading: false, responseText: "" })
    );
  }, [dispatch]);

  useEffect(() => {
    const propsData = {
      title: type.toString().toUpperCase(),
      content: message,
      maskClosable: false,
      onOk: okayHandle,
    };
    if (showMessage) {
      if (message !== "") {
        switch (type) {
          case "info":
            Modal.info({
              ...propsData,
            });
            break;
          case "warning":
            Modal.warning({
              ...propsData,
            });
            break;
          case "error":
            Modal.error({
              ...propsData,
              onOk: () => Okay(),
            });
            break;
          case "success":
            Modal.success({
              ...propsData,
              onOk: () => Okay(),
            });
            break;
          default:
            return;
        }
      }
    }
  }, [showMessage, message, type, okayHandle, Okay]);
}
