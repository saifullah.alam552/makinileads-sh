import {
  ActionsEnum,
  ActionsType,
  IAppStatus,
  ICategoryData,
  IJobData,
  IJobSummaryData,
  ISubCategoryData,
  IUserData,
} from "../types";

export function appStatusAction(status: IAppStatus): ActionsType {
  return {
    type: ActionsEnum.APP_STATUS,
    payload: status,
  };
}

export function authStatusAction(token: string): ActionsType {
  return {
    type: ActionsEnum.ACCESS_TOKEN,
    payload: token,
  };
}

export function addUserDataAction(user: IUserData): ActionsType {
  return {
    type: ActionsEnum.USER_INFO,
    payload: user,
  };
}

export function getUserDetailAction(user: IUserData): ActionsType {
  return {
    type: ActionsEnum.USER_DETAIL,
    payload: user,
  };
}

export function userLogoutAction(): ActionsType {
  localStorage.removeItem("expiresIn");
  localStorage.removeItem("token");
  window.location.assign("/");
  return {
    type: ActionsEnum.LOGOUT,
  };
}

export function getCategoriesAction(data: ICategoryData[]): ActionsType {
  return {
    type: ActionsEnum.GET_CATEGORIES,
    payload: data,
  };
}

export function getSubCategoriesAction(data: ISubCategoryData[]): ActionsType {
  return {
    type: ActionsEnum.GET_SUB_CATEGORIES,
    payload: data,
  };
}

export function getAllUsersAction(data: IUserData[]): ActionsType {
  return {
    type: ActionsEnum.GET_ALL_USERS,
    payload: data,
  };
}

export function getCategoryAction(data: ICategoryData): ActionsType {
  return {
    type: ActionsEnum.GET_CATEGORY,
    payload: data,
  };
}

export function getSubCategoryAction(data: ISubCategoryData): ActionsType {
  return {
    type: ActionsEnum.GET_SUB_CATEGORY,
    payload: data,
  };
}

export function getJobSummariesAction(
  data: IJobSummaryData[],
  total: number
): ActionsType {
  return {
    type: ActionsEnum.GET_JOB_SUMMARIES,
    payload: { data, total },
  };
}

export function getJobDataAction(data: IJobData): ActionsType {
  return {
    type: ActionsEnum.GET_JOB_DATA,
    payload: data,
  };
}
