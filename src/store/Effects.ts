import axios from "axios";
import { ThunkAction } from "redux-thunk";
import {
  ActionsType,
  AppStateType,
  CategoryFormModel,
  ChangePasswdInput,
  ForgetPasswdInput,
  JobFormModel,
  JobPurchaseForm,
  JobStatusChangeForm,
  JobStatusEnum,
  LoginFormInput,
  SubCategoryFormModel,
  UserSignupFormModel,
  UserTypeEnum,
} from "../types";
import apiUrls, { JobQuery, UserQuery } from "../utils/apiUrls";
import { axiosClient } from "../utils/axiosClient";
import { cloudinary_config } from "../utils/cloudinary-config";
import {
  addUserDataAction,
  appStatusAction,
  authStatusAction,
  getAllUsersAction,
  getCategoriesAction,
  getCategoryAction,
  getJobDataAction,
  getJobSummariesAction,
  getSubCategoriesAction,
  getSubCategoryAction,
  getUserDetailAction,
  userLogoutAction,
} from "./Actions";
import { status } from "./Reducer";

type Effect = ThunkAction<any, AppStateType, any, ActionsType>;

export function Login(data: LoginFormInput, history: any): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      const response = await axiosClient().post(apiUrls.login(), data);
      const expirationDate = new Date().getTime() + response.data.expiresIn;
      localStorage.setItem("token", response.data.accessToken);
      localStorage.setItem("expiresIn", expirationDate.toString());
      dispatch(addUserDataAction(response.data.result));
      dispatch(authStatusAction(response.data.accessToken));
      dispatch(appStatusAction(status));
      history.push("/dashboard");
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function GetUserInfo(): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      const response = await axiosClient().get(apiUrls.getUserInfo());
      dispatch(addUserDataAction(response.data.result));
      dispatch(appStatusAction(status));
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
      dispatch(userLogoutAction());
    }
  };
}

export function ChangePasswordEffect(data: ChangePasswdInput): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      await axiosClient().post(apiUrls.changePassword(), data);
      const response = await axiosClient().get(apiUrls.getUserInfo());
      dispatch(addUserDataAction(response.data.result));
      dispatch(
        appStatusAction({
          ...status,
          responseText: "Your password is changed.",
        })
      );
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function UpdateUserInfo(
  id: string,
  data: UserSignupFormModel,
  profilePic?: any,
  oldDocs?: any[],
  newDocs?: any[]
): Effect {
  return async function (dispatch) {
    try {
      if (oldDocs && data.documentsUrl.length !== oldDocs.length) {
        data.documentsUrl = oldDocs.map((d) => d.preview);
      }
      dispatch(appStatusAction({ ...status, loading: true }));

      if (profilePic && profilePic !== "") {
        const formData = new FormData();
        formData.append("file", profilePic);
        formData.append("upload_preset", cloudinary_config.upload_preset);
        formData.append("tags", `${data.email}, profile-picture`);
        const uploadProfile = await axios.post(
          apiUrls.cloudinaryUpload(),
          formData
        );
        data.profilePicUrl = uploadProfile.data.url;
      }

      if (newDocs && newDocs.length > 0) {
        const cloudinaryResponse = await Promise.all(
          newDocs.map((f) => {
            const formData = new FormData();
            formData.append("file", f);
            formData.append("upload_preset", cloudinary_config.upload_preset);
            formData.append("tags", `${data.email}, documents`);
            return axios.post(apiUrls.cloudinaryUpload(), formData);
          })
        );
        cloudinaryResponse.forEach((d) => {
          data.documentsUrl = [...data.documentsUrl, d.data.url];
        });
      }

      const response = await axiosClient().post(apiUrls.updateUser(id), data);
      dispatch(addUserDataAction(response.data.result));
      dispatch(
        appStatusAction({
          ...status,
          responseText: "Your record is updated.",
        })
      );
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function ForgetPassword(data: ForgetPasswdInput, history: any): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      await axiosClient().post(apiUrls.forgetPassword(), data);
      dispatch(appStatusAction(status));
      history.push("/forget-password-success");
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function Register(
  formData: UserSignupFormModel,
  history: any,
  formType: string,
  files: any[]
): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      let data = formData;
      await axiosClient().post(apiUrls.checkEmailPhone(), data);
      data.documentsUrl = [];
      // uploading images to cloudinary
      if (files.length > 0) {
        const cloudinaryResponse = await Promise.all(
          files.map((f) => {
            const formData = new FormData();
            formData.append("file", f);
            formData.append("upload_preset", cloudinary_config.upload_preset);
            formData.append("tags", `${data.email}, documents`);
            return axios.post(apiUrls.cloudinaryUpload(), formData);
          })
        );
        cloudinaryResponse.forEach((d) => {
          data.documentsUrl = [...data.documentsUrl, d.data.url];
        });
      }
      await axiosClient().post(apiUrls.register(), data);
      dispatch(appStatusAction(status));
      if (formType === "SR") history.push("/sr-success");
      if (formType === "SP") history.push("/sp-success");
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function JobPostLogin(
  loginData: LoginFormInput,
  jobData: JobFormModel,
  history: any
): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      const user = await axiosClient().post(apiUrls.login(), loginData);
      if (
        user.data.result.userType === UserTypeEnum.SP_IND ||
        user.data.result.userType === UserTypeEnum.SP_COM
      ) {
        dispatch(
          appStatusAction({
            ...status,
            error: true,
            responseText: "Please make Service Receiver account to post a job.",
          })
        );
        return;
      }
      let data: JobFormModel = {
        ...jobData,
        postedBy: user.data.result._id,
        jobStatus: JobStatusEnum.OPEN,
      };
      await axiosClient().post(apiUrls.addJob(), data);
      dispatch(appStatusAction(status));
      history.push("/job-success-login");
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function JobPostNewAcct(
  formData: UserSignupFormModel,
  jobData: JobFormModel,
  history: any
): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      await axiosClient().post(apiUrls.checkEmailPhone(), formData);
      const user = await axiosClient().post(apiUrls.register(), formData);
      let data: JobFormModel = {
        ...jobData,
        postedBy: user.data.id,
        jobStatus: JobStatusEnum.OPEN,
      };
      await axiosClient().post(apiUrls.addJob(), data);
      dispatch(appStatusAction(status));
      history.push("/job-success");
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function GetAllCategories(): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      const response = await axiosClient().get(apiUrls.getAllCategories());
      dispatch(getCategoriesAction(response.data.result));
      dispatch(appStatusAction(status));
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function GetAllSubCategories(): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      const response = await axiosClient().get(apiUrls.getAllSubCategories());
      dispatch(getSubCategoriesAction(response.data.result));
      dispatch(appStatusAction(status));
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function GetAllUsers(rq: UserQuery): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      const response = await axiosClient().get(apiUrls.getAllUsers(rq));
      dispatch(getAllUsersAction(response.data.result));
      dispatch(appStatusAction(status));
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function GetUserDetailById(id: string): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      const response = await axiosClient().get(apiUrls.getUsersById(id));
      dispatch(getUserDetailAction(response.data.result[0]));
      dispatch(appStatusAction(status));
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function ChangeIsActive(id: string, active: boolean): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      await axiosClient().post(apiUrls.changeIsActive(id, active));
      const response = await axiosClient().get(apiUrls.getUsersById(id));
      dispatch(getUserDetailAction(response.data.result[0]));
      dispatch(
        appStatusAction({
          ...status,
          responseText: `Account is ${active ? "activated" : "blocked"}`,
        })
      );
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function ChangeUserJobs(id: string, jobs: number): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      await axiosClient().post(apiUrls.changeUserJobs(id, jobs));
      const response = await axiosClient().get(apiUrls.getUsersById(id));
      dispatch(getUserDetailAction(response.data.result[0]));
      dispatch(
        appStatusAction({
          ...status,
          responseText: `Record Updated`,
        })
      );
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function GetCategoryDetailById(id: string): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      const response = await axiosClient().get(apiUrls.getCategoryById(id));
      dispatch(getCategoryAction(response.data.result[0]));
      dispatch(appStatusAction(status));
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function GetSubCategoryDetailById(id: string): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      const response = await axiosClient().get(apiUrls.getSubCategoryById(id));
      dispatch(getSubCategoryAction(response.data.result[0]));
      dispatch(appStatusAction(status));
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function AddEditCategory(data: CategoryFormModel, id: string): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      if (!id || id === "") {
        await axiosClient().post(apiUrls.addCategory(), data);
        dispatch(
          appStatusAction({
            ...status,
            responseText: `Category Added.`,
          })
        );
      } else {
        const response = await axiosClient().post(
          apiUrls.editCategory(id),
          data
        );
        dispatch(getCategoryAction(response.data.result));
        dispatch(
          appStatusAction({
            ...status,
            responseText: `Category Updated.`,
          })
        );
      }
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function DeleteCategory(id: string): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      await axiosClient().post(apiUrls.deleteCategory(id));
      const response = await axiosClient().get(apiUrls.getAllCategories());
      dispatch(getCategoriesAction(response.data.result));
      dispatch(
        appStatusAction({
          ...status,
          responseText: `Category Deleted.`,
        })
      );
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function DeleteSubCategory(id: string): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      await axiosClient().post(apiUrls.deleteSubCategory(id));
      const response = await axiosClient().get(apiUrls.getAllSubCategories());
      dispatch(getSubCategoriesAction(response.data.result));
      dispatch(
        appStatusAction({
          ...status,
          responseText: `Skill Deleted.`,
        })
      );
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function AddEditSubCategory(
  data: SubCategoryFormModel,
  id: string
): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      if (!id || id === "") {
        await axiosClient().post(apiUrls.addSubCategory(), data);
        dispatch(
          appStatusAction({
            ...status,
            responseText: `Skill Added.`,
          })
        );
      } else {
        const response = await axiosClient().post(
          apiUrls.editSubCategory(id),
          data
        );
        dispatch(getSubCategoryAction(response.data.result));
        dispatch(
          appStatusAction({
            ...status,
            responseText: `Skill Updated.`,
          })
        );
      }
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function GetJobsDataEffect(rq: JobQuery): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      const response = await axiosClient().get(apiUrls.getJobs(rq));
      if (rq.summary) {
        dispatch(
          getJobSummariesAction(response.data.result, response.data.total)
        );
      } else {
        dispatch(getJobDataAction(response.data.result[0]));
      }

      dispatch(appStatusAction(status));
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function AddEditJob(
  data: JobFormModel,
  id: string,
  history: any
): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      if (!id || id === "new") {
        await axiosClient().post(apiUrls.addJob(), data);
        dispatch(
          appStatusAction({
            ...status,
            responseText: `Job Posted.`,
          })
        );
        history.push("/dashboard");
      } else {
        const response = await axiosClient().post(apiUrls.editJob(id), data);
        dispatch(getJobDataAction(response.data.result));
        dispatch(
          appStatusAction({
            ...status,
            responseText: `Job Updated.`,
          })
        );
        history.push("/dashboard");
      }
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function PurchaseJob(data: JobPurchaseForm, rq: JobQuery): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      await axiosClient().post(apiUrls.purchaseJob(), data);
      const response = await axiosClient().get(apiUrls.getJobs(rq));
      dispatch(dispatch(getJobDataAction(response.data.result[0])));
      dispatch(
        appStatusAction({
          ...status,
          responseText: `You have purchased this job.`,
        })
      );
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}

export function ChangeJobStatus(
  data: JobStatusChangeForm,
  jobId: string
): Effect {
  return async function (dispatch) {
    try {
      dispatch(appStatusAction({ ...status, loading: true }));
      await axiosClient().post(apiUrls.changeJobStatus(jobId), data);
      const response = await axiosClient().get(
        apiUrls.getJobs({ postedBy: true, id: jobId })
      );
      dispatch(getJobDataAction(response.data.result[0]));
      dispatch(
        appStatusAction({
          ...status,
          responseText: `Job status is updated.`,
        })
      );
    } catch (err) {
      console.log(err);
      const error = err.response
        ? err.response.data.message
        : "Something went wrong...";
      dispatch(
        appStatusAction({
          ...status,
          error: true,
          responseText: error,
        })
      );
    }
  };
}
