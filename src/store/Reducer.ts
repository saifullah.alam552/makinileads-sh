import produce from "immer";
import {
  ActionsEnum,
  ActionsType,
  AppMainStore,
  IAppStatus,
  IUserData,
  JobPurchaseType,
  JobStatusEnum,
  UserTypeEnum,
} from "../types";

export const status: IAppStatus = {
  error: false,
  loading: false,
  responseText: "",
};

const userInit: IUserData = {
  BRELA: 0,
  NIDA: 0,
  _id: "",
  companyName: "",
  confirmPassword: "",
  contactPerson: "",
  district: "",
  documentsUrl: [],
  email: "",
  experience: 0,
  firstName: "",
  isActive: false,
  isEmailConfirm: false,
  jobTitle: "",
  lastName: "",
  noOfEmpoyees: 0,
  password: "",
  phoneNumber: "",
  profilePicUrl: "",
  region: "",
  skills: [],
  streetAddress: "",
  userType: UserTypeEnum.SP_IND,
  vocationTraining: "No",
  ward: "",
  expDetails: "",
  jobsCanBuy: 0,
  refPersonName: "",
  refPhoneNumber: "",
  createdAt: "",
  updatedAt: "",
  forgetPassword: false,
};

export const initialState: AppMainStore = {
  appStatus: { error: false, loading: false, responseText: "" },
  accessToken: localStorage.getItem("token") || "",
  categories: [],
  subCategories: [],
  allUsers: [],
  category: {
    _id: "",
    createdAt: "",
    isActive: false,
    title: "",
    updatedAt: "",
  },
  subCategory: {
    _id: "",
    category: {
      _id: "",
      createdAt: "",
      isActive: false,
      title: "",
      updatedAt: "",
    },
    createdAt: "",
    updatedAt: "",
    isActive: false,
    title: "",
  },
  userDetail: userInit,
  user: userInit,

  jobSummaries: { data: [], total: 0 },
  jobData: {
    _id: "",
    createdAt: "",
    district: "",
    isActive: false,
    jobAmount: 0,
    jobDescription: "",
    jobPurchaseType: JobPurchaseType.NONE,
    jobStatus: JobStatusEnum.OPEN,
    jobTitle: "",
    postedBy: userInit,
    purchasedBy: [],
    region: "",
    skills: [],
    streetAddress: "",
    updatedAt: "",
    ward: "",
  },
};

export function Reducer(state = initialState, action: ActionsType) {
  switch (action.type) {
    case ActionsEnum.APP_STATUS:
      return produce(state, (draft) => {
        draft.appStatus = action.payload;
      });
    case ActionsEnum.ACCESS_TOKEN:
      return produce(state, (draft) => {
        draft.accessToken = action.payload;
      });
    case ActionsEnum.USER_INFO:
      return produce(state, (draft) => {
        draft.user = action.payload;
      });
    case ActionsEnum.USER_DETAIL:
      return produce(state, (draft) => {
        draft.userDetail = action.payload;
      });
    case ActionsEnum.GET_ALL_USERS:
      return produce(state, (draft) => {
        draft.allUsers = action.payload;
      });
    case ActionsEnum.GET_CATEGORY:
      return produce(state, (draft) => {
        draft.category = action.payload;
      });
    case ActionsEnum.GET_SUB_CATEGORY:
      return produce(state, (draft) => {
        draft.subCategory = action.payload;
      });
    case ActionsEnum.GET_CATEGORIES:
      return produce(state, (draft) => {
        draft.categories = action.payload;
      });
    case ActionsEnum.GET_SUB_CATEGORIES:
      return produce(state, (draft) => {
        draft.subCategories = action.payload;
      });
    case ActionsEnum.GET_JOB_SUMMARIES:
      return produce(state, (draft) => {
        draft.jobSummaries = action.payload;
      });
    case ActionsEnum.GET_JOB_DATA:
      return produce(state, (draft) => {
        draft.jobData = action.payload;
      });
    case ActionsEnum.LOGOUT:
      return initialState;
    default:
      return state;
  }
}
