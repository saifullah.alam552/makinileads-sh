import * as yup from "yup";
import {
  changePasswdValidate,
  forgetPasswdValidate,
  loginFormValidate,
} from "../utils/validations";

export type LoginFormInput = yup.InferType<typeof loginFormValidate>;
export type ForgetPasswdInput = yup.InferType<typeof forgetPasswdValidate>;
export type ChangePasswdInput = yup.InferType<typeof changePasswdValidate>;

// SERVICE PROVIDER FORM
export interface UserSignupFormModel {
  userType: UserTypeEnum;
  firstName: string;
  lastName: string;
  email: string;
  NIDA: number;
  vocationTraining: "Yes" | "No";
  experience: number;
  companyName: string;
  contactPerson: string;
  jobTitle: string;
  phoneNumber: string;
  BRELA: number;
  noOfEmpoyees: number;
  region: string;
  district: string;
  ward: string;
  streetAddress: string;
  password: string;
  confirmPassword: string;
  skills: string[];
  documentsUrl: string[];
  profilePicUrl: string;
  isActive: boolean;
  jobsCanBuy: number;
  refPersonName: string;
  refPhoneNumber: string;
  expDetails: string;
}

// ENUM
export enum UserTypeEnum {
  SP_IND = "SP-IND",
  SP_COM = "SP-COM",
  SR_IND = "SR-IND",
  SR_COM = "SR-COM",
  ADMIN = "ADMIN",
}

export enum JobStatusEnum {
  CONTACTED = "CONTACTED",
  COMPLETED = "COMPLETED",
  REPOSTED = "REPOSTED",
  OPEN = "OPEN",
  PURCHASED = "PURCHASED",
}

export enum JobPurchaseType {
  EXCLUSIVE = "EXCLUSIVE",
  GENERAL = "GENERAL",
  NONE = "NONE",
}

interface TimeStamps {
  createdAt: string;
  updatedAt: string;
}

export interface IUserData extends UserSignupFormModel, TimeStamps {
  _id: string;
  isEmailConfirm: boolean;
  forgetPassword: boolean;
}

// JOB
export interface JobFormModel {
  jobTitle: string;
  jobDescription: string;
  jobAmount: number;
  skills: string[];
  region: string;
  district: string;
  ward: string;
  streetAddress: string;
  isActive: boolean;
  postedBy: string;
  jobStatus: JobStatusEnum;
}

export interface JobPurchaseForm {
  jobId: string;
  jobPurchaseType: JobPurchaseType;
}

export interface JobStatusChangeForm {
  jobStatus: JobStatusEnum;
  isActive: boolean;
}

export interface IJobData extends TimeStamps {
  _id: string;
  jobTitle: string;
  jobDescription: string;
  jobAmount: number;
  skills: string[];
  region: string;
  district: string;
  ward: string;
  streetAddress: string;
  isActive: boolean;
  jobStatus: JobStatusEnum;
  jobPurchaseType: JobPurchaseType;
  postedBy: IUserData;
  purchasedBy: IUserData[];
}

export interface IJobSummaryData extends TimeStamps {
  _id: string;
  jobTitle: string;
  jobDescription: string;
  jobAmount: number;
  skills: string[];
  jobStatus: JobStatusEnum;
  jobPurchaseType: JobPurchaseType;
  isActive: boolean;
  purchasedBy: string[];
  region: string;
  district: string;
  ward: string;
}

// CATEGORIES & SUBCATEGORIES
export interface CategoryFormModel {
  title: string;
  isActive: boolean;
}

export interface SubCategoryFormModel {
  title: string;
  category: string;
  isActive: boolean;
}

export interface ICategoryData extends TimeStamps {
  _id: string;
  title: string;
  isActive: boolean;
}

export interface ISubCategoryData extends TimeStamps {
  _id: string;
  title: string;
  isActive: boolean;
  category: ICategoryData;
}

export interface IAppStatus {
  loading: boolean;
  error: boolean;
  responseText: string;
}

// Store
export type AppMainStore = {
  appStatus: IAppStatus;
  accessToken: string;
  user: IUserData;
  userDetail: IUserData;
  allUsers: IUserData[];
  categories: ICategoryData[];
  category: ICategoryData;
  subCategories: ISubCategoryData[];
  subCategory: ISubCategoryData;
  jobSummaries: { data: IJobSummaryData[]; total: number };
  jobData: IJobData;
};

// State
export type AppStateType = {
  mainStore: AppMainStore;
};

// Actions
type ActionMap<M extends { [index: string]: any }> = {
  [Key in keyof M]: M[Key] extends undefined
    ? { type: Key }
    : { type: Key; payload: M[Key] };
};

export enum ActionsEnum {
  ACCESS_TOKEN = "ACCESS_TOKEN",
  APP_STATUS = "APP_STATUS",
  USER_INFO = "USER_INFO",
  USER_DETAIL = "USER_DETAIL",
  GET_ALL_USERS = "GET_ALL_USERS",
  LOGOUT = "LOGOUT",
  GET_CATEGORIES = "GET_CATEGORIES",
  GET_CATEGORY = "GET_CATEGORY",
  GET_SUB_CATEGORIES = "GET_SUB_CATEGORIES",
  GET_SUB_CATEGORY = "GET_SUB_CATEGORY",
  GET_JOB_SUMMARIES = "GET_JOB_SUMMARIES",
  GET_JOB_DATA = "GET_JOB_DATA",
}

type Payloads = {
  [ActionsEnum.ACCESS_TOKEN]: string;
  [ActionsEnum.APP_STATUS]: IAppStatus;
  [ActionsEnum.USER_INFO]: IUserData;
  [ActionsEnum.USER_DETAIL]: IUserData;
  [ActionsEnum.LOGOUT]: undefined;
  [ActionsEnum.GET_CATEGORIES]: ICategoryData[];
  [ActionsEnum.GET_CATEGORY]: ICategoryData;
  [ActionsEnum.GET_SUB_CATEGORIES]: ISubCategoryData[];
  [ActionsEnum.GET_SUB_CATEGORY]: ISubCategoryData;
  [ActionsEnum.GET_ALL_USERS]: IUserData[];
  [ActionsEnum.GET_JOB_SUMMARIES]: { data: IJobSummaryData[]; total: number };
  [ActionsEnum.GET_JOB_DATA]: IJobData;
};

export type ActionsType = ActionMap<Payloads>[keyof ActionMap<Payloads>];
