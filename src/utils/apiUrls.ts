import { JobPurchaseType, JobStatusEnum, UserTypeEnum } from "../types";
import { cloudinary_config } from "./cloudinary-config";

// const devUrl = "http://localhost:5000/api";
const devUrl = "https://powerful-fortress-45521.herokuapp.com/api";
const prodUrl = "https://powerful-fortress-45521.herokuapp.com/api";

export interface JobQuery {
  id?: string;
  isActive?: string;
  skillId?: string;
  status?: JobStatusEnum;
  purchaseType?: JobPurchaseType;
  summary?: boolean;
  postedBy?: boolean;
  purchasedBy?: boolean;
  postedById?: string;
  purchasedById?: string;
  sortBy?: string;
  postedDateStart?: string;
  postedDateEnd?: string;
  limit?: number;
  skip?: number;
}

export interface UserQuery {
  type?: UserTypeEnum;
  name?: string;
}

const apiUrls = {
  baseURL: process.env.NODE_ENV === "production" ? prodUrl : devUrl,

  cloudinaryUpload: () =>
    `https://api.cloudinary.com/v1_1/${cloudinary_config.cloud_name}/upload`,

  // user
  login: () => "/login",
  register: () => "/register",
  getUserInfo: () => "/getUserInfo",
  changePassword: () => "/changePassword",
  checkEmailPhone: () => "/checkEmailPhone",
  forgetPassword: () => "/forgetPassword",
  changeIsActive: (id: string, active: boolean) =>
    `/changeIsActive/${id}/${active}`,
  changeUserJobs: (id: string, jobs: number) => `/changeUserJobs/${id}/${jobs}`,
  updateUser: (id: string) => `/updateUser/${id}`,
  getAllUsers: (rq: UserQuery) =>
    `/getUsers?type=${rq.type || ""}&name=${rq.name || ""}`,
  getUsersById: (id: string) => `/getUsers?id=${id}`,

  // job
  addJob: () => "/addJob",
  editJob: (id: string) => `/editJob/${id}`,
  changeJobStatus: (id: string) => `/changeJobStatus/${id}`,
  purchaseJob: () => `/purchaseJob`,
  getJobs: (rq: JobQuery) =>
    `/getJobs?id=${rq.id || ""}&isActive=${rq.isActive || ""}&skillId=${
      rq.skillId || ""
    }&status=${rq.status || ""}&purchaseType=${rq.purchaseType || ""}&summary=${
      rq.summary || ""
    }&postedBy=${rq.postedBy || ""}&purchasedBy=${
      rq.purchasedBy || ""
    }&postedById=${rq.postedById || ""}&purchasedById=${
      rq.purchasedById || ""
    }&sortBy=${rq.sortBy || ""}&postedDateStart=${
      rq.postedDateStart || ""
    }&postedDateEnd=${rq.postedDateEnd || ""}&limit=${rq.limit || ""}&skip=${
      rq.skip || ""
    }`,

  // category
  addCategory: () => `/addCategory`,
  editCategory: (id: string) => `/editCategory/${id}`,
  deleteCategory: (id: string) => `/deleteCategory/${id}`,
  getAllCategories: () => "/getCategories",
  getCategoryById: (id: string) => `/getCategories?id=${id}`,

  // sub category
  addSubCategory: () => `/addSubCategory`,
  editSubCategory: (id: string) => `/editSubCategory/${id}`,
  deleteSubCategory: (id: string) => `/deleteSubCategory/${id}`,
  getAllSubCategories: () => "/getSubCategories?withCategory=true",
  getSubCategoryById: (id: string) =>
    `/getSubCategories?withCategory=true&id=${id}`,
};

export default apiUrls;
