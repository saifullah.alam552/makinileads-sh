import axios, { AxiosRequestConfig } from "axios";
import apiUrl from "./apiUrls";
import { store } from "../store/ConfigStore";

export function axiosClient() {
  const defaultOptions: AxiosRequestConfig = {
    baseURL: apiUrl.baseURL,
    headers: {
      "Content-Type": "application/json",
    },
  };

  // Create instance
  let instance = axios.create(defaultOptions);

  // Set the AUTH token for any request
  instance.interceptors.request.use(function (config) {
    const token = localStorage.getItem("token");
    config.headers.Authorization = token;
    return config;
  });

  instance.defaults.headers.common[
    "Authorization"
  ] = store.getState().mainStore.accessToken;

  return instance;
}
