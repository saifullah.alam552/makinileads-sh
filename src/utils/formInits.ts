import { FormErrors } from "../hooks/useForm";
import {
  CategoryFormModel,
  ChangePasswdInput,
  ForgetPasswdInput,
  JobFormModel,
  JobPurchaseForm,
  JobPurchaseType,
  JobStatusEnum,
  LoginFormInput,
  SubCategoryFormModel,
  UserSignupFormModel,
  UserTypeEnum,
} from "../types";

export const loginInitValues: LoginFormInput = {
  username: "",
  password: "",
};

export const loginInitErrors: FormErrors<LoginFormInput> = {
  username: "",
  password: "",
};

export const forgetInitValues: ForgetPasswdInput = {
  username: "",
};

export const forgetInitErrors: FormErrors<ForgetPasswdInput> = {
  username: "",
};

export const changePasswordInitValues: ChangePasswdInput = {
  currentPassword: "",
  confirmPassword: "",
  newPassword: "",
};

export const changePasswordInitErrors: FormErrors<ChangePasswdInput> = {
  currentPassword: "",
  confirmPassword: "",
  newPassword: "",
};

export const userInitValues: UserSignupFormModel = {
  userType: UserTypeEnum.SP_IND,
  firstName: "",
  lastName: "",
  email: "",
  phoneNumber: "",
  confirmPassword: "",
  password: "",
  NIDA: 0,
  vocationTraining: "Yes",
  experience: 0,
  companyName: "",
  contactPerson: "",
  jobTitle: "",
  BRELA: 0,
  noOfEmpoyees: 0,
  region: "",
  district: "",
  ward: "",
  streetAddress: "",
  skills: [],
  documentsUrl: [],
  profilePicUrl: "",
  isActive: true,
  expDetails: "",
  jobsCanBuy: 3,
  refPersonName: "",
  refPhoneNumber: "",
};

export const userInitErrors: FormErrors<UserSignupFormModel> = {
  userType: "",
  firstName: "",
  lastName: "",
  email: "",
  NIDA: "",
  vocationTraining: "",
  experience: "",
  companyName: "",
  contactPerson: "",
  jobTitle: "",
  phoneNumber: "",
  BRELA: "",
  noOfEmpoyees: "",
  region: "",
  district: "",
  ward: "",
  streetAddress: "",
  confirmPassword: "",
  password: "",
  skills: "",
  documentsUrl: "",
  profilePicUrl: "",
  isActive: "",
  expDetails: "",
  jobsCanBuy: "",
  refPersonName: "",
  refPhoneNumber: "",
};

export const jobInitValues: JobFormModel = {
  jobAmount: 0,
  jobDescription: "",
  district: "",
  region: "",
  skills: [],
  streetAddress: "",
  jobTitle: "",
  ward: "",
  isActive: true,
  jobStatus: JobStatusEnum.OPEN,
  postedBy: "",
};

export const jobInitErrors: FormErrors<JobFormModel> = {
  jobAmount: "",
  jobDescription: "",
  district: "",
  region: "",
  skills: "",
  streetAddress: "",
  jobTitle: "",
  ward: "",
  isActive: "",
  jobStatus: "",
  postedBy: "",
};

export const categoryInitValues: CategoryFormModel = {
  title: "",
  isActive: true,
};

export const cateogoryInitErrors: FormErrors<CategoryFormModel> = {
  title: "",
  isActive: "",
};

export const subCategoryInitValues: SubCategoryFormModel = {
  title: "",
  category: "",
  isActive: true,
};

export const subCateogoryInitErrors: FormErrors<SubCategoryFormModel> = {
  title: "",
  category: "",
  isActive: "",
};

export const JobPurchaseInitValues: JobPurchaseForm = {
  jobId: '',
  jobPurchaseType: JobPurchaseType.NONE
}
