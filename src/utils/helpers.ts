import {
  ICategoryData,
  ISubCategoryData,
  IUserData,
  UserTypeEnum,
} from "../types";

export const getUsername = (user: IUserData) => {
  let name: string = "No Name";
  if (
    user.userType === UserTypeEnum.SP_COM ||
    user.userType === UserTypeEnum.SR_COM
  ) {
    name = user.companyName;
  } else if (user.firstName !== "") {
    name = `${user.firstName} ${user.lastName}`;
  }
  return name;
};

export const SPUserSelect = (users: IUserData[]) =>
  users
    .filter(
      (u: IUserData) =>
        u.userType === UserTypeEnum.SP_COM || u.userType === UserTypeEnum.SP_IND
    )
    .map((d: IUserData) => {
      return {
        key: d._id,
        label: getUsername(d),
        disabled: d.isActive,
      };
    });

export const SRUserSelect = (users: IUserData[]) =>
  users
    .filter(
      (u: IUserData) =>
        u.userType === UserTypeEnum.SR_COM || u.userType === UserTypeEnum.SR_IND
    )
    .map((d: IUserData) => {
      return {
        key: d._id,
        label: getUsername(d),
        disabled: d.isActive,
      };
    });

export const CategorySelect = (categories: ICategoryData[]) =>
  categories.map((d: ICategoryData) => {
    return {
      key: d._id,
      label: d.title,
      disabled: d.isActive,
    };
  });

export const SubCategorySelect = (subCategory: ISubCategoryData[]) =>
  subCategory.map((d: ISubCategoryData) => {
    return {
      key: d._id,
      label: d.title,
      disabled: d.isActive,
    };
  });

export const SubCategorySelectOnCategory = (
  categories: ISubCategoryData[],
  catId: string
) =>
  categories
    .filter((d) => d.category._id === catId)
    .map((d: ISubCategoryData) => {
      return {
        key: d._id,
        label: d.title,
        disabled: d.isActive,
      };
    });

export const currencyFormat = (value: number) => {
  const amt = `${parseInt(value!.toString(), 10)}`.replace(
    /(\d)(?=(\d{3})+(?!\d))/g,
    "$1,"
  );
  return `${value === 0 ? "Nil" : `${amt}`}`;
};
