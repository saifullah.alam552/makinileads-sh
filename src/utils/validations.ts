import * as yup from "yup";

export const loginFormValidate = yup.object({
  username: yup.string().required("Required."),
  password: yup.string().required("Required."),
});

export const forgetPasswdValidate = yup.object({
  username: yup.string().required("Required."),
});

export const changePasswdValidate = yup.object({
  currentPassword: yup.string().required("Required."),
  newPassword: yup.string().required("Required").min(6, "Minimum 6 Characters"),
  confirmPassword: yup
    .string()
    .required("Required")
    .min(6, "Minimum 6 Characters"),
});

export const validateTypeForm = yup.object({
  userType: yup.string().notRequired(),
});

export const addressValidation = yup.object({
  region: yup.string().required("Required"),
  district: yup.string().required("Required"),
  ward: yup.string().required("Required"),
  streetAddress: yup.string().required("Required"),
});

export const UserIndividualValidation = yup.object({
  firstName: yup.string().required("Required"),
  lastName: yup.string().required("Required"),
  phoneNumber: yup.string().required("Required"),
  email: yup.string().required("Required").email("Enter valid email."),
});

export const UserCompanyValidation = yup.object({
  companyName: yup.string().required("Required"),
  contactPerson: yup.string().required("Required"),
  jobTitle: yup.string().required("Required"),
  phoneNumber: yup.string().required("Required"),
  email: yup.string().required("Required").email("Enter valid email."),
});

export const SPIndividualValidation = UserIndividualValidation.concat(
  yup.object({
    NIDA: yup.string().required("Required"),
    refPersonName: yup.string().required("Required"),
    refPhoneNumber: yup.string().required("Required"),
  })
);

export const SPCompanyValidation = UserCompanyValidation.concat(
  yup.object({
    BRELA: yup.string().required("Required"),
  })
);

export const validateInfoEduIndividual = yup.object({
  vocationTraining: yup.string().required("Required"),
  experience: yup.number().required("Required").positive().integer().min(1),
  expDetails: yup.string().required("Required"),
});

export const validateInfoEduCompany = yup.object({
  noOfEmpoyees: yup.number().required("Required").positive().integer().min(1),
  experience: yup.number().required("Required").positive().integer().min(1),
  expDetails: yup.string().required("Required"),
});

export const individualFormSPValidate = SPIndividualValidation.concat(
  addressValidation
).concat(validateInfoEduIndividual);

export const individualFormValidate = UserIndividualValidation.concat(
  addressValidation
);

export const companyFormSPValidate = SPCompanyValidation.concat(
  addressValidation
).concat(validateInfoEduCompany);

export const companyFormValidate = UserCompanyValidation.concat(
  addressValidation
);

export const validatePassword = yup.object({
  password: yup.string().required("Required").min(6, "Minimum 6 Characters"),
  confirmPassword: yup
    .string()
    .required("Required")
    .min(6, "Minimum 6 Characters"),
});

export const validateJobDetail = yup.object({
  jobTitle: yup.string().required("Required"),
  jobDescription: yup.string().required("Required"),
  jobAmount: yup.number().notRequired(),
});

export const jobFormValidation = validateJobDetail.concat(addressValidation);

export const validateCategoryForm = yup.object({
  title: yup.string().required("Required"),
  isActive: yup.string().notRequired(),
});

export const validateSubCategoryForm = yup.object({
  title: yup.string().required("Required"),
  category: yup.string().required("Required"),
  isActive: yup.string().notRequired(),
});
